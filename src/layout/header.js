import React, { useState, useEffect } from 'react'
import Storage from '@/utils/storage'
import { withRouter } from "react-router-dom"
import { withLocalize } from 'react-localize-redux'
// import { useDispatch } from 'react-redux'
// import { actions } from '@/store/actions'
import ModalChangePassword from '@/components/modal-change-password'
import iconVI from '../../public/assets/images/vn.svg'
// import iconEN from '../../public/assets/images/gb.svg'
import iconJP from '../../public/assets/images/jp.svg'
import './style.scss'

const Header = ({ history, pushToggle, isSideBarOpen, setActiveLanguage, translate }) => {
  const language = [
    {
      name: translate('utils.vi'),
      code: 'vi',
      icon: iconVI
    }, 
    // {
    //   name: 'English',
    //   code: 'en',
    //   icon: iconEN
    // }, 
    {
      name: translate('utils.ja'),
      code: 'jp',
      icon: iconJP
    }
  ]
  const [openModal, setOpenModal] = useState(false)
  const [selectedLanguage, setSelectedLanguage] = useState(language[0])

  const currentLanguage = Storage.get('LANGUAGE')

  useEffect(() => {
    if (currentLanguage) {
      const languageSelect = language.find((e) => e.code === currentLanguage)
      setActiveLanguage(languageSelect?.code)
      setSelectedLanguage(languageSelect)
    }
  }, [currentLanguage])

  const handleChangeLanguage = (value) => {
    Storage.set('LANGUAGE', value?.code)
    setActiveLanguage(value?.code)
    setSelectedLanguage(value)
  }

  const onLogout = () => {
    Storage.clear();
    history.push('/')
  };

  const handleToggle = () => {
    pushToggle(!isSideBarOpen)
  }

  return (
    <div className={`page-topbar gradient-blue1 ${!isSideBarOpen && `sidebar_shift`}`}>
      <div className="logo-area crypto" />
      <div className="quick-area">
        <div className="pull-left">
          <ul className="info-menu left-links list-inline list-unstyled">
            <li className="sidebar-toggle-wrap">
              <a onClick={() => handleToggle()} data-toggle="sidebar" className="sidebar_toggle">
                <i className="fa fa-bars" />
              </a>
            </li>
          </ul>
        </div>
        <div className="pull-right">
          <ul className="info-menu right-links list-inline list-unstyled">

            <li className="profile">
              <a href="#" data-toggle="dropdown" className="toggle">
                <img
                  src={selectedLanguage?.icon}
                  alt=""
                  style={{
                    height: '20px',
                    objectFit: 'cover',
                    boxShadow: '0 0 0 1px rgb(0 0 0 / 10%), 0 4px 16px rgb(0 0 0 / 10%)'
                  }}
                />
              </a>
              <ul className="dropdown-menu profile animated fadeIn">
                {language.map((e) => (
                  <li key={e.code} onClick={() => handleChangeLanguage(e)}>
                    <a className="flag-item">
                      <img
                        src={e.icon}
                        alt=""
                        style={{
                          height: '20px',
                          objectFit: 'cover',
                          boxShadow: '0 0 0 1px rgb(0 0 0 / 10%), 0 4px 16px rgb(0 0 0 / 10%)'
                        }}
                      /> {e.name}
                    </a>
                  </li>
                ))}
              </ul>
            </li>


            <li className="profile">
              <a href="#" data-toggle="dropdown" className="toggle">
                <img
                  src='/assets/images/user-male.png'
                  alt="user-image"
                  className="img-circle img-inline"
                />
                <span>Admin
                  <i className="fa fa-angle-down" />
                </span>
              </a>
              <ul className="dropdown-menu profile animated fadeIn">

                <li onClick={onLogout}>
                  <a>
                    <i className="fa fa-lock" />{translate('utils.logout')}
                  </a>
                </li>

                <li onClick={() => setOpenModal(true)}>
                  <a>
                    <i className="fa fa-wrench" />{translate('utils.change-password')}
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>

      <ModalChangePassword
        close={() => setOpenModal(false)}
        isOpen={openModal}
      />
      <div className="chatapi-windows "></div>
    </div>
  )
}

export default withLocalize(withRouter(Header))
