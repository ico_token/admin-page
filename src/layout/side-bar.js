import React, { useEffect, useState } from 'react'
import { withLocalize } from 'react-localize-redux'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import './style.scss'

function SideBar({ isSideBarOpen, translate }) {
  const [activeMenu, setActiveMenu] = useState('')
  const [child, setChild] = useState('');

  const menu = [
    {
      link: '/dashboard',
      title: translate('dashboard.dashboard'),
      icon: '/data/crypto-dash/icons/1.png'
    },
    {
      link: '/users',
      title: translate('users.users'),
      icon: '/data/crypto-dash/icons/10.png'
    },
    {
      link: '/transactions',
      title: translate('transactions.transactions'),
      icon: '/data/crypto-dash/icons/3.png'
    },
    {
      link: '/admin-wallet',
      title: translate('wallet.admin-wallet'),
      icon: '/data/crypto-dash/icons/2.png'
    },
    {
      link: '/user-wallet',
      title: translate('wallet.user-wallet'),
      icon: '/data/crypto-dash/icons/2.png'
    },
    {
      link: '/system-settings',
      title: translate('system-setting.system-setting'),
      icon: '/data/crypto-dash/icons/11.png'
    },
    {
      link: '/packets',
      title: translate('packets.packets'),
      icon: '/data/crypto-dash/icons/3.png'
    },
    {
      link: '/contacts',
      title: translate('contacts.contacts'),
      icon: '/data/crypto-dash/icons/12.png'
    },
    {
      link: '/withdraw-requests',
      title: translate('withdraw.withdraw-request'),
      icon: '/data/crypto-dash/icons/6.png'
    },
    {
      link: '/group',
      title: translate('group.group'),
      icon: '/data/crypto-dash/icons/15.png'
    },
    {
      link: '/supply-rank',
      title: translate('users.supply-rank'),
      icon: '/data/crypto-dash/icons/4.png'
    },
    {
      link: '/coin-supplications',
      title: translate('coin-supply.coin-supplications'),
      icon: '/data/crypto-dash/icons/16.png'
    },
    // {
    //   link: '/buy-sell',
    //   title: 'Buy & Sell',
    //   icon: '/data/crypto-dash/icons/6.png'
    // },
    // {
    //   link: '/ico-admin',
    //   title: 'ICO Admin',
    //   icon: '/data/crypto-dash/icons/4.png',
    //   label: 'HOT'
    // },
    // {
    //   link: '/settings',
    //   title: 'Settings',
    //   icon: '/data/crypto-dash/icons/11.png',
    //   child: [
    //     {
    //       link: '/personal-settings',
    //       title: 'Personal Settings'
    //     },
    //     {
    //       link: '/account-settings',
    //       title: 'Account Settings'
    //     }
    //   ]
    // },
    // {
    //   link: '/access',
    //   title: 'Access Pages',
    //   icon: '/data/crypto-dash/icons/9.png',
    //   child: [
    //     {
    //       link: '/login',
    //       title: 'Login'
    //     },
    //     {
    //       link: '/register',
    //       title: 'Registration'
    //     },
    //     {
    //       link: '/404',
    //       title: '404'
    //     }
    //   ]
    // },
    // {
    //   link: '/mailbox',
    //   title: 'Mailbox',
    //   icon: '/data/crypto-dash/icons/7.png',
    //   child: [
    //     {
    //       link: '/inbox',
    //       title: 'Inbox'
    //     },
    //     {
    //       link: '/compose',
    //       title: 'Compose'
    //     },
    //     {
    //       link: '/view',
    //       title: 'View'
    //     },
    //     {
    //       link: '/sent',
    //       title: 'Sent',
    //       hidden: true
    //     }
    //   ]
    // },
    {
      link: '/coins',
      title: translate('coins.coins'),
      icon: '/data/crypto-dash/set2.png'
    },
    {
      link: '/referral',
      title: translate('referral.referral'),
      icon: '/data/crypto-dash/icons/5.png'
    },
    {
      link: '/faq',
      title: translate('faq.faq'),
      icon: '/data/crypto-dash/icons/12.png'
    },
    {
      link: '/logs',
      title: translate('logs.logs'),
      icon: '/data/crypto-dash/icons/13.png'
    }
  ]

  useEffect(() => {
    menu.forEach((e) => {
      if (e.child?.length) {
        const activeChild = e.child.find((i) => i.link === window.location.pathname)
        if (activeChild) {
          setActiveMenu(e.link)
          setChild(activeChild.link)
        }
      } else if (e.link === window.location.pathname) {
        setActiveMenu(e.link)
        setChild('')
      }
    })
  }, [window.location.pathname])

  return (
    <div className={`page-sidebar absolute ${!isSideBarOpen ? `collapseit` : ''}`}>
      <div className="page-sidebar-wrapper crypto" id="main-menu-wrapper">
        <ul className="wraplist">
          {menu.map((e, index) => {
            if (e.child?.length) {
              return (
                <div key={index}>
                  <li
                    className={classNames({ open: activeMenu === e.link })}
                    onClick={() => (activeMenu === e.link ? setActiveMenu('') : setActiveMenu(e.link))}
                  >
                    <a>
                      <i className="img">
                        <img src={e.icon} alt="" className="width-20" />
                      </i>
                      <span className="title">{e.title}</span>
                      {e.label && <span className="label label-accent">{e.label}</span>}
                      <span className={classNames('arrow', { open: activeMenu === e.link })} />
                    </a>
                  </li>
                  <li className={classNames('open', { 'active-menu-child': activeMenu === e.link })}>
                    <ul
                      className="sub-menu"
                      style={{ height: activeMenu === e.link ? `${e.child.length * 36}px` : '0' }}
                    >
                      {e.child.map((i) => !i.hidden && (
                        <Link
                          to={i.link}
                          key={i.title}
                          onClick={() => setChild(i.link)}
                          className={classNames({ active: child === i.link })}
                        >
                          {i.title}
                        </Link>
                      ))}
                    </ul>
                  </li>
                </div>
              )
            }

            return (
              <li
                className={classNames({ open: activeMenu === e.link })}
                key={index}
                onClick={() => setActiveMenu(e.link)}
              >
                <Link to={e.link}>
                  <i className="img">
                    <img src={e.icon} alt="" className="width-20" />
                  </i>
                  <span className="title">{e.title}</span>
                  {e.label && <span className="label label-accent">{e.label}</span>}
                </Link>
              </li>
            )
          })}
        </ul>
      </div>
      <div className="chatapi-windows "></div>
    </div>
  )
}

export default withLocalize(SideBar)
