import { MainApi } from './endpoint';

export function getListContacts(payload) {
  return MainApi.get(`/contacts`, payload)
}

export function updateContacts(payload) {
  return MainApi.put(`/contacts/${payload.id}`)
}