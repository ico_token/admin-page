import { MainApi, ExternalApi, FileApi } from "./endpoint";

const getMaintain = (payload) => MainApi.get("/maintenance", payload);

const getSignS3 = (payload) => MainApi.get("/aws/signs3", payload);

const uploadS3 = async (payload) =>
  ExternalApi.put(`${payload.url}`, payload.file);

const sendOtp = async (payload) => MainApi.post("/otp/send", payload);

const exportUser = async (payload) => FileApi.get("/export/excel", payload);
export { getMaintain, getSignS3, uploadS3, sendOtp, exportUser };
