import { MainApi } from './endpoint';

export function getIcoSetting() {
  return MainApi.get(`/ico-setting`)
}

export function getMaintenances() {
  return MainApi.get(`/maintenances`)
}

export function updateIcoSetting(payload) {
  return MainApi.put(`/ico-setting`, payload)
}

export function updateMaintenances(payload) {
  return MainApi.post(`/maintenances`, payload)
}

export function getAllLogs(payload) {
  return MainApi.get(`/logs`,payload)
}