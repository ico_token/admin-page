import { MainApi } from './endpoint';

export function getListWithdrawRequest(payload) {
  return MainApi.get(`/withdraw-requests`, payload)
}

export function handleWithdrawRequest(payload) {
  const { id, is_approved, private_key} = payload;
  return MainApi.post(`/withdraw-requests/${id}/handle`, {is_approved, private_key})
}