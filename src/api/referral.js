import { MainApi } from './endpoint';

export function getReferral(payload) {
  return MainApi.get(`/referrals/history`, payload)
}

export function getReferralTree(payload) {
  return MainApi.get(`/referrals/tree`, payload)
}

export function updateRank({ id, rank }) {
  return MainApi.post(`/referrals/user/${id}/rank`, { rank })
}