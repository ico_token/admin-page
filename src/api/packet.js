import { MainApi } from './endpoint';

export function createPacketDetail({payload, id}) {
  return MainApi.post(`/packets/${id}/packet-detail`, payload)
}

export function updatePacketDetail(payload) {
    return MainApi.put(`/packets/packet-details`, payload)
}

export function deletePacketDetail(payload) {
    return MainApi.delete(`/packets/packet-detail/${payload.id}`)
}

export function createPacket(payload) {
    return MainApi.post(`/packets`, payload)
}

export function updatePacket({payload, id}) {
    return MainApi.put(`/packets/${id}`, payload)
}

export function getPacketById(payload) {
    return MainApi.get(`/packets/${payload.id}`, payload)
}

export function getPacketList() {
    return MainApi.get(`/packets`)
}
