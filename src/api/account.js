import { MainApi } from './endpoint';

export function login(payload) {
  return MainApi.post('/auth/login', payload)
}

export function resetPassword(payload) {
  return MainApi.post('/auth/reset-password', payload)
}

export function changePassword(payload) {
  return MainApi.post('/auth/change-password', payload)
}

export function changePasswordByOldPassword(payload) {
  return MainApi.post('/auth/change-password-by-old-password', payload)
}

export function getStatistical() {
  return MainApi.get('/statistical')
}
