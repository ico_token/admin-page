import { MainApi } from './endpoint';

export function createFaq(payload) {
  return MainApi.post('/faq', payload)
}

export function deleteFaq(payload) {
  return MainApi.delete(`/faq/${payload.id}`, payload)
}

export function getFaq(payload) {
  return MainApi.get('/faq', payload)
}

export function updateFaq(payload) {
  return MainApi.put(`/faq/${payload.id}`, payload)
}

export function getFaqById(payload) {
  return MainApi.get(`/faq/${payload.id}`, payload)
}