import { MainApi } from './endpoint'

export function createCoin(payload) {
  return MainApi.post('/coins', payload)
}

export function getListCoin(payload) {
  return MainApi.get('/coins', payload)
}

export function updateCoin(payload) {
  return MainApi.put(`/coins/${payload.id}`, payload)
}

export function deleteCoin(payload) {
  return MainApi.delete('/coins', payload)
}
