import { MainApi } from './endpoint';

export function createWallet(payload) {
  return MainApi.post('/admin-wallets/create-wallet', payload)
}

export function getAdminWallet() {
  return MainApi.get('/admin-wallets')
}

export function sendOtpConfirmWallet(payload) {
  return MainApi.post('/admin-wallets/send-otp', payload)
}