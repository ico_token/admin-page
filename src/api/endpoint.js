import Request from "@/utils/request";
import Configs from "@/configs";

const endpoint = `${Configs.API_URL}`;

const MainApi = Request.create({
  endpoint,
  handleToken: true,
});

const ExternalApi = Request.create({
  endpoint: "",
});

const FileApi = Request.create({
  endpoint,
  handleToken: true,
  responseType: "blob", // Cấu hình để nhận file
});
export { MainApi, ExternalApi, FileApi };
