import { MainApi, ExternalApi } from './endpoint'

const linkBscscan = 'https://api.bscscan.com/api?module=account&action=tokenbalance&contractaddress=0xb6b2a00b52ab51cbb4066b3b19b1acd7b178e7eb&address=0xc37573e81d32fc3dacede8ec429de6706bc109a0&tag=latest&apikey=M3H3CJ2CQ43FDVTKZ9SEVAW12REC798STF'

export function getListUsers(payload) {
  return MainApi.get('/users', payload)
}

export function getUserById(payload) {
  return MainApi.get(`/users/${payload}`)
}

export function deleteUserById(payload) {
  return MainApi.delete(`/users/${payload}`)
}

export function updateUser({ id, payload }) {
  return MainApi.put(`/users/${id}`, { ...payload })
}

export function getTransactionByUser(payload) {
  return MainApi.get('/transactions', payload)
}

export function verifyKyc(payload) {
  const { id, is_kyc, reason } = payload
  return MainApi.post(`/users/${id}/verify-kyc`, { is_kyc, reason })
}

export function handleLockUser(payload) {
  const { id, is_lock } = payload
  return MainApi.put(`/users/${id}/lock`, { is_lock })
}

export function getTotalAmount(payload) {
  return ExternalApi.get(linkBscscan, payload)
}

export function getUserCurrency({ id, payload }) {
  return MainApi.get(`/users/currency/${id}`, payload)
}

export function getUserWallet() {
  return MainApi.get('/user-wallet')
}
