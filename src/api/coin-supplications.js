import { MainApi } from './endpoint';

export function getListCoinSupplications(payload) {
  return MainApi.get(`/coin-supplications`, payload)
}

export function supplyTosiToUser({payload, id}) {
  return MainApi.post(`/coin-supplications/user/${id}`, payload)
}

export function recallSuppliedTosiOfUser(payload) {
  return MainApi.delete(`/coin-supplications/user/${payload.id}`, payload)
}