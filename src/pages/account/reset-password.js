import React, { useEffect, useState } from 'react'
import { Formik, Form } from 'formik'
import { object, string, ref } from 'yup'
import { useDispatch } from 'react-redux'
import { TYPES, actions } from '@/store/actions'
import Input from '@/components/input'
import Page from '@/components/page'
import Field from '@/components/field'
import Notification from '@/components/notification'
import { withLocalize } from 'react-localize-redux'
import Button from '@/components/button'
import SelectFlag from './select-flag'

const { changePassword } = actions;
const ChangePassword = ({ history, translate }) => {
  let email = (process.browser && new URL(window.location.href).searchParams.get('email')) || '';
  let code = (process.browser && new URL(window.location.href).searchParams.get('code')) || '';
  const [isLoading, setIsLoading] = useState(false)

  const dispatch = useDispatch();
  const _onSubmit = (values) => {
    const body = {
      newPassword: values.newPassword,
      email, code
    }
    setIsLoading(true)
    dispatch(
      changePassword(body, (action, data) => {
        setIsLoading(false)
        if (action === TYPES.CHANGE_PASSWORD_SUCCESS) {
          history.push('/login')
          return Notification.success(translate(`success.CHANGE_PASSWORD_SUCCESS`))
        }
      })
    )
  }

  const _renderForm = ({ handleSubmit, ...form }) => {
    return (
      <div className=" login_page login-bg">
        <SelectFlag/>
        <div className="container">
          <div className="row">
            <div className=" mt-90 col-lg-8 col-lg-offset-2">
              <div className="row">
                <div className="login-wrapper crypto">
                  <div className="col-lg-5 col-sm-12 hidden-sm no-padding-left  no-padding-right">
                    <img src='/data/crypto-dash/login-img.png' alt="" />
                  </div>

                  <div className="col-lg-7 col-sm-12">
                    <div id="login" className="login loginpage mt-0 no-pl no-pr pt30 pb30">
                      <div className="login-form-header  flex align-items-center">
                        <img src='/data/crypto-dash/padlock.png' alt="login-icon" style={{ maxWidth: `64px` }} />
                        <div className="login-header">
                          <h4 className="bold">{translate(`utils.change-password`)}</h4>
                        </div>
                      </div>

                      <div className="box login">

                        <div className="content-body" style={{ paddingTop: `30px` }}>

                          <Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
                            <div className="row">
                              <div className="col-xs-12">

                                <div className="form-group form-login">
                                  <label className="form-label">{translate(`utils.password`)}</label>
                                  <div className="controls">
                                    <Field
                                      form={form}
                                      name="newPassword"
                                      label="newPassword"
                                      type="password"
                                      component={Input}
                                    />
                                  </div>
                                </div>

                                <div className="form-group form-login">
                                  <label className="form-label">{translate(`utils.confirm-password`)}</label>
                                  <div className="controls">
                                    <Field
                                      form={form}
                                      name="confirmPassword"
                                      label="confirmPassword"
                                      type="password"
                                      component={Input}
                                    />
                                  </div>
                                </div>

                                <div className="text-center">
                                  <Button
                                    className="btn btn-primary mt-10 btn-corner right-15"
                                    onClick={handleSubmit}
                                    loading={isLoading}
                                    disabled={isLoading}
                                  >{translate(`utils.save`)}</Button>
                                </div>

                              </div>
                            </div>
                          </Form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const initialValues = {
    newPassword: '',
    confirmPassword: '',
  }

  const validationSchema = object().shape({
    newPassword: string().required().nullable(),
    confirmPassword: string().required().oneOf([ref('newPassword'), null], 'Passwords must match'),
  })

  return (
    <Page>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={_onSubmit}
        component={_renderForm}
      />
    </Page>
  )
}

export default withLocalize(ChangePassword)
