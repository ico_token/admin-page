import React, { Component } from 'react'
import { Formik, Form } from 'formik'
import { object, string } from 'yup'
import styled from 'styled-components'
import { useSelector, useDispatch } from 'react-redux';

import Request from '@/utils/request'
import Storage from '@/utils/storage'
import { Images } from '@/theme'
import { TYPES, actions } from '@/store/actions'
import Container from '@/components/container'
import Input from '@/components/input'
import Button from '@/components/button'
import Page from '@/components/page'
import Field from '@/components/field'

const { login } = actions;

const Register = ({ history }) => {

  const { submitting } = useSelector(state => state.account);
  const dispatch = useDispatch();
  const _onSubmit = (values) => {
    dispatch(
      login(values, (action, data) => {
        if (action === TYPES.LOGIN_SUCCESS) {
          Storage.set('ACCESS_TOKEN', data.token)
          Request.setAccessToken(data.token)
          history.push('/')
        }
      })
    )
  }

  const _renderForm = ({ handleSubmit, ...form }) => {
    return (
      <div className="login_page login-bg">
        <div className="container">
          <div className="row">

            <div className=" mt-90 col-lg-8 col-lg-offset-2">
              <div className="row">
                <div className="login-wrapper crypto">
                  <div className="col-lg-6 col-sm-12 over-h hidden-sm no-padding-left  no-padding-right">
                    <img src="/data/crypto-dash/login-img.png" style={{ width: `105%` }} alt="" />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <div id="login" className="login loginpage mt-0 no-pl no-pr pt30 pb30">
                      <div className="login-form-header flex align-items-center">
                        <img src="/data/crypto-dash/signup.png" alt="login-icon" style={{ maxWidth: `64px` }} />
                        <div className="login-header">
                          <h4 className="bold">Signup Now!</h4>
                          <h4><small>Enter your data to register.</small></h4>
                        </div>
                      </div>

                      <div className="box login">
                        <div className="content-body" style={{paddingTop:`25px`}}>
                          <Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
                            <div className="row">
                              <div className="col-xs-12">

                                <div className="form-group">
                                  <label className="form-label">User name</label>
                                  <div className="controls">
                                    <input type="text" className="form-control" name="formfield2" placeholder="enter username" />
                                  </div>
                                </div>
                                <div className="form-group">
                                  <label className="form-label">Email</label>
                                  <div className="controls">
                                    <input type="text" className="form-control" name="formfield1" placeholder="enter email" />
                                  </div>
                                </div>
                                <div className="form-group">
                                  <label className="form-label">Password</label>
                                  <div className="controls">
                                    <input type="password" className="form-control" name="formfield2" placeholder="enter password" />
                                  </div>
                                </div>
                                <div className="form-group">
                                  <label className="form-label">Repeat Password</label>
                                  <div className="controls">
                                    <input type="password" className="form-control" name="formfield1" placeholder="repeat password" />
                                  </div>
                                </div>
                                <div className="text-center">
                                  <a href="/" className="btn btn-primary mt-10 btn-corner right-15">Sign up</a>
                                  <a href="/login" className="btn mt-10 btn-corner signup">Login</a>
                                </div>

                              </div>
                            </div>
                          </Form>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const initialValues = {
    userCode: '',
    password: ''
  }

  const validationSchema = object().shape({
    userCode: string().required(),
    password: string().required()
  })

  return (
    <Page>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={_onSubmit}
        component={_renderForm}
      />
    </Page>
  )
}

export default Register
