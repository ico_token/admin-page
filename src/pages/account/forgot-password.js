import React, { useEffect, useState } from 'react'
import { Formik, Form } from 'formik'
import { object, string } from 'yup'
import { useDispatch } from 'react-redux';
import { TYPES, actions } from '@/store/actions'
import Input from '@/components/input'
import Page from '@/components/page'
import Field from '@/components/field'
import Notification from '@/components/notification'
import { withLocalize } from 'react-localize-redux'
import Button from '@/components/button'
import SelectFlag from './select-flag'

const { resetPassword } = actions;
const ForgotPassword = ({ translate }) => {
  const [isLoading, setIsLoading] = useState(false)

  const dispatch = useDispatch();
  const _onSubmit = (values) => {
    setIsLoading(true)
    dispatch(
      resetPassword(values, (action, data) => {
        setIsLoading(false)
        if (action === TYPES.RESET_PASSWORD_SUCCESS) {
          return Notification.success(translate(`success.RESET_PASSWORD_SUCCESS`))
        }
      })
    )
  }

  const _renderForm = ({ handleSubmit, ...form }) => {
    return (
      <div className=" login_page login-bg">
        <SelectFlag/>
        <div className="container">
          <div className="row">
            <div className=" mt-90 col-lg-8 col-lg-offset-2">
              <div className="row">
                <div className="login-wrapper crypto">
                  <div className="col-lg-5 col-sm-12 hidden-sm no-padding-left  no-padding-right">
                    <img src='/data/crypto-dash/login-img.png' alt="" />
                  </div>

                  <div className="col-lg-7 col-sm-12">
                    <div id="login" className="login loginpage mt-0 no-pl no-pr pt30 pb30">
                      <div className="login-form-header  flex align-items-center">
                        <img src='/data/crypto-dash/padlock.png' alt="login-icon" style={{ maxWidth: `64px` }} />
                        <div className="login-header">
                          <h4 className="bold">{translate(`account.forgotyour-password`)}</h4>
                          <h4><small>{translate(`account.enter-your-name-to-send-mail`)}</small></h4>
                        </div>
                      </div>

                      <div className="box login">

                        <div className="content-body" style={{ paddingTop: `30px` }}>

                          <Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
                            <div className="row">
                              <div className="col-xs-12">

                                <div className="form-group form-login">
                                  <label className="form-label">{translate(`utils.name`)}</label>
                                  <div className="controls">
                                    <Field
                                      form={form}
                                      name="username"
                                      label="username"
                                      component={Input}
                                    />
                                    
                                  </div>
                                </div>

                                <div className="text-center">
                                  <Button
                                    className="btn btn-primary mt-10 btn-corner right-15"
                                    onClick={handleSubmit}
                                    loading={isLoading}
                                    disabled={isLoading}
                                  >{translate(`account.send-mail`)}</Button>
                                </div>

                              </div>
                            </div>
                          </Form>
                        </div>
                      </div>
                      <p id="nav" className="over-h">
                        <a className="pull-left blue-text" href="/login">{translate("account.log-in")}</a>
                      </p>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const initialValues = {
    username: '',
  }

  const validationSchema = object().shape({
    username: string().required()
  })

  return (
    <Page>
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={_onSubmit}
        component={_renderForm}
      />
    </Page>
  )
}

export default withLocalize(ForgotPassword)
