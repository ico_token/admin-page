import React, { useState, useEffect } from 'react'
import Storage from '@/utils/storage'
import { withRouter } from "react-router-dom"
import { withLocalize } from 'react-localize-redux'
import iconVI from '../../../public/assets/images/vn.svg'
// import iconEN from '../../public/assets/images/gb.svg'
import iconJP from '../../../public/assets/images/jp.svg'
import './style.scss'

const SelectFlag = ({ setActiveLanguage, translate }) => {
  const language = [
    {
      name: translate('utils.vi'),
      code: 'vi',
      icon: iconVI
    },
    // {
    //   name: 'English',
    //   code: 'en',
    //   icon: iconEN
    // }, 
    {
      name: translate('utils.ja'),
      code: 'jp',
      icon: iconJP
    }
  ]
  const [selectedLanguage, setSelectedLanguage] = useState(language[0])

  const currentLanguage = Storage.get('LANGUAGE')

  useEffect(() => {
    if (currentLanguage) {
      const languageSelect = language.find((e) => e.code === currentLanguage)
      setActiveLanguage(languageSelect?.code)
      setSelectedLanguage(languageSelect)
    }
  }, [currentLanguage])

  const handleChangeLanguage = (value) => {
    Storage.set('LANGUAGE', value?.code)
    setActiveLanguage(value?.code)
    setSelectedLanguage(value)
  }


  return (
    <div className="pull-right form-select-flag">
      <ul className="info-menu right-links list-inline list-unstyled">

        <li className="profile">
          <a href="#" data-toggle="dropdown" className="toggle">
            <img
              src={selectedLanguage?.icon}
              alt=""
              style={{
                height: '20px',
                objectFit: 'cover',
                boxShadow: '0 0 0 1px rgb(0 0 0 / 10%), 0 4px 16px rgb(0 0 0 / 10%)'
              }}
            />
          </a>
          <ul className="dropdown-menu profile animated fadeIn">
            {language.map((e) => (
              <li key={e.code} onClick={() => handleChangeLanguage(e)}>
                <a className="flag-item">
                  <img
                    src={e.icon}
                    alt=""
                    style={{
                      height: '20px',
                      objectFit: 'cover',
                      boxShadow: '0 0 0 1px rgb(0 0 0 / 10%), 0 4px 16px rgb(0 0 0 / 10%)'
                    }}
                  /> {e.name}
                </a>
              </li>
            ))}
          </ul>
        </li>
      </ul>
    </div>
  )
}

export default withLocalize(withRouter(SelectFlag))
