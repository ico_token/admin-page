import React, { useEffect, useState } from 'react';
import Modal from '@/components/modal'
import Input from '@/components/input'
import Field from '@/components/field'
import Select from '@/components/select'
import Button from '@/components/button'
import Notification from '@/components/notification'
import { Formik, Form } from 'formik'
import { object, number } from 'yup'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
import { OPTION_MONTH } from '@/utils/constant'
import { Collapse } from 'antd';
import 'antd/es/collapse/style/css'
import Popconfirm from '@/components/popconfirm'
import BigNumber from 'bignumber.js'

import './style.scss'
const { Panel } = Collapse;

const { createPacketDetail, updatePacketDetail, deletePacketDetail } = actions;
const ModalDetail = ({ close, isOpen, reload, packetUpdate, translate }) => {
	const [listDetail, setlistDetail] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [isLoadingDetail, setIsLoadingDetail] = useState(false)
	const dispatch = useDispatch();
	const { id, packet_details } = packetUpdate;

	useEffect(() => {
		setlistDetail(packet_details)
	}, [packet_details])


	const _onSubmit = (values) => {
		const payload = {
			...values,
			interest_rate: +values.interest_rate / 100
		}
		setIsLoading(true)
		dispatch(
			createPacketDetail({ payload, id }, (action) => {
				setIsLoading(false)
				if (action === TYPES.CREATE_PACKET_DETAIL_SUCCESS) {
					close()
					reload()
					return Notification.success(translate('success.CREATE_PACKET_DETAIL'))
				}
			})
		)
	}

	const handleDeleteDetail = ({id}) => {
		dispatch(
			deletePacketDetail({ id }, (action) => {
				if (action === TYPES.DELETE_PACKET_DETAIL_SUCCESS) {
					close()
					reload()
					return Notification.success(translate('success.DELETE_PACKET_DETAIL'))
				}
			})
		)
	}

	const handleChangeValueDetail = ({e,id}) => {
		const { value, name } = e.target;
		const newList = listDetail.map(item => {
			if(item.id === id) { 
				if(name === 'interest_rate') {
					return {...item, interest_rate: +new BigNumber(value).div(100)}
				}
				else {
					return {...item, [name]: value}
				}
			}
			else {return item}
		});
		setlistDetail(newList)
	}
	
	const handleUpdate = () => {
		const newData = listDetail.map(item => {
			delete item.is_deleted
			delete item.created_at
			delete item.updated_at
			delete item.packet_id
			return item
		})
		const param = {
			packet_details: newData
		}
		setIsLoadingDetail(true)
		dispatch(
			updatePacketDetail(param, (action) => {
				setIsLoadingDetail(false)
				if (action === TYPES.UPDATE_PACKET_DETAIL_SUCCESS) {
					close()
					reload()
					return Notification.success(translate('success.UPDATE_PACKET_DETAIL'))
				}
			})
		)
	}

	const renderListDetail = () => {
		return (
			<div>
				<div className="col-xs-5 col-lg-4">
					<label className="form-label">{translate('packets.months')}</label>
				</div>

				<div className="form-login col-xs-6 col-lg-7">
					<label className="form-label">{translate('packets.interest-rate')}</label>
				</div>
				
				{listDetail.map((item, i) => (
					<div key={i}>
						<div className="form-login col-xs-5 col-lg-4 mb-10" key={i}>
							<div className="controls">
								<Select
									name="number_of_months"
									options={OPTION_MONTH}
									value={item.number_of_months}
									onChange={(e) => handleChangeValueDetail({e, id:item.id})}
								/>
							</div>
						</div>

						<div className="form-login col-xs-6 col-lg-7 mb-10">
							<div className="controls">
								<Input
									name="interest_rate"
									value={new BigNumber(item.interest_rate).multipliedBy(100)}
									type="number"
									suffix="%"
									onChange={(e) => handleChangeValueDetail({e, id:item.id})}
								/>
							</div>
						</div>

						<div className='col-xs-1 form-trash'>
							<Popconfirm
                title={translate('packets.delete-text')}
                onConfirm={() => handleDeleteDetail({id: item.id})}
                okText={translate('utils.yes')}
                cancelText={translate('utils.no')}
              >
                <a className="box_setting fa fa-trash ml-5"
									title={translate('packets.delete-detail-packet')}
								></a>
              </Popconfirm>
						</div>
					</div>
				))}

				<div className="text-center">
					<Button
						className="btn btn-primary btn-corner"
						onClick={handleUpdate}
						loading={isLoadingDetail}
						disabled={isLoadingDetail}
					>{translate('utils.update')}</Button>
				</div>
			</div>
		)
	}

	const _renderForm = ({ handleSubmit, ...form }) => {
		return (
			<div>
				<Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
					<div>
						<div className="form-login col-xs-6 col-lg-4">
							<label className="form-label">{translate('packets.months')}</label>
							<div className="controls">
								<Field
									form={form}
									name="number_of_months"
									options={OPTION_MONTH}
									component={Select}
								/>
							</div>
						</div>

						<div className="form-login col-xs-6 col-lg-8">
							<label className="form-label">{translate('packets.interest-rate')}</label>
							<div className="controls">
								<Field
									form={form}
									name="interest_rate"
									type="number"
									suffix="%"
									component={Input}
								/>
							</div>
						</div>

						<div className="text-center">
							<Button
								className="btn btn-primary btn-corner"
								onClick={handleSubmit}
								loading={isLoading}
								disabled={isLoading}
							>{translate('utils.create')}</Button>
						</div>
					</div>
				</Form>
			</div>
		)
	}

	const validationSchema = object().shape({
		number_of_months: number().required(),
		interest_rate: number().required()
	})
	return (
		<Modal
			visible={isOpen}
			onCancel={close}
			title={translate('packets.update-detail')}
			destroyOnClose={true}
		>
			<div>
				<Collapse >
					<Panel header={translate('packets.create-packet-detail')}>
						<Formik
							validateOnChange={false}
							validateOnBlur={false}
							validationSchema={validationSchema}
							onSubmit={_onSubmit}
							component={_renderForm}
						/>
					</Panel>

					{listDetail?.length > 0 &&
						<Panel header="Update Detail Packets">{renderListDetail()}</Panel>
					}
				</Collapse>
			</div>

		</Modal>

	)
}

export default ModalDetail