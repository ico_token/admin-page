import React, { useState } from 'react';
import Modal from '@/components/modal'
import Input from '@/components/input'
import Field from '@/components/field'
import Notification from '@/components/notification'
import Button from '@/components/button'
import { Formik, Form } from 'formik'
import { object, string, number, ref } from 'yup'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

const { createPacket, updatePacket } = actions;
const ModalCreate = ({ close, isOpen, reload, packetUpdate, translate }) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false)

	const _onSubmit = (values) => {
		const payload = {...values} 
		if(!payload.max_budget) {
			payload.max_budget = null
		}
		if(!_.isEmpty(packetUpdate)) {
			const { id } = packetUpdate
			setIsLoading(true)
			dispatch(
				updatePacket({payload, id}, (action) => {
					setIsLoading(false)
					if (action === TYPES.UPDATE_PACKET_SUCCESS) {
						reload()
						close()
						return Notification.success(translate('success.UPDATE_PACKET'))
					}
				})
			)
		} else {
			dispatch(
				createPacket(payload, (action) => {
					setIsLoading(false)
					if (action === TYPES.CREATE_PACKET_SUCCESS) {
						reload()
						close()
						return Notification.success(translate('success.CREATE_PACKET'))
					}
				})
			)
		}
  }

	const _renderForm = ({ handleSubmit, ...form }) => {
		return (
			<div>
				<Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
					<div className="row">
						<div className="col-xs-12">
							<div className="form-login">
								<label className="form-label">{translate('utils.name')}</label>
								<div className="controls">
									<Field
										form={form}
										name="name"
										component={Input}
									/>
								</div>
							</div>

							<div className="form-login">
								<label className="form-label">{translate('packets.min-budget')}</label>
								<div className="controls">
									<Field
										form={form}
										name="min_budget"
										type="number"
										component={Input}
									/>
								</div>
							</div>

							<div className="form-login">
								<label className="form-label">{translate('packets.max-budget')}</label>
								<div className="controls">
									<Field
										form={form}
										name="max_budget"
										type="number"
										component={Input}
									/>
								</div>
							</div>

							<div className="text-center">
								<Button
									className="btn btn-primary btn-corner"
									onClick={handleSubmit}
									loading={isLoading}
									disabled={isLoading}
								>{ !_.isEmpty(packetUpdate) ? translate('utils.update') : translate('utils.create') }
								</Button>
							</div>
						</div>
					</div>
				</Form>
			</div>
		)
	}
	const initialValues = {
		name: packetUpdate.name || '',
		min_budget: packetUpdate.min_budget || '',
		max_budget: packetUpdate.max_budget || ''
	}

	const validationSchema = object().shape({
		name: string().required(),
		min_budget: number().required()
		.typeError('validation.number')
    .positive('must-be-greater-0'),
		max_budget: number()
		.typeError('validation.number')
    .positive('must-be-greater-0')
		.moreThan(
			ref("min_budget"),'more-than-min-budget'
		)
	})
	return (
		<Modal
			visible={isOpen}
			onCancel={close}
			title={ !_.isEmpty(packetUpdate) ? translate('packets.update-packets') : translate('packets.create-packets')}
			destroyOnClose={true}
		>
			<Formik
				validateOnChange={false}
				validateOnBlur={false}
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={_onSubmit}
				component={_renderForm}
				enableReinitialize
			/>
		</Modal>

	)
}

export default ModalCreate