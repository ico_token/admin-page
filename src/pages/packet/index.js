import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { TYPES, actions } from '../../store/actions'
import TitlePage from '../../components/title-page'
import './style.scss'
import ModalHandlePacket from './modal-handle-packet'
import ModalHandleDetail from './modal-handle-detail'
import BigNumber from 'bignumber.js'
import { withLocalize } from 'react-localize-redux'

const { getPacketList } = actions

const Packet = ({translate}) => {
	const dispatch = useDispatch()

	const [listPacket, setListPacket] = useState([])
	const [openModalPacket, setOpenModalPacket] = useState(false)
	const [openModalDetail, setOpenModalDetail] = useState(false)
	const [packetUpdate, setPacketUpdate] = useState({})

	const handleGetPacketList = () => {
		dispatch(getPacketList({}, (action, data) => {
			if (action === TYPES.GET_PACKET_LIST_SUCCESS) {
				setListPacket(data.rows)
			}
		}))
	}

	const reload = () => {
		handleGetPacketList()
	}

	useEffect(() => {
		handleGetPacketList()
	}, [])

	const handleOpenModalDetail = (item) => {
		setPacketUpdate(item)
		setOpenModalDetail(true)
	}

	const handleOpenModalPacket = (item) => {
		if (item) {
			setPacketUpdate(item)
		}
		setOpenModalPacket(true)
	}

	const closeModal = () => {
		setOpenModalPacket(false)
		setOpenModalDetail(false)
		setPacketUpdate({})
	}

	return (
		<div>
			<TitlePage namePage={translate('packets.packets')} />
			<div>
				<div className='col-xs-6 col-lg-4'>
					<div className="img-affa-wrapper text-center no-mb form-add" style={{ padding: "25px 20px" }}
						onClick={() => handleOpenModalPacket()}
					>
						<div className="">
							<i className="fa fa-plus has-gradient-to-right-bottom icon-sm icon-rounded inviewport visible "></i>
							<a><h4 className="no-mb mt-20">{translate('packets.create-packets')}</h4></a>
						</div>
					</div>
				</div>
				{
					listPacket.map((item, i) => {
						return (
							<div className="col-xs-6 col-lg-4" key={i}>
								<div className="statistics-box form-add">
									<div className="form-parkets">
										<div className="form-header">
											<span className="boldy mb-5 text-name">{item.name}</span>
											<div>
												<a className="box_setting fa fa-cog"
													title={translate('packets.update-packets')}
													onClick={() => handleOpenModalPacket(item)}
												></a>

												{
													item?.packet_details.length > 0 &&
													<a className="box_setting fa fa-bars ml-5"
														title={translate('packets.update-detail')}
														onClick={() => handleOpenModalDetail(item)}
													></a>
												}
											</div>
										</div>
										<div className='content-packet'>
											<h4 className="green-text">{item?.max_budget ? `$${item.min_budget} – $${item.max_budget}` : `>= $${item.min_budget}`}</h4>

											{
												item?.packet_details.length > 0 ?
													item?.packet_details?.map((elm, index) =>
														<p className="mb-5" key={index}>{`${elm.number_of_months} tháng ${new BigNumber(elm.interest_rate).multipliedBy(100)}%`}</p>
													)
													:
													<button className="btn btn-success btn-icon bottom15 right15"
														onClick={() => handleOpenModalDetail(item)}
													>
														<i className="fa fa-plus"></i> &nbsp; <span>{translate('packets.create-packet-detail')}</span>
													</button>
											}
										</div>
									</div>
								</div>
							</div>
						)
					})
				}
			</div>

			<ModalHandlePacket
				close={() => closeModal()}
				packetUpdate={packetUpdate}
				isOpen={openModalPacket}
				translate={translate}
				reload={() => reload()}
			/>

			<ModalHandleDetail
				close={() => closeModal()}
				packetUpdate={packetUpdate}
				isOpen={openModalDetail}
				translate={translate}
				reload={() => reload()}
			/>
		</div>
	)
}

export default withLocalize(Packet)
