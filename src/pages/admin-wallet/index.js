import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux'
import { withLocalize } from 'react-localize-redux'
import ModalHandleWallet from './modal-handle-wallet'
import Notification from '@/components/notification'
import Transaction from '@/components/transaction';
import TitlePage from '@/components/title-page'
import { COIN_SYSTEM, ICON_COIN } from '@/utils/constant'
import { TYPES, actions } from '@/store/actions'
import QRCode from 'qrcode.react';
import _ from 'lodash';

const { getAdminWallet, getListCoin } = actions

const CryptoWallet = ({translate}) => {
  const dispatch = useDispatch()
  const [listWallet, setListWallet] = useState([])
  const [listCoins, setListCoins] = useState([])
  const [coinActive, setCoinActive] = useState({})
  const [openModalWallet, setOpenModalWallet] = useState(false)
  const [walletUpdate, setWalletUpdate] = useState({})

  const handleOpenModalWallet = (item) => {
    setOpenModalWallet(true)
    setWalletUpdate(item)
  }

  const copyToClipboard = () => {
    const copyText = document.getElementById("address-copy");
    copyText.select();
    document.execCommand("copy");
    return Notification.success(translate('success.copy'))

  };

  const handleGetListCoin = () => {
    dispatch(getListCoin({}, (action, data) => {
      if (action === TYPES.GET_LIST_COIN_SUCCESS) {
        setListCoins(data)
      }
    }))
  }

  const handleGetAdminWallet = () => {
    dispatch(getAdminWallet({}, (action, data) => {
      if (action === TYPES.GET_ADMIN_WALLET_SUCCESS) {
        setListWallet(data)
      }
    }))
  }

  const reload = () => {
    handleGetAdminWallet()
  }

  useEffect(() => {
    handleGetAdminWallet()
    handleGetListCoin()
  }, [])

  const renderBgCoin = (code) => {
    switch (code) {
      case 'BTC':
        return 'gradient-blue'
      case 'ETH':
        return 'gradient-pink'
      case 'XRP':
        return 'gradient-violet'
      case 'TSI':
        return 'gradient-grep'
      case 'BNB':
        return 'gradient-green'
      case 'USDT':
        return 'gradient-blue1'
      default:
        return 'orange-bg'
    }
  }

  return (
    <div>
      <TitlePage namePage={translate('wallet.admin-wallet')} />
      {
        listWallet.map((item, index) => (
          <div className="col-lg-3 col-sm-6 col-xs-12" key={index}>
            <div className={`box-wallet statistics-box text-center mt-15 ${renderBgCoin(item.coin.code)}`}>
              <a className="box_setting fa fa-cog"
                onClick={() => handleOpenModalWallet(item)}
              ></a>
              <div className="crypto-icon mb-15">
                {
                  item.coin.code === COIN_SYSTEM.CODE
                    ? <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system mr-1" />
                    : <i className={ICON_COIN[item.coin.code]?.COLOR} />
                }
              </div>
              <h4 className="coin-name boldy ">{item.coin.name}</h4>
            </div>
          </div>
        ))
      }

      <div className="col-lg-3 col-sm-6 col-xs-12" >
        <div className="img-affa-wrapper text-center no-mb mt-15" style={{ padding: '25px 20px' }}
          onClick={() => handleOpenModalWallet()}
        >
          <div className="">
            <i className="fa fa-plus has-gradient-to-right-bottom icon-sm icon-rounded inviewport visible " style={{ width: '50px', height: '50px', lineHeight: '25px' }} ></i>
            <a><h4 className="no-mb mt-20">{translate('wallet.create-wallet')}</h4></a>
          </div>
        </div>
      </div>


      <div className="clearfix"></div>

      <div className="col-lg-12">
        <section className="box ">
          <header className="panel_header">
            <h2 className="title pull-left">{translate('wallet.address-wallet-text')}</h2>
          </header>
          <div className="content-body">
            <div className="row">
              <div className="col-md-12">

                <div className="row tabs-area">
                  <ul className="nav nav-tabs crypto-wallet-address vertical col-xs-4 col-md-3 left-aligned primary">
                    {
                      listWallet.map((item, i) => (
                        <li className={`${(coinActive.coin_id === item.coin_id) && 'active'} text-center relative`} key={i}
                          onClick={() => setCoinActive(item)}
                        >
                          <a>
                            {
                              item.coin.code === COIN_SYSTEM.CODE
                                ? <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system mr-1" />
                                : <i className={ICON_COIN[item.coin.code]?.COLOR} />
                            }
                            <h4>{item.coin.name}</h4>
                          </a>
                          <div className="check-span"><i className="fa fa-check"></i></div>
                        </li>
                      ))
                    }
                  </ul>
                  {
                    !_.isEmpty(coinActive) &&
                    <div className="tab-content wallet-address-tab vertical col-xs-12 left-aligned primary" style={{ paddingRight: '0px' }}>
                      <div className="tab-pane fade active in">
                        <div className="row">
                          <div className="col-xs-12 col-md-8">
                            <div className="option-identity-wrapper">
                              <h3 className="boldy mt-0">{`${coinActive.coin.code}`}</h3>
                              <div className="row">
                                <div className="col-lg-8">
                                  <div className="form-group mb-0">
                                    <label className="form-label mb-10">{translate('wallet.address-wallet')}</label>
                                    <span className="desc ">{translate('wallet.text-address')}</span>
                                    <div className="input-group primary">
                                      <input 
                                      type="text" className="form-control"
                                      id='address-copy'
                                      readOnly
                                      value={coinActive.address} />
                                    </div>

                                  </div>
                                </div>
                                <div className="col-lg-4 no-pl mt-30">
                                  <a onClick={() => copyToClipboard()} className="btn btn-primary btn-corner"><i className="fa fa-copy"></i></a>
                                  {/* <a href="#" className="btn btn-primary btn-corner right15">Generate New</a> */}
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-4 col-xs-12">
                            <div className="option-identity-wrapper form-qr">
                              <div className="option-icon">
                                <QRCode
                                  className="tab-img-icon"
                                  value= {coinActive.address ||''}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  }
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-12">
          <Transaction
            params={{type: 5}}
          />

          </div>
        </section>
      </div>
      <ModalHandleWallet
        close={() => setOpenModalWallet(false)}
        isOpen={openModalWallet}
        walletUpdate={walletUpdate}
        listCoins={listCoins}
        reload={() => reload()}
      />

    </div>

  )
}

export default withLocalize(CryptoWallet)
