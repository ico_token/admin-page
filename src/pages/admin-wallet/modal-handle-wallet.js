import React, { useState } from 'react';
import { useDispatch } from 'react-redux'
import Modal from '@/components/modal'
import Input from '@/components/input'
import SelectCoin from '@/components/select-coin'
import Field from '@/components/field'
import Notification from '@/components/notification'
import CodeInput from '@/components/code-input'
import Button from '@/components/button'
import { TYPES, actions } from '@/store/actions'
import { Formik, Form } from 'formik'
import { withLocalize } from 'react-localize-redux'
import { object, string } from 'yup'
import _ from 'lodash'
import './style.scss'

const { createWallet, sendOtpConfirmWallet } = actions;
const ModalCreate = ({ close, isOpen, reload, walletUpdate, listCoins, translate }) => {
	const dispatch = useDispatch();
	const [statusRenderFormOtp, setStatusRenderFormOtp] = useState(false)
	const [valueSubmit, setValueSubmit] = useState({})
  const [isLoadingOTP, setIsLoadingOTP] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

	const _close = () => {
		close()
		setValueSubmit({})
		setStatusRenderFormOtp(false)
	}

	const _onSubmit = (values) => {
		setIsLoadingOTP(true)
		dispatch(
			sendOtpConfirmWallet(values, (action) => {
				setIsLoadingOTP(false)
				if (action === TYPES.SEND_OTP_CONFIRM_WALLET_SUCCESS) {
					setValueSubmit(values)
					setStatusRenderFormOtp(true)
					return Notification.success(translate('success.SEND_OTP_CONFIRM_WALLET_SUCCESS'))
				}
			})
		)
	}

	const handleCreateWallet = () => {
		setIsLoading(true)
		dispatch(
			createWallet(valueSubmit, (action) => {
				setIsLoading(false)
				if (action === TYPES.CREATE_WALLET_SUCCESS) {
					reload()
					close()
					setStatusRenderFormOtp(false)
					return Notification.success(!_.isEmpty(walletUpdate) ? translate('success.update_wallet') : translate('success.create_wallet'))
				}
			})
		)
	}

	const renderFormOtp = () => {
		return (
			<div className="form-otp">
				<div className="form-login col-xs-12 col-sm-6 text-center auto-margin">
					<label className="form-label text-center">OTP</label>
					<div className="controls mb-15 ">
						<CodeInput
							type="number"
							count={4}
							onInputNotFinish={() => {
								const data = { ...valueSubmit }
								delete data.otp_code
								setValueSubmit(data)
							}}
							onInputFinish={(value) => {
								setValueSubmit({ ...valueSubmit, otp_code: value })
							}}
							onInputChange={(value) => {
								setValueSubmit({ ...valueSubmit, otp_code: value })
							}}
						/>
					</div>
				</div>

				<div className="text-center">
					<Button
						className="btn btn-primary btn-corner"
						onClick={handleCreateWallet}
						loading={isLoading}
						disabled={isLoading}
					>{!_.isEmpty(walletUpdate) ? translate('utils.update') : translate('utils.create')}</Button>
				</div>
			</div>

		)
	}

	const _renderForm = ({ handleSubmit, ...form }) => {
		return (
			<div>
				<Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
					<div className="row">
						<div className="col-xs-12">

							<div className="form-login">
								<label className="form-label">{translate('coins.coins')}</label>
								<div className="controls">
									<Field
										form={form}
										name="coin_id"
										options={listCoins}
										component={SelectCoin}
										disabled={!_.isEmpty(walletUpdate)}
									/>
								</div>
							</div>

							<div className="form-login">
								<label className="form-label">{translate('utils.address')}</label>
								<div className="controls">
									<Field
										form={form}
										name="address"
										component={Input}
									/>
								</div>
							</div>

							<div className="text-center">
								<Button
									className="btn btn-primary btn-corner"
									onClick={handleSubmit}
									loading={isLoadingOTP}
									disabled={isLoadingOTP}
								>{translate('wallet.send-otp')}</Button>
							</div>
						</div>
					</div>
				</Form>
			</div>
		)
	}
	const initialValues = {
		coin_id: !_.isEmpty(walletUpdate) ? walletUpdate.coin_id : '',
		address: !_.isEmpty(walletUpdate) ? walletUpdate.address : ''
	}
	const validationSchema = object().shape({
		coin_id: string().required(),
		address: string().required()
	})
	return (
		<Modal
			visible={isOpen}
			onCancel={_close}
			title={!_.isEmpty(walletUpdate) ? translate('wallet.update-wallet') : translate('wallet.create-wallet')}
			destroyOnClose={true}
		>

			{
				!statusRenderFormOtp ?
					<Formik
						validateOnChange={false}
						validateOnBlur={false}
						initialValues={initialValues}
						validationSchema={validationSchema}
						onSubmit={_onSubmit}
						component={_renderForm}
						enableReinitialize
					/> : renderFormOtp()
			}

		</Modal>

	)
}

export default withLocalize(ModalCreate)