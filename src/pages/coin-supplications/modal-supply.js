import React, { useState } from 'react';
import Modal from '@/components/modal'
import Input from '@/components/input'
import TextArea from '@/components/textarea'
import Field from '@/components/field'
import DatePicker from '@/components/date-picker'
import Notification from '@/components/notification'
import Button from '@/components/button'
import { Formik, Form } from 'formik'
import { object, string, number, date, ref } from 'yup'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux'
import { FROM_TIME } from '@/utils/constant'
import _ from 'lodash'
import moment from 'moment'
import BigNumber from 'bignumber.js'

const { supplyTosiToUser } = actions;
const ModalCreate = ({ close, isOpen, reload, itemSupply, translate }) => {
	const { user_info: { id }, coin_supplication } = itemSupply
	const today = moment().format(FROM_TIME);
	const dispatch = useDispatch();
	const [isLoading, setIsLoading] = useState(false)

	const _onSubmit = (values) => {
		const payload = {
			...values,
			end_date: moment(values.end_date).format(FROM_TIME),
			start_date: moment(values.start_date).format(FROM_TIME),
			number_of_tosi: new BigNumber(values.number_of_tosi)
		}
		setIsLoading(true)
		dispatch(
			supplyTosiToUser({ payload, id }, (action) => {
				setIsLoading(false)
				if (action === TYPES.SUPPLY_TOSI_TO_USER_SUCCESS) {
					reload()
					close()
					return Notification.success(translate('success.SUPPLY_TOSI'))
				}
			})
		)
	}

	const _renderForm = ({ handleSubmit, setFieldValue, ...form }) => {
		return (
			<div>
				<Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
					<div className="row">
						<div className="col-xs-12">
							<div className="form-login">
								<label className="form-label">{translate('coin-supply.amount-tsi')}</label>
								<div className="controls">
									<Field
										form={form}
										name="number_of_tosi"
										type="number"
										component={Input}
									/>
								</div>
							</div>

							{
								(!coin_supplication || moment(coin_supplication?.end_date).isBefore(today)) &&
								<div className="form-login">
									<label className="form-label">{translate('coin-supply.date-supply')}</label>
									<div className="controls">
										<Field
											form={form}
											name="start_date"
											className="form-control"
											style={{ border: 'none' }}
											value={form.values.start_date}
											onChange={(e) => {
												setFieldValue('start_date', e.target.value)
												setFieldValue('end_date', moment(e.target.value).add(6, 'months').add(2,'days'))
											}}
											component={DatePicker}
										/>
									</div>
								</div>
							}

							<div className="form-login">
								<label className="form-label">{translate('coin-supply.date-recall')}</label>
								<div className="controls">
									<Field
										form={form}
										name="end_date"
										className="form-control"
										style={{ border: 'none' }}
										value={form.values.end_date}
										onChange={(e) => setFieldValue('end_date', e.target.value)}
										component={DatePicker}
									/>
								</div>
							</div>

							<div className="form-login">
								<label className="form-label">{translate('coin-supply.note')}</label>
								<div className="controls">
									<Field
										form={form}
										name="note"
										component={TextArea}
									/>
								</div>
							</div>

							<div className="text-center">
								<Button
									className="btn btn-primary btn-corner"
									onClick={handleSubmit}
									loading={isLoading}
									disabled={isLoading}
								>{translate('coin-supply.supply')}
								</Button>
							</div>
						</div>
					</div>
				</Form>
			</div>
		)
	}

	const initialValues = {
		number_of_tosi: coin_supplication?.number_of_tosi ? coin_supplication.number_of_tosi : '',
		start_date: coin_supplication?.start_date ? moment(coin_supplication.start_date) : '',
		end_date: coin_supplication?.end_date ? moment(coin_supplication.end_date) : '',
		note: coin_supplication?.note ? coin_supplication.note : '',
	}
	const validationNotStartDate = object().shape({
		number_of_tosi: number().required().test(
			'is-decimal',
			'invalid decimal',
			value => (value + "").match(/^[0-9]\d*$/)),
		end_date: date().required()
			.min(new Date(moment(coin_supplication?.end_date).format(FROM_TIME)), 
			'end-date-must-be-more-than-end-date-before'),
		note: string()
	})

	const validation = object().shape({
		number_of_tosi: number().required().test(
			'is-decimal',
			'invalid decimal',
			value => (value + "").match(/^[0-9]\d*$/)),
		start_date: date().required().min(new Date(), 'start-date-must-be-later-than-today'),
		end_date: date().required()
			.min(new Date(moment(ref("start_date")).add(6, 'months').format(FROM_TIME)), 
			'end-date-must-be-more-than-start-date-6-month'),
		note: string()
	})
	return (
		<Modal
			visible={isOpen}
			onCancel={close}
			title={`${translate('coin-supply.supply')} TSI`}
			destroyOnClose={true}
		>
			<Formik
				validateOnChange={false}
				validateOnBlur={false}
				initialValues={initialValues}
				validationSchema={
					(!coin_supplication || moment(coin_supplication?.end_date).isBefore(today)) ?
						validation : validationNotStartDate
				}
				onSubmit={_onSubmit}
				component={_renderForm}
				enableReinitialize
			/>
		</Modal>

	)
}

export default ModalCreate