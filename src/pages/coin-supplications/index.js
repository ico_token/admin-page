import React, { useEffect, useState } from 'react';
import Input from '@/components/input'
import Button from '@/components/button'
import { withLocalize } from 'react-localize-redux'
import Pagination from '@/components/pagination'
import Select from '@/components/select'
import TitlePage from '@/components/title-page'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { FROM_TIME } from '@/utils/constant'
import ModalSupply from './modal-supply'
import { Collapse } from 'antd';
import 'antd/es/collapse/style/css'
import './style.scss'
import Popconfirm from '@/components/popconfirm'
import Notification from '@/components/notification'
const { Panel } = Collapse;

const { getListCoinSupplications, recallSuppliedTosiOfUser } = actions;
const CoinSupplications = ({ translate }) => {
  const mockSearch = {
    page: 1,
    limit: 10
  }

  const dispatch = useDispatch();
  const today = moment().format(FROM_TIME);
  const [listSupplications, setListSupplications] = useState([]);
  const [totalSupplications, setTotalSupplications] = useState(0);
  const [page, setPage] = useState(mockSearch.page);
  const [limit] = useState(mockSearch.limit);
  const [isLoading, setIsLoading] = useState(false)
  const [valueSearch, setValueSearch] = useState(mockSearch);
  const [openModalSupply, setOpenModalSupply] = useState(false)
  const [itemSupply, setItemSupply] = useState({})

  const closeModal = () => {
    setOpenModalSupply(false)
    setItemSupply({})
  }

  const handleopenModalSupply = ({ item }) => {
    setItemSupply(item)
    setOpenModalSupply(true)
  }

  useEffect(() => {
    getList(valueSearch)
  }, []);

  const reload = () => {
    getList(valueSearch)
  }

  const handleRecall = ({ item }) => {
    const { user_info: { id } } = item
    dispatch(
      recallSuppliedTosiOfUser({ id }, (action) => {
        if (action === TYPES.RECALL_SUPPLIED_TOSI_OF_USER_SUCCESS) {
          reload()
          return Notification.success(translate('success.RECALL_SUPPLIED'))
        }
      })
    )
  }

  const getList = (value) => {
    setIsLoading(true)
    dispatch(
      getListCoinSupplications(value, (action, data) => {
        setIsLoading(false)
        if (action === TYPES.GET_LIST_COIN_SUPPLICATIONS_SUCCESS) {
          setListSupplications(data.rows);
          setTotalSupplications(data.count);
        }
      })
    )
  };

  const onChangePage = (p) => {
    setPage(p);
    setValueSearch({ ...valueSearch, page: p })
    getList({ ...valueSearch, page: p })
  };

  const changeSearch = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setValueSearch({
      ...valueSearch,
      [name]: value.trim()
    })
  };

  const handleSearch = () => {
    getList(valueSearch)
  };

  const handleReset = () => {
    setValueSearch(mockSearch)
    getList(mockSearch)
  }

  return (
    <div>
      <TitlePage
        namePage={translate('coin-supply.coin-supplications')}
      />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12 mt-15">
            <Collapse >
              <Panel header={translate('utils.filter')}>
                <div>
                  <div className="field col-sm-12"	>
                    <label htmlFor="field1">{translate('utils.email')}</label>
                    <Input name="email" onChange={changeSearch} value={valueSearch.email} />
                  </div>
                </div>

                <div>
                  <Button
                    margin={true}
                    onClick={handleSearch}
                    loading={isLoading}
                    disabled={isLoading}
                  >{translate('utils.search')}</Button>

                  <Button
                    onClick={handleReset}
                    loading={isLoading}
                    disabled={isLoading}
                  >{translate('utils.reset')}</Button>

                </div>
              </Panel>
            </Collapse>
          </div>

          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header">
                <h2 className="title pull-left">{translate('coin-supply.list-supply')}</h2>
              </header>
              <div className="content-body">

                <div className="row">
                  <div className="col-xs-12">
                    <div className="table-responsive" data-pattern="priority-columns">
                      <table id="tech-companies-1" className="table vm trans table-small-font no-mb table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>{translate('utils.name')}</th>
                            <th>{translate('utils.email')}</th>
                            <th className="text-center">{translate('utils.amount')}</th>
                            <th className="text-center">{translate('coin-supply.date-supply')}</th>
                            <th className="text-center">{translate('coin-supply.date-recall')}</th>
                            <th>{translate('coin-supply.note')}</th>
                            <th className="text-center">{translate('utils.locked')}</th>
                            <th className="text-center"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            listSupplications.map((item, i) => (
                              <tr key={i}>
                                <td><small className="text-muted">{`${item.user_info.first_name || ''} ${item.user_info.last_name || ''}`}</small></td>
                                <td><small className="text-muted">{item.email}</small></td>
                                <td className="green-text boldy text-center">{item.coin_supplication?.number_of_tosi}</td>
                                <td className="text-center"><small className="text-muted">{item.coin_supplication?.start_date ? moment(item.coin_supplication.start_date).format(FROM_TIME) : ''}</small></td>
                                <td className="text-center"><small className="text-muted">{item.coin_supplication?.end_date ? moment(item.coin_supplication.end_date).format(FROM_TIME) : ''}</small></td>
                                <td><small className="text-muted">{item.coin_supplication?.note}</small></td>
                                <td className="text-center">
                                  {item?.is_locked ?
                                    <i className="fa fa-check-circle verification"></i>
                                    : <i className="fa fa-times-circle not-verification"></i>}
                                </td>
                                <td className="text-center">
                                  <div className="btn-group btn-group-justified btn-detail">
                                    {
                                      (!item.coin_supplication
                                        || moment(item.coin_supplication?.end_date).isSameOrAfter(today)
                                      ) &&
                                      <a className="btn btn-warning btn-xs"
                                        onClick={() => handleopenModalSupply({ item })}
                                      >{translate('coin-supply.supply')}</a>
                                    }

                                    {
                                      // !!item.coin_supplication
                                      // && moment(item.coin_supplication?.end_date).isBefore(today) &&
                                      <Popconfirm
                                        title={translate('coin-supply.confrim-recall')}
                                        onConfirm={() => handleRecall({ item })}
                                        okText={translate('utils.yes')}
                                        cancelText={translate('utils.no')}
                                      >
                                        <a className="btn btn-danger btn-xs">{translate('coin-supply.recall')}</a>
                                      </Popconfirm>
                                    }
                                  </div>
                                </td>
                              </tr>
                            ))
                          }
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </section>
          </div>
          {
            totalSupplications > 10 &&
            <div className="col-lg-12">
              <Pagination
                current={page} onChange={onChangePage}
                total={totalSupplications} pageSize={limit}
              />
            </div>
          }
        </section>
      </div>
      {
        openModalSupply &&
        <ModalSupply
          close={() => closeModal()}
          itemSupply={itemSupply}
          isOpen={openModalSupply}
          translate={translate}
          reload={() => reload()}
        />
      }

    </div>

  )
}

export default withLocalize(CoinSupplications)
