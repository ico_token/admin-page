import React, { useEffect, useState, Fragment } from 'react'
import { useDispatch } from 'react-redux'
import { withLocalize } from 'react-localize-redux'
import { Collapse } from 'antd'
import classNames from 'classnames'
import { actions, TYPES } from '@/store/actions'
import { RANK, RANK_IMG } from '@/utils/constant'
import TitlePage from '@/components/title-page'
import Input from '@/components/input'
import Button from '@/components/button'

import './style.scss'

const { Panel } = Collapse
const { getReferralTree } = actions
const Group = ({ translate }) => {
  const dispatch = useDispatch()
  const [listTree, setListTree] = useState([])
  const [originListTree, setOriginListTree] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [valueSearch, setValueSearch] = useState([])
  const [email, setEmail] = useState('')

  const getList = (value) => {
    dispatch(
      getReferralTree(value, (action, data) => {
        if (action === TYPES.GET_REFERRAL_TREE_SUCCESS) {
          setListTree(data)
          setOriginListTree(data)
        }
      })
    )
  }

  useEffect(() => {
    getList(valueSearch)
  }, [])

  const checkCon = (data, index) => {
    const { listOfRefSameLevel } = data
    let id
    for (let i = 0; i < listOfRefSameLevel?.length; i++) {
      if (listOfRefSameLevel[i]?.info?.email.includes(valueSearch?.email?.trim())) {
        return index
      }

      const { listOfRefNextLevel } = listOfRefSameLevel[i]
      if (listOfRefNextLevel) {
        id = checkCon(listOfRefNextLevel, index)
        if (id !== undefined) return index
      }
    }
  }

  const checkCha = (data, index) => {
    const { info, refs } = data
    const { user } = info
    if (user?.email?.includes(valueSearch.email?.trim())) {
      return index
    }

    if (refs) {
      const elm = checkCon(refs, index)
      if (elm !== undefined) {
        setListTree([listTree[elm]])
        return index
      }
    }
  }

  const handleSearch = () => {
    // if (!valueSearch?.email) {
    //   setListTree(originListTree)
    //   return
    // }

    // setEmail(valueSearch.email)

    // let item = null

    // for (let i = 0; i < listTree?.length; i++) {
    //   item = checkCha(listTree[i], i)
    //   if (item !== undefined) {
    //     setListTree([listTree[item]])
    //     return
    //   }
    // }
    if (valueSearch.email) {
      getList({ email: valueSearch.email })
    }
  }

  const handleReset = () => {
    setValueSearch({})
    getList({})
  }

  const changeSearch = (e) => {
    const { name } = e.target
    const { value } = e.target
    if (value === 'all') {
      const newData = { ...valueSearch }
      setValueSearch(newData)
    } else {
      setValueSearch({
        ...valueSearch,
        [name]: value.trim()
      })
    }
  }

  const addBranches = (item) => {
    const { level, listOfRefSameLevel } = item
    const checkSearch = (valueSearch?.length || valueSearch?.email) && listTree?.length === 1
    const listBranch = listOfRefSameLevel.map((branch, index) => {
      const { info, dollar_total, listOfRefNextLevel, rank } = branch
      const name = `${info?.user_info?.first_name} ${info?.user_info?.last_name}`
      return (
        <Fragment key={index}>
          <div className={`box-branch${level > 2 ? (checkSearch ? '' : ' hide') : ''}`}>
            <div className={classNames('dot', { active: info?.email.includes(valueSearch?.email) })}>
              {!!rank && (
                <div className={`label-rank ${rank}`}>
                  <img src={RANK_IMG[rank]} alt={RANK[rank]} />
                </div>
              )}
              <span className="name" style={{ paddingLeft: 10 }}>ID: {info?.id},&nbsp;</span>
              {(info?.user_info?.first_name && info?.user_info?.last_name) && (
                <span className="name" style={{ paddingLeft: 10 }}>{name},&nbsp;</span>
              )}
              {info?.email && (
                <span
                  className="email"
                  style={{ paddingLeft: (!info?.user_info?.first_name || !info?.user_info?.last_name) ? 10 : 0 }}
                >{`${info.email}`},&nbsp;
                </span>
              )}
              <span className="amount">
                <span>{translate('group.dollar-total')}: </span>{dollar_total || 0} USD
              </span>
              {(!!listOfRefNextLevel && level >= 2) && (
                <div
                  className={`box-plush ${info.id}`}
                  onClick={async () => {
                    await $(`.box-plush.${info.id}`).parent().parent().children('.box-branch')
                      .toggleClass('hide')
                    $(`.box-plush.${info.id} i.fa-plus-circle`).toggleClass('hide')
                    $(`.box-plush.${info.id} i.fa-minus-circle`).toggleClass('hide')
                  }}
                >
                  <i className={classNames('fa fa-plus-circle', { hide: checkSearch })} />
                  <i className={classNames('fa fa-minus-circle', { hide: !checkSearch })} />
                </div>
              )}
            </div>
            {!!listOfRefNextLevel && addBranches(listOfRefNextLevel)}
          </div>
        </Fragment>
      )
    })
    return listBranch
  }

  const tree = (referralTree) => {
    const { info, refs } = referralTree
    const { user } = info
    return (
      <>
        <div className="box-branch">
          <div className={classNames('dot', { active: user?.email.includes(valueSearch?.email) })}>
            {!!referralTree.rank && (
              <div className={`label-rank ${referralTree.rank}`}>
                <img src={RANK_IMG[referralTree.rank]} alt={RANK[referralTree.rank]} />
              </div>
            )}
            <span className="name" style={{ paddingLeft: 10 }}>ID: {user?.id},&nbsp;</span>
            {(user?.user_info?.first_name && user?.user_info?.last_name) && (
              <p className="name">
                <span className="name" style={{ paddingLeft: 10 }}>{`${user.user_info.first_name} ${user.user_info.last_name}`},&nbsp;</span>
              </p>
            )}
            {user?.email && (
              <span
                className="email"
                style={{ paddingLeft: (!user?.user_info?.first_name || !user?.user_info?.last_name) ? 10 : 0 }}
              >{`${user.email}`},&nbsp;
              </span>
            )}
            <p className="amount">
              <span>{translate('group.dollar-total')}: </span>{referralTree.dollar_total || 0} USD
            </p>
          </div>
          {!!refs && addBranches(refs)}
        </div>
      </>
    )
  }

  return (
    <div>
      <TitlePage namePage={translate('group.group')} />

      <div className="col-lg-12 mt-15">
        <Collapse>
          <Panel header={translate('utils.filter')}>
            <div className="col-sm-12">
              <div>
                <label>{translate('utils.email')}</label>
                <Input name="email" onChange={changeSearch} value={valueSearch.email} />
              </div>
            </div>

            <div>
              <Button
                margin
                onClick={handleSearch}
                loading={isLoading}
                disabled={isLoading}
              >{translate('utils.search')}
              </Button>
              <Button
                onClick={handleReset}
                loading={isLoading}
                disabled={isLoading}
              >{translate('utils.reset')}
              </Button>
            </div>

          </Panel>
        </Collapse>

      </div>

      <div className="col-lg-12">
        <section className="box ">
          <div className="box-group">
            {
              listTree.map((referralTree, i) => (
                <div className="box-tree" key={i}>
                  {tree(referralTree)}
                </div>
              ))
            }
          </div>
        </section>
      </div>

    </div>
  )
}

export default withLocalize(Group)
