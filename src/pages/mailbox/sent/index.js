import React from 'react'

function Sent() {
  return (
    <div className="col-md-9 col-sm-8 col-xs-12">
      <div className="mail_content">

        <div className="row">
          <div className="col-xs-12 header-mailbox">
            <div>
              <h3 className="mail_head">Sent</h3>
              <i className="fa fa-refresh icon-primary icon-xs icon-accent mail_head_icon" />
              <i className="fa fa-search icon-primary icon-xs icon-accent mail_head_icon" />
            </div>
            <i className="fa fa-cog icon-primary icon-xs icon-accent mail_head_icon pull-right" />

          </div>

          <div className="col-xs-12">

            <div className="pull-left">
              <div className="btn-group mail_more_btn">
                <button type="button" className="btn btn-default"> All</button>
                <button
                  type="button"
                  className="btn btn-default dropdown-toggle"
                  data-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span className="caret" />
                </button>
                <ul className="dropdown-menu" role="menu">
                  <li><a href="#">All</a></li>
                  <li><a href="#">Read</a></li>
                  <li><a href="#">Unread</a></li>
                  <li><a href="#">Starred</a></li>
                </ul>
              </div>
            </div>

            <nav className="pull-right">
              <ul className="pager mail_nav">
                <li>
                  <a href="#">
                    <i className="fa fa-arrow-left icon-xs icon-accent icon-secondary" />
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fa fa-arrow-right icon-xs icon-accent icon-secondary" />
                  </a>
                </li>
              </ul>
            </nav>
            <span className="pull-right mail_count_nav text-muted">
              <strong>1</strong> to <strong>20</strong> of 3247
            </span>
          </div>

          <div className="mail_list col-xs-12">
            <table className="table table-striped table-hover table-mail">
              <tr className="unread" id="mail_msg_1">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star-o icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">John Smith</td>
                <td className="open-view">
                  <span className="label label-primary">Selling</span>&nbsp;
                  <span className="msgtext">Hi, Lorem ipsum dolor sit amet consectetur adipisicing.</span>
                </td>
                <td className="open-view"><span className="msg_time">19:34</span>
                </td>
              </tr>
              <tr className="unread" id="mail_msg_2">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star-o icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">Laura Willson</td>
                <td className="open-view"><span className="msgtext"> elit. Magnam hic, accusantium ab odit, atque fugiat</span>
                </td>
                <td className="open-view"><span className="msg_time">21:54</span>
                </td>
              </tr>
              <tr className="unread" id="mail_msg_3">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">Jane D.</td>
                <td className="open-view"><span className="msgtext">corrupti voluptatibus quod omnis consectetur</span>
                </td>
                <td className="open-view"><span className="msg_time">22:28</span>
                </td>
              </tr>
              <tr id="mail_msg_4">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star-o icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">John Smith</td>
                <td className="open-view"><span className="msgtext">aperiam dolores praesentium. Ut, pariatur, nulla.</span>
                </td>
                <td className="open-view">
                  <span className="msg_time">Yesterday</span>
                </td>
              </tr>
              <tr id="mail_msg_5">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star">
                    <i className="fa fa-star icon-xs icon-accent" />
                  </div>
                </td>
                <td className="open-view">Laura Willson</td>
                <td className="open-view"><span className="msgtext">Reiciendis alias laborum esse Lorem ipsum dolo.</span>
                </td>
                <td className="open-view">
                  <span className="msg_time">Yesterday</span>
                </td>
              </tr>
              <tr id="mail_msg_6">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star-o icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">Jane D.</td>
                <td className="open-view">
                  <span className="label label-warning">Pending</span>&nbsp;
                  <span className="msgtext">reprehenderit error provident mollitia atque.</span>
                </td>
                <td className="open-view"><span className="msg_time">28 Feb</span>
                </td>
              </tr>
              <tr className="unread">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">John Smith</td>
                <td className="open-view">
                  <span className="label label-warning">Pending</span>&nbsp;
                  <span className="msgtext">Voluptate fuga minus odit aliquid. Illo rerum, velit.</span>
                </td>
                <td className="open-view"><span className="msg_time">25 Feb</span>
                </td>
              </tr>
              <tr id="mail_msg_8">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">Laura Willson</td>
                <td className="open-view"><span className="msgtext">consectetur adipisicing elit. Reiciendis molestiae</span>
                </td>
                <td className="open-view"><span className="msg_time">25 Feb</span>
                </td>
              </tr>
              <tr id="mail_msg_9">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star-o icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">Jane D.</td>
                <td className="open-view"><span className="msgtext">We play, dance and love. Share love all around you.</span>
                </td>
                <td className="open-view"><span className="msg_time">25 Feb</span>
                </td>
              </tr>
              <tr id="mail_msg_10">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star-o icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">John Smith</td>
                <td className="open-view">
                  <span className="label label-success">Buying</span>&nbsp;
                  <span className="msgtext">Hello, Lorem ipsum dolor sit amet, adipisicing elit.</span>
                </td>
                <td className="open-view"><span className="msg_time">25 Feb</span>
                </td>
              </tr>
              <tr className="unread">
                <td className="">
                  <input className="iCheck" type="checkbox" />
                </td>
                <td className="">
                  <div className="star"><i
                    className="fa fa-star icon-xs icon-accent"
                  />
                  </div>
                </td>
                <td className="open-view">Laura Willson</td>
                <td className="open-view"><span className="msgtext">Your offer seems amazingly smart and elegant to accept.</span>
                </td>
                <td className="open-view"><span className="msg_time">21 Feb</span>
                </td>
              </tr>

            </table>
          </div>
        </div>

      </div>
    </div>
  )
}

export default Sent
