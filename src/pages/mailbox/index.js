import React, { lazy } from 'react'
import { Link, Route, Switch } from 'react-router-dom'

import './style.scss'

const Inbox = lazy(() => import('@/pages/mailbox/inbox'))
const Compose = lazy(() => import('@/pages/mailbox/compose'))
const View = lazy(() => import('@/pages/mailbox/view'))
const Sent = lazy(() => import('@/pages/mailbox/sent'))

function Index() {
  const renderLazyComponent = (LazyComponent, params) => (props) => <LazyComponent {...props} {...params} />
  return (
    <div className="page-mailbox">
      <div className="col-xs-12">
        <div className="page-title">

          <div className="pull-left">
            <h1 className="title">Mailbox</h1>
          </div>

          <div className="pull-right hidden-xs">
            <ol className="breadcrumb">
              <Link to="/dashboard"><i className="fa fa-home" />Home</Link>
              <li>
                <a>Mailbox</a>
              </li>
              <li className="active">
                <strong>Inbox</strong>
              </li>
            </ol>
          </div>

        </div>
      </div>
      <div className="clearfix" />

      <div className="col-lg-12">
        <section className="box nobox ">
          <div className="content-body">
            <div className="row">
              <div className="col-md-3  col-sm-4 col-xs-12">
                <Link to="/compose" class="btn btn-primary btn-block">Compose</Link>
                <ul className="list-unstyled mail_tabs">
                  <li className="active">
                    <Link to="/inbox" className="mail-item">
                      <div>
                        <i className="fa fa-inbox" /> Inbox
                      </div>
                      <span className="badge badge-primary pull-right">6</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="/sent" className="mail-item">
                      <div>
                        <i className="fa fa-send-o" /> Sent
                      </div>
                      <div />
                    </Link>
                  </li>
                </ul>
              </div>

              <Switch>
                <Route exact path="/inbox" component={renderLazyComponent(Inbox)} />
                <Route exact path="/compose" component={renderLazyComponent(Compose)} />
                <Route exact path="/view" component={renderLazyComponent(View)} />
                <Route exact path="/sent" component={renderLazyComponent(Sent)} />
              </Switch>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

export default Index
