import React from 'react'

function View() {
  return (
    <div className="col-md-9 col-sm-8 col-xs-12">
      <div className="mail_content">

        <div className="row">
          <div className="col-xs-12 header-mailbox">
            <div>
              <h3 className="mail_head">View</h3>
              <i className="fa fa-refresh icon-primary icon-xs icon-accent mail_head_icon" />
              <i className="fa fa-search icon-primary icon-xs icon-accent mail_head_icon" />
            </div>
            <i className="fa fa-cog icon-primary icon-xs icon-accent mail_head_icon pull-right" />
          </div>

          <div className="col-xs-12 mail_view_title">

            <div className="pull-left">
              <h3 className="">New Offer to Buy Cryptocurrency.</h3>
            </div>

            <div className="pull-right">
              <button
                type="button"
                className="btn btn-default btn-icon"
                data-color-class="primary"
                data-animate=" animated fadeIn"
                data-toggle="tooltip"
                data-original-title="Reply"
                data-placement="top"
              >
                <i className="fa fa-reply icon-xs" />
              </button>
              <button
                type="button"
                className="btn btn-default btn-icon"
                data-color-class="primary"
                data-animate=" animated fadeIn"
                data-toggle="tooltip"
                data-original-title="Reply All"
                data-placement="top"
              >
                <i className="fa fa-reply-all icon-xs" />
              </button>
              <button
                type="button"
                className="btn btn-default btn-icon"
                data-color-class="primary"
                data-animate=" animated fadeIn"
                data-toggle="tooltip"
                data-original-title="Forward"
                data-placement="top"
              >
                <i className="fa fa-mail-forward icon-xs" />
              </button>
              <button
                type="button"
                className="btn btn-default btn-icon"
                data-color-class="primary"
                data-animate=" animated fadeIn"
                data-toggle="tooltip"
                data-original-title="Attachments"
                data-placement="top"
              >
                <i className="fa fa-paperclip icon-xs" />
              </button>
              <button
                type="button"
                className="btn btn-default btn-icon"
                data-color-class="primary"
                data-animate=" animated fadeIn"
                data-toggle="tooltip"
                data-original-title="Trash"
                data-placement="top"
              >
                <i className="fa fa-trash-o icon-xs" />
              </button>
            </div>

          </div>

          <div className="col-xs-12 mail_view_info">

            <div className="pull-left">
              <span className=""><strong>Clarine Vassar</strong> (clarine_vassar@example.com) to <strong>You</strong></span>
            </div>

            <div className="pull-right">
              <span className="msg_ts text-muted">12:40 PM, Today</span>
            </div>

          </div>

          <div className="col-xs-12 mail_view">
            <p>Hello Smith,</p>
            <h4>What is your offer for crypto trading?</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error pariatur,
                          ullam, quasi quis vero enim harum sequi voluptate voluptatibus modi nihil.
                          Sapiente, quas quae consectetur ipsam nisi qui quidem veniam.
            </p>
            <br />
            <h4>Balance</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident
                          consequuntur ad eius, quia quam laboriosam, amet molestiae blanditiis,
                          incidunt maiores repudiandae ducimus. Accusantium expedita consectetur
                          doloribus numquam minus neque aut Lorem ipsum dolor sit amet,
                          consectetur adipisicing elit. Soluta quasi optio eligendi. Similique
                          debitis qui delectus quibusdam error aspernatur, provident. Reiciendis
                          distinctio debitis expedita nobis illo itaque eveniet tempore
                          libero?.
            </p>
            <br />
            <h4>Contrast</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum,
                          ad, vero! Consequuntur aut fugiat accusantium sunt mollitia
                          officia quam quas, dolor numquam sed magnam reprehenderit?
                          Repudiandae dolorem inventore dicta architecto Lorem ipsum dolor
                          sit amet, consectetur adipisicing elit. Similique dolor, facere
                          tempore reiciendis harum at iure autem obcaecati, unde, distinctio
                          consequuntur sapiente debitis quam vero assumenda labore
                          voluptates! Nam, hic..
            </p>
            <br />
          </div>

          <div className="col-xs-12 mail_view_attach">
            <h3>
              <i className="fa fa-paperclip icon-sm" /> Attachments
            </h3>
            <br />

            <ul className="list-unstyled list-inline">

              <li>
                <a href="#" className="file">
                  <img
                    alt=""
                    src="/data/dashelements/attach-1.png"
                    className="img-thumbnail"
                  />
                </a>

                <a href="#" className="title">
                                  Conditions_Details.jpg
                  <span>12KB</span>
                </a>

                <div className="actions">
                  <a href="#">View</a> -
                  <a href="#">Download</a>
                </div>
              </li>
            </ul>
          </div>

          <div className="col-xs-12 mail_view_reply">
            <div className="form-group">
              <div className="controls">
                <textarea
                  className="form-control autogrow"
                  cols="5"
                  id="field-7"
                  placeholder="Reply or Forward this message"
                  style={{ overflow: 'hidden', wordWrap: 'break-word', resize: 'horizontal', height: '120px' }}
                />
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  )
}

export default View
