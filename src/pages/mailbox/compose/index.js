import React from 'react'

function Compose() {
  return (
    <div className="col-md-9 col-sm-8 col-xs-12">
      <div className="mail_content">

        <div className="row">
          <div className="col-xs-12 header-mailbox">
            <div>
              <h3 className="mail_head">Compose</h3>
              <i className="fa fa-refresh icon-primary icon-xs icon-accent mail_head_icon" />
              <i className="fa fa-search icon-primary icon-xs icon-accent mail_head_icon" />
            </div>
            <i className="fa fa-cog icon-primary icon-xs icon-accent mail_head_icon pull-right" />
          </div>

          <div className="col-xs-12 mail_view_title">

            <div className="pull-right">
              <button
                type="button"
                className="btn btn-default btn-icon"
                data-color-class="primary"
                data-animate=" animated fadeIn"
                data-toggle="tooltip"
                data-original-title="Send"
                data-placement="top"
              >
                <i className="fa fa-paper-plane-o icon-xs" />
              </button>
              <button
                type="button"
                className="btn btn-default btn-icon"
                data-color-class="primary"
                data-animate=" animated fadeIn"
                data-toggle="tooltip"
                data-original-title="Save"
                data-placement="top"
              >
                <i className="fa fa-floppy-o icon-xs" />
              </button>
              <button
                type="button"
                className="btn btn-default btn-icon"
                data-color-class="primary"
                data-animate=" animated fadeIn"
                data-toggle="tooltip"
                data-original-title="Trash"
                data-placement="top"
              >
                <i className="fa fa-trash-o icon-xs" />
              </button>
            </div>

          </div>

          <div className="col-xs-12 mail_view_info">

            <div className="form-group mail_cc_bcc">
              <label className="form-label">To:</label>
              <span className="desc">e.g. someemail@example.com</span>
              <div className="labels">
                <span className="label label-secondary cc">CC</span>
                <span className="label label-secondary bcc">BCC</span>
              </div>
              <div className="controls">
                <input type="text" className="form-control mail_compose_to" value="" />
              </div>
            </div>

            <div className="form-group mail_compose_cc">
              <label className="form-label">CC:</label>
              <span className="desc">e.g. someemail@example.com</span>
              <div className="controls">
                <input type="text" className="form-control mail_compose_to" value="" />
              </div>
            </div>

            <div className="form-group mail_compose_bcc">
              <label className="form-label">BCC:</label>
              <span className="desc">e.g. someemail@example.com</span>
              <div className="controls">
                <input type="text" className="form-control mail_compose_to" value="" />
              </div>
            </div>

            <div className="form-group">
              <label className="form-label">Subject:</label>
              <span className="desc">e.g. Buying offer for Bitcoin</span>
              <div className="controls">
                <input type="text" className="form-control" value="" />
              </div>
            </div>

            <div className="form-group">
              <label className="form-label">Message:</label>
              <textarea
                className="mail-compose-editor"
                placeholder="Enter text ..."
                style={{ width: '100%', height: '250px', fontSize: '14px', lineHeight: '23px', padding: '15px' }}
              />
            </div>

          </div>

          <div className="col-xs-12">

            <div className="pull-left">
              <button type="button" className="btn btn-primary">
                <i className="fa fa-paper-plane-o icon-xs" /> &nbsp; SEND
              </button>
              <button type="button" className="btn btn-primary">
                <i className="fa fa-floppy-o icon-xs" /> &nbsp; SAVE
              </button>
              <button type="button" className="btn btn-secondary">
                <i className="fa fa-trash-o icon-xs" /> &nbsp; TRASH
              </button>
            </div>

          </div>

        </div>

      </div>
    </div>
  )
}

export default Compose
