import React from 'react'
import { Line } from 'react-chartjs-2'

function StatisticTokenEarnings() {
  const configChart = {
    responsive: true,
    legend: {
      display: false
    },
    tooltips: {
      intersect: false
    }
  }

  const dataTokenEarnings = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Sept'],
    datasets: [
      {
        label: '',
        data: [20, 35, 30, 39, 29, 45, 35, 50, 45],
        fill: true,
        backgroundColor: 'rgba(63,81,181,0.5)',
        borderColor: 'rgba(63,81,181,1)',
        pointBackgroundColor: 'rgba(63,81,181,1)',
        pointBorderColor: '#ffffff',
        borderWidth: 2,
        pointBorderWidth: 1,
        pointRadius: 5,
        pointHoverBackgroundColor: '#ffffff',
        pointHoverBorderColor: 'rgba(63,81,181,1)'
      }
    ]
  }

  return (
    <div>
      <div className="col-lg-8">
        <section className="box ">
          <header className="panel_header">
            <h2 className="title pull-left">Total Token Earnings</h2>
            <div className="actions panel_actions pull-right">
              <a className="box_toggle fa fa-chevron-down" />
              <a className="box_setting fa fa-cog" data-toggle="modal" href="#section-settings" />
              <a className="box_close fa fa-times" />
            </div>
          </header>
          <div className="content-body">
            <div className="row">
              <div className="col-xs-12">
                <div className="relative">
                  <Line
                    data={dataTokenEarnings}
                    options={configChart}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div className="col-lg-4">
        <div className="row">
          <div className="col-lg-12">
            <div className="r1_graph1 db_box db_box_large mt-15 text-center">
              <div className="invest-img relative">
                <img
                  src="/data/crypto-dash/invest-img.png"
                  className="center-block img-responsive"
                  alt=""
                />
                <div className="invest-data">
                  <h5 className="boldy mt-0">Investment Split</h5>
                  <h2 className="boldy">45%</h2>
                  <p className="mb-0">On Ethereum</p>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-6 no-pr">
                  <div className="text-left">
                    <h5><i className="fa fa-dot-circle-o p-text mr-10" />Bitcoin
                      <span className="mb-0 green-text">24.98%</span>
                    </h5>
                  </div>
                </div>
                <div className="col-lg-6 no-pr">
                  <div className="text-left">
                    <h5><i className="fa fa-dot-circle-o blue-text mr-10" />Litecoin
                      <span className="mb-0 green-text">19.98%</span>
                    </h5>
                  </div>
                </div>
                <div className="col-lg-6 no-pr">
                  <div className="text-left">
                    <h5><i className="fa fa-dot-circle-o red-text mr-10" />Dashcoin
                      <span className="mb-0 green-text">19.36%</span>
                    </h5>
                  </div>
                </div>
                <div className="col-lg-6 no-pr">
                  <div className="text-left">
                    <h5><i className="fa fa-dot-circle-o green-text mr-10" />Monero
                      <span className="mb-0 green-text">9.05%</span>
                    </h5>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
  )
}

export default StatisticTokenEarnings
