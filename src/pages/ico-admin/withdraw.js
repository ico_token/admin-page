import React from 'react'

function Withdraw() {
  return (
    <div>
      <div className="col-lg-4">
        <div className="dotted-wrapper mt-15">
          <div className="ref-payout">
            <img src="/data/crypto-dash/coin1.png" alt="" />
            <span className="w-text">BTC Payout Address
            </span>
          </div>
          <div className="payout-address mt-30">
            <div className="input-group primary mb-30">
              <input
                type="text"
                className="form-control text-left transparent"
                value="Xy734-34093jksd90sk-JKAOOEI900"
                aria-describedby="basic-addon1"
              />
              <span className="input-group-addon orange-bg" id="basic-addon1"><span
                className="arrow"
              /><i className="fa fa-edit" />
              </span>
            </div>
            <button type="button" className="btn btn-primary btn-lg gradient-orange"> Withdraw BTC Funds
            </button>
          </div>
        </div>
      </div>

      <div className="col-lg-4">
        <div className="dotted-wrapper mt-15">
          <div className="ref-payout">
            <img src="/data/crypto-dash/coin2.png" alt="" /><span className="w-text">ETH Payout Address</span>
          </div>
          <div className="payout-address mt-30">
            <div className="input-group primary mb-30">
              <input
                type="text"
                className="form-control text-left transparent"
                value="Xy734-34093jksd90sk-JKAOOEI900"
                aria-describedby="basic-addon1"
              />
              <span className="input-group-addon " id="basic-addon1"><span className="arrow" /><i
                className="fa fa-edit"
              />
              </span>
            </div>
            <button type="button" className="btn btn-primary btn-lg gradient-blue">Withdraw ETH Funds</button>
          </div>
        </div>
      </div>

      <div className="col-lg-4">
        <div className="row">
          <div className="col-xs-12">
            <div className="r1_graph1 db_box db_box_large mt-15">
              <div className="coin-box2 flex align-items-center">
                <div className="coin-icon mr-10">
                  <img src="/data/crypto-dash/coin1.png" alt="" />
                </div>

                <h5 className="coin-name boldy">Bitcoin</h5>
                <h5 className="coin-price boldy">$8.958</h5>
                <p className="mb-0 green-text">4.98%<i className="complete fa fa-arrow-up ml-10" /></p>
              </div>
              <div className="coin-box2 flex align-items-center">
                <div className="coin-icon mr-10">
                  <img src="/data/crypto-dash/coin2.png" alt="" />
                </div>

                <h5 className="coin-name boldy">Ethereum</h5>
                <h5 className="coin-price boldy">$1.958</h5>
                <p className="mb-0 red-text">3.45%<i className="cancelled fa fa-arrow-down ml-10" /></p>
              </div>
              <div className="coin-box2 flex align-items-center">
                <div className="coin-icon mr-10">
                  <img src="/data/crypto-dash/coin3.png" alt="" />
                </div>

                <h5 className="coin-name boldy">Dashcoin</h5>
                <h5 className="coin-price boldy">$838,45</h5>
                <p className="mb-0 green-text">3.45%<i className="complete fa fa-arrow-up ml-10" /></p>
              </div>
              <div className="coin-box2 flex align-items-center mb-0">
                <div className="coin-icon mr-10">
                  <img src="/data/crypto-dash/coin8.png" alt="" />
                </div>

                <h5 className="coin-name boldy">Litecoin</h5>
                <h5 className="coin-price boldy">$308,09</h5>
                <p className="mb-0 red-text">2.05%<i className="cancelled fa fa-arrow-down ml-10" /></p>
              </div>

            </div>
          </div>

        </div>
      </div>
    </div>
  )
}

export default Withdraw
