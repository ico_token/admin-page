import React from 'react'
import ActivitiesHistory from './activities-history'
import Withdraw from './withdraw'
import StatisticTokenEarnings from './statistic-token-earnings'
import ProfileBalance from './profile-balance'
import PricingItem from './pricing-item'

function ICOAdmin() {
  return (
    <div>
      <div className="col-xs-12">
        <div className="page-title">

          <div className="pull-left">
            <h1 className="title">ICO Admin Dash</h1>
          </div>

          <div className="pull-right hidden-xs">
            <ol className="breadcrumb">
              <li>
                <a href="#"><i className="fa fa-home" />Home</a>
              </li>
              <li className="active">
                <strong>ICO Admin Dash</strong>
              </li>
            </ol>
          </div>

        </div>
      </div>
      <div className="clearfix" />

      <ProfileBalance />

      <div className="clearfix" />

      <StatisticTokenEarnings />

      <div className="clearfix" />

      <PricingItem />

      <div className="clearfix" />

      <div className="col-lg-5">
        <div className="ref-num-box statistics-box mt-15 text-center pt0">
          <h4 className="boldy mt-30 mb-5"> The Next Round</h4>
          <p className="mb-0 text-muted boldy mb-30">
              The Bounes token in next Round is
            <span className="boldy blue-text">15%</span>
          </p>
          <div className="count-down titled circled text-center">
            <div className="simple_timer" />
          </div>

        </div>
      </div>

      <div className="col-lg-7">
        <div className="ref-num-box statistics-box mt-15 text-center pt0 pb30">
          <h4 className="boldy mt-30 mb-5">The Funds Rasied </h4>
          <p className="mb-0 text-muted boldy mb-30">Lorem ipsum dolor sit amet</p>
          <div className="ico-progress">
            <ul className="list-unstyled list-inline clearfix mb-10">
              <li className="title">33m</li>
              <li className="strength">75m</li>
            </ul>
            <div className="current-progress relative">
              <div className="progress progress-lg">
                <div
                  className="progress-bar gradient-blue"
                  role="progressbar"
                  aria-valuenow="70"
                  aria-valuemin="0"
                  aria-valuemax="100"
                  style={{ width: '70%' }}
                />
              </div>
            </div>
            <span className="pull-left">Softcap in 103 days</span>
            <span className="pull-right">Token Hardcap</span>
          </div>

        </div>
      </div>

      <div className="clearfix" />

      <Withdraw />

      <div className="clearfix" />

      <ActivitiesHistory />
    </div>
  )
}

export default ICOAdmin
