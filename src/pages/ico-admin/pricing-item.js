import React from 'react'

function PricingItem() {
  return (
    <div>
      <div className="col-lg-3 col-md-6">
        <div className="pricing-item ">
          <h4>Round 1</h4>
          <h3><strong className="xzc-1-month">0.96$</strong></h3>
          <span>1 ETH = 500 Token</span>
          <div className="pricing">15,000,000 Token</div>
          <label><strong>42% bonus</strong></label>
        </div>
      </div>
      <div className="col-lg-3 col-md-6">
        <div className="pricing-item ">
          <h4>Round 2</h4>
          <h3><strong className="xzc-1-month">1.25$</strong></h3>
          <span>1 ETH = 500 Token</span>
          <div className="pricing">15,000,000 Token</div>
          <label><strong>30% bonus</strong></label>
        </div>
      </div>
      <div className="col-lg-3 col-md-6">
        <div className="pricing-item ">
          <h4>Round 3</h4>
          <h3><strong className="xzc-1-month">1.50$</strong></h3>
          <span>1 ETH = 500 Token</span>
          <div className="pricing">15,000,000 Token</div>
          <label><strong>25% bonus</strong></label>
        </div>
      </div>
      <div className="col-lg-3 col-md-6">
        <div className="pricing-item ">
          <h4>Round 4</h4>
          <h3><strong className="xzc-1-month">1.96$</strong></h3>
          <span>1 ETH = 500 Token</span>
          <div className="pricing">15,000,000 Token</div>
          <label><strong>15% bonus</strong></label>
        </div>
      </div>
    </div>
  )
}

export default PricingItem
