import React from 'react'

function ProfileBalance() {
  return (
    <div>
      <div className="col-lg-4">
        <div className="profile-balance gradient-blue mb-15">
          <div className="profile-bal-head">
            <h3>Total Token Earning</h3>
          </div>
          <div className="profile-bal-body">
            <h2>19.002324<small>BTC</small></h2>
            <p className="mb-0 g-text">$90,782.84</p>
          </div>
        </div>
      </div>
      <div className="col-lg-4">
        <div className="profile-balance gradient-blue mb-15">
          <div className="profile-bal-head">
            <h3>Purchased Tokens</h3>
          </div>
          <div className="profile-bal-body">
            <h2>25,002,324</h2>
            <p className="mb-0 w-text">4.98%<i className="complete fa fa-arrow-up ml-10" /></p>
          </div>
        </div>
      </div>
      <div className="col-lg-4">
        <div className="profile-balance gradient-blue mb-15">
          <div className="profile-bal-head">
            <h3>Registered Users</h3>
          </div>
          <div className="profile-bal-body">
            <h2>501,903</h2>
            <p className="mb-0 g-text">Total Users Registered</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProfileBalance
