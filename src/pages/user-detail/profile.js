import React, { useState } from "react";
import { getGender } from "@/utils/tools";
import ModalUpdatePhone from "./modal-update-phone";
import ModalUpdateCMND from "./modal-update-cmnd";
import ModalUpdateAge from "./modal-update-age";
import ModalUpdateGender from "./modal-update-gender";
import ModalDeleteUser from "./modal-delete-user";
import ModalUpdateEmail from "./modal-update-email";
import ModalUpdateFirstName from "./modal-update-first-name";
import ModalUpdateLastName from "./modal-update-last-name";
import Button from "@/components/button";
import ModalUpdateIMG from "./modal-update-img";
import ModalUpdate2FA from "./modal-update-2fa";

const Profile = ({ dataDetail, reload, translate, history }) => {
  const {
    user_info,
    phone_number,
    tosi_address,
    email,
    phone_code,
    is_email_verified,
    is_kyc,
    id,
    is_phone_verified,
    is_locked,
    kyc_id,
    is_tfa,
  } = dataDetail;

  const [openModal, setOpenModal] = useState(false);
  const [isOpenUpdateCMND, setIsOpenUpdateCMND] = useState(false);
  const [isOpenUpdateAge, setIsOpenUpdateAge] = useState(false);
  const [isOpenUpdateGender, setIsOpenUpdateGender] = useState(false);
  const [isOpenDeleteUser, setIsOpenDeleteUser] = useState(false);
  const [isOpenUpdateFirstName, setIsOpenUpdateFirstName] = useState(false);
  const [isOpenUpdateLastName, setIsOpenUpdateLastName] = useState(false);
  const [isOpenUpdateEmail, setIsOpenUpdateEmail] = useState(false);
  const [openModalIMG, setOpenModalIMG] = useState(false);
  const [isOpenUpdate2FA, setIsOpenUpdate2FA] = useState(false);

  return (
    <div>
      <div className="col-lg-4">
        <div className="row">
          <div className="col-md-12">
            <section className="box has-border-left-3">
              <div className="content-body">
                <div className="uprofile-image mt-30">
                  <a
                    className="box_setting fa fa-cog"
                    onClick={() => setOpenModalIMG("true")}
                  />
                  <div className="prof-contain relative">
                    <img
                      alt=""
                      src={user_info?.avatar || "/assets/images/user-male.png"}
                      className="img-responsive"
                    />
                    <span className="prof-check fa fa-check" />
                  </div>
                </div>
                <div className="uprofile-name">
                  <h3>
                    <a>{`${user_info?.last_name || ""} ${
                      user_info?.first_name || ""
                    }`}</a>
                    <span className="uprofile-status online" />
                  </h3>
                </div>
                <div className="uprofile-info v2">
                  <ul className="list-unstyled mb-0">
                    <li className="pt0 d-flex">
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-envelope" />{" "}
                        {translate("utils.email")}
                      </h5>
                      <span className="text-mail" title={email}>
                        {email}
                      </span>
                      <a
                        className="box_setting fa fa-cog"
                        onClick={() => setIsOpenUpdateEmail(true)}
                      />
                    </li>

                    <li className="row-info">
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-credit-card" />{" "}
                        {translate("utils.last_name")}
                      </h5>
                      <span>{user_info?.last_name}</span>
                      <a
                        className="box_setting fa fa-cog"
                        onClick={() => setIsOpenUpdateLastName(true)}
                      />
                    </li>

                    <li className="row-info">
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-credit-card" />{" "}
                        {translate("utils.name")}
                      </h5>
                      <span>{user_info?.first_name}</span>
                      <a
                        className="box_setting fa fa-cog"
                        onClick={() => setIsOpenUpdateFirstName(true)}
                      />
                    </li>

                    <li className="d-flex">
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-home" />{" "}
                        {translate("utils.address")}
                      </h5>
                      <span className="text-mail" title={tosi_address}>
                        {tosi_address}
                      </span>
                    </li>

                    <li className="row-info">
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-phone" /> {translate("utils.phone")}
                      </h5>
                      <span>{phone_number}</span>
                      <a
                        className="box_setting fa fa-cog"
                        title={translate("users.update-phone")}
                        onClick={() => setOpenModal(true)}
                      />
                    </li>

                    <li className="row-info">
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-credit-card" />{" "}
                        {translate("utils.id_number")}
                      </h5>
                      <span>{user_info?.id_number}</span>
                      <a
                        className="box_setting fa fa-cog"
                        onClick={() => setIsOpenUpdateCMND(true)}
                      />
                    </li>

                    <li className="row-info">
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-credit-card" />{" "}
                        {translate("utils.age")}
                      </h5>
                      <span>{user_info?.age}</span>
                      <a
                        className="box_setting fa fa-cog"
                        onClick={() => setIsOpenUpdateAge(true)}
                      />
                    </li>
                    <li className="row-info">
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-user" /> {translate("utils.gender")}
                      </h5>
                      <span>{getGender(user_info?.gender)}</span>
                      <a
                        className="box_setting fa fa-cog"
                        onClick={() => setIsOpenUpdateGender(true)}
                      />
                    </li>
                    <li>
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-check" />{" "}
                        {translate("utils.verified-email")}
                      </h5>
                      {is_email_verified ? (
                        <i className="fa fa-check-circle verification" />
                      ) : (
                        <i className="fa fa-times-circle not-verification" />
                      )}
                    </li>
                    <li>
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-check" />{" "}
                        {translate("utils.verified-phone")}
                      </h5>
                      {is_phone_verified ? (
                        <i className="fa fa-check-circle verification" />
                      ) : (
                        <i className="fa fa-times-circle not-verification" />
                      )}
                    </li>
                    <li>
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-check" />{" "}
                        {translate("utils.locked")}
                      </h5>
                      {is_locked ? (
                        <i className="fa fa-check-circle verification" />
                      ) : (
                        <i className="fa fa-times-circle not-verification" />
                      )}
                    </li>
                    <li>
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-check" /> {translate("utils.kyc")}
                      </h5>
                      {is_kyc && (
                        <i className="fa fa-check-circle verification" />
                      )}
                      {!is_kyc && kyc_id && (
                        <i className="fa fa-spinner waiting" />
                      )}
                      {!is_kyc && !kyc_id && (
                        <i className="fa fa-times-circle not-verification" />
                      )}
                    </li>
                    <li className="row-info">
                      <h5 className="mt-0 mb-0">
                        <i className="fa fa-check" /> 2FA
                      </h5>
                      {is_tfa ? (
                        <i className="fa fa-check-circle verification" />
                      ) : (
                        <i className="fa fa-times-circle not-verification" />
                      )}
                      <a
                        className="box_setting fa fa-cog"
                        onClick={() => setIsOpenUpdate2FA(true)}
                      />
                    </li>

                    <li className="text-center">
                      <Button
                        className="btn btn-danger btn-corner"
                        onClick={() => setIsOpenDeleteUser(true)}
                      >
                        {translate("utils.delete") +
                          " " +
                          translate("users.users")}
                      </Button>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>

      <ModalUpdatePhone
        close={() => setOpenModal(false)}
        isOpen={openModal}
        reload={reload}
        translate={translate}
        phone_number={phone_number}
        phone_code={phone_code}
        id={id}
      />

      <ModalUpdateCMND
        isOpen={isOpenUpdateCMND}
        close={() => setIsOpenUpdateCMND(false)}
        reload={reload}
        translate={translate}
        id={id}
        id_number={user_info?.id_number}
      />

      <ModalUpdateAge
        isOpen={isOpenUpdateAge}
        close={() => setIsOpenUpdateAge(false)}
        reload={reload}
        translate={translate}
        id={id}
        age={user_info?.age}
      />

      <ModalUpdateGender
        isOpen={isOpenUpdateGender}
        close={() => setIsOpenUpdateGender(false)}
        reload={reload}
        translate={translate}
        id={id}
        gender={user_info?.gender}
      />

      <ModalUpdate2FA
        isOpen={isOpenUpdate2FA}
        close={() => setIsOpenUpdate2FA(false)}
        reload={reload}
        translate={translate}
        id={id}
        is_tfa={is_tfa}
      />

      <ModalDeleteUser
        isOpen={isOpenDeleteUser}
        close={() => setIsOpenDeleteUser(false)}
        reload={reload}
        translate={translate}
        id={id}
        email={email}
        history={history}
      />

      <ModalUpdateEmail
        isOpen={isOpenUpdateEmail}
        close={() => setIsOpenUpdateEmail(false)}
        reload={reload}
        translate={translate}
        id={id}
        email={email}
      />

      <ModalUpdateFirstName
        isOpen={isOpenUpdateFirstName}
        close={() => setIsOpenUpdateFirstName(false)}
        reload={reload}
        translate={translate}
        id={id}
        firstName={user_info?.first_name}
      />

      <ModalUpdateLastName
        isOpen={isOpenUpdateLastName}
        close={() => setIsOpenUpdateLastName(false)}
        reload={reload}
        translate={translate}
        id={id}
        lastName={user_info?.last_name}
      />

      <ModalUpdateIMG
        close={() => setOpenModalIMG(false)}
        isOpen={openModalIMG}
        reload={reload}
        translate={translate}
        name={"avatar"}
        url={user_info?.avatar || ""}
        id={id}
      />
    </div>
  );
};

export default Profile;
