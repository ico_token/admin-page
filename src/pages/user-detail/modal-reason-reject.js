import React, { useState } from 'react';
import Modal from '@/components/modal'
import TextArea from '@/components/textarea'
import Field from '@/components/field'
import Notification from '@/components/notification'
import Button from '@/components/button'
import { Formik, Form } from 'formik'
import { object, string } from 'yup'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

const { verifyKyc } = actions;
const ModalReason = ({ close, isOpen, reload, translate, id }) => {
	const dispatch = useDispatch();
	const [isLoading, setIsLoading] = useState(false)

	const _onSubmit = (values) => {
    const payload = {
      reason: values.reason, id, is_kyc: false
    }

    setIsLoading(true)
    dispatch(
      verifyKyc(payload, (action) => {
        setIsLoading(false)
        if (action === TYPES.VERIFY_KYC_SUCCESS) {
          reload()
					close()
          return Notification.success("Từ chối xác thực KYC")
        }
      })
    )
  }

	const _renderForm = ({ handleSubmit, ...form }) => {
		return (
			<div>
				<Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
					<div className="row">
						<div className="col-xs-12">
							<div className="form-login">
								<label className="form-label">{translate('users.reason')}</label>
								<div className="controls">
									<Field
										form={form}
										name="reason"
										component={TextArea}
									/>
								</div>
							</div>

							<div className="text-center">
								<Button
									className="btn btn-danger btn-corner"
									onClick={handleSubmit}
									loading={isLoading}
									disabled={isLoading}
								>{translate('utils.reject')}
								</Button>
							</div>
						</div>
					</div>
				</Form>
			</div>
		)
	}

	const validation = object().shape({
		reason: string().max(255,'max_255')
	})
	return (
		<Modal
			visible={isOpen}
			onCancel={close}
			title={translate('users.reject-kyc')}
			destroyOnClose={true}
		>
			<Formik
				validateOnChange={false}
				validateOnBlur={false}
				validationSchema={validation}
				onSubmit={_onSubmit}
				component={_renderForm}
				enableReinitialize
			/>
		</Modal>

	)
}

export default ModalReason