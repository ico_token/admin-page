import React, { useState } from "react";
import { Formik, Form } from "formik";
import { boolean, object, string } from "yup";
import { useDispatch } from "react-redux";
import Modal from "@/components/modal";
import Select from "@/components/select";
import Field from "@/components/field";
import Notification from "@/components/notification";
import Button from "@/components/button";
import { TYPES, actions } from "@/store/actions";
import { getGender } from "../../utils/tools";

const { updateUser } = actions;

const ModalUpdate2FA = ({ close, isOpen, reload, is_tfa, id, translate }) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);

  const optionGender = [
    {
      name: "Enable",
      value: "enable",
    },
    {
      name: "Disable",
      value: "disable",
    },
  ];

  const onClose = () => {
    close();
  };

  const onSubmit = (values) => {
    const payload = {
      is_tfa: values.is_tfa,
    };
    setIsLoading(true);
    dispatch(
      updateUser({ id, payload }, (action) => {
        setIsLoading(false);
        if (action === TYPES.UPDATE_USER_SUCCESS) {
          reload();
          onClose();
          return Notification.success(translate("success.update_success"));
        }
      })
    );
  };

  const renderForm = ({ handleSubmit, ...form }) => (
    <div>
      <Form
        id="msg_validate"
        action="#"
        novalidate="novalidate"
        className="no-mb no-mt"
      >
        <div className="row">
          <div className="col-xs-12">
            <div className="form-login">
              <label className="form-label">2FA</label>
              <div className="controls">
                <Field
                  form={form}
                  name="is_tfa"
                  component={Select}
                  options={optionGender}
                />
              </div>
            </div>

            <div className="text-center">
              <Button
                className="btn btn-primary btn-corner"
                onClick={handleSubmit}
                loading={isLoading}
                disabled={isLoading}
              >
                {translate("utils.update")}
              </Button>
            </div>
          </div>
        </div>
      </Form>
    </div>
  );

  const validationSchema = object().shape({
    is_tfa: string().required(),
  });

  return (
    <Modal
      visible={isOpen}
      onCancel={onClose}
      title={translate("users.update_2fa")}
      destroyOnClose
    >
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{ is_tfa: is_tfa ? "enable" : "disable" }}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        component={renderForm}
        enableReinitialize
      />
    </Modal>
  );
};

export default ModalUpdate2FA;
