import React, { useEffect, useState,  } from 'react';
import Modal from '@/components/modal'
import Input from '@/components/input'
import Field from '@/components/field'
import Notification from '@/components/notification'
import Button from '@/components/button'
import { Formik, Form } from 'formik'
import { object, string } from 'yup'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux'
import PhoneInput from 'react-phone-input-2'
import _ from 'lodash'
import 'react-phone-input-2/lib/style.css'

const { updateUser } = actions;
const ModalUpdate = ({ close, isOpen, reload, phone_number, phone_code, id, translate }) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false)
	const [countryCode, setCountryCode] = useState('')

	useEffect(() => {
		setCountryCode(phone_code || '')
	}, [phone_code])

	const _onSubmit = (values) => {
	
		const payload = {
			phone_number: values.phone_number, phone_code: countryCode
		}
		setIsLoading(true)
		dispatch(
			updateUser({id, payload}, (action) => {
				setIsLoading(false)
				if (action === TYPES.UPDATE_USER_SUCCESS) {
					reload()
					_close()
					return Notification.success(translate('success.update-phone-success'))
				}
			})
		)
  }

	const _close = () => {
		close()
		setCountryCode(phone_code)
	}

	const _renderForm = ({ handleSubmit, ...form }) => {
		return (
			<div>
				<Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
					<div className="row">
						<div className="col-xs-12">

							<div className="form-login mb-15">
								<label className="form-label">{translate('users.country-code')}</label>
								<div className="controls form-country-code">
									<PhoneInput
										enableSearch
										value={countryCode}
										onChange={(phone) => setCountryCode(`+${phone}`)}
									/>
									<Input
										className="value-country-code"
										value = {countryCode ? `${countryCode}`: ''}
										readOnly
									/>
								</div>
							</div>

							<div className="form-login">
								<label className="form-label">{translate('utils.phone')}</label>
								<div className="controls">
									<Field
										form={form}
										name="phone_number"
										component={Input}
									/>
								</div>
							</div>

							<div className="text-center">
								<Button
									className="btn btn-primary btn-corner"
									onClick={handleSubmit}
									loading={isLoading}
									disabled={isLoading || !countryCode}
								>{translate('utils.update')}
								</Button>
							</div>
						</div>
					</div>
				</Form>
			</div>
		)
	}

	const validationSchema = object().shape({
		phone_number: string().required()
	})
	return (
		<Modal
			visible={isOpen}
			onCancel={_close}
			title={translate('users.update-phone')}
			destroyOnClose={true}
		>
			<Formik
				validateOnChange={false}
				validateOnBlur={false}
				initialValues={{ phone_number }}
				validationSchema={validationSchema}
				onSubmit={_onSubmit}
				component={_renderForm}
				enableReinitialize
			/>
		</Modal>

	)
}

export default ModalUpdate