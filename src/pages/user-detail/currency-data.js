import React from 'react'
import HeaderForm from '@/components/header-form'
import './style.scss'
import _ from 'lodash';
import Button from '@/components/button'
import DatePicker from '@/components/date-picker'
import moment from 'moment'


const CurrencyData = ({ translate, dataCurrency, from, setFrom, to, setTo, onSearch }) => {
  const handleSearch = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    const date = moment(value).format('YYYY-MM-DD')
    if (name === "from") {
      setFrom(date)
    } else {
      setTo(date)
    }
  }

  return (
    <div>
      <div className="col-lg-12">
        <section className="box">
          <HeaderForm
            nameForm={translate('utils.detail')}
          />
          <div className="content-body">
            <div className="row">
              <div className="field col-lg-9">
                <label htmlFor="field1">{translate('utils.time')}</label>
                <div className="form-time">
                  <DatePicker
                    name="from" placeholder={translate('utils.start')}
                    value={from ? moment(from) : ''}
                    onChange={handleSearch} />
                  <DatePicker
                    name="to" placeholder={translate('utils.end')}
                    value={to ? moment(to) : ''}
                    onChange={handleSearch} />
                  <Button
                    margin={true}
                    onClick={() => onSearch({ from, to })}
                    disabled={(from === '' && to !== '') || ((from !== '' && to === ''))}>
                    {translate('utils.search')}
                  </Button>
                  <Button
                    margin={true}
                    onClick={() => {
                      setFrom('')
                      setTo('')
                    }}
                  >
                    {translate('utils.reset')}
                  </Button>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3">
                <h5 className="mt-2 mb-2">{translate('utils.all-bought-tsi')}: </h5>
              </div>

              <div className="col-lg-3">
                <h5 className="mt-2 mb-2 green-text boldy">{dataCurrency.all_tosi_bought}</h5>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3">
                <h5 className="mt-2 mb-2">{translate('utils.amount-now')}: </h5>
              </div>

              <div className="col-lg-3">
                <h5 className="mt-2 mb-2 green-text boldy">{dataCurrency.current_tosi}</h5>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3">
                <h5 className="mt-2 mb-2">{translate('group.dollar-total')}: </h5>
              </div>

              <div className="col-lg-3">
                <h5 className="mt-2 mb-2 green-text boldy">{dataCurrency.tosi_group_total}</h5>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3">
                <h5 className="mt-2 mb-2">{translate('referral.bonus-tsi')}: </h5>
              </div>

              <div className="col-lg-3">
                <h5 className="mt-2 mb-2 green-text boldy">{dataCurrency.tosi_bonus_total}</h5>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3">
                <h5 className="mt-2 mb-2">{translate('referral.invest-tsi')}: </h5>
              </div>
              <div className="col-lg-9">
                {dataCurrency.investments && dataCurrency.investments.map(investment => {
                  return (
                    <h5 className="mt-2 mb-2 green-text boldy">{investment.tosi_budget} - {translate('utils.time') + ": " + investment.number_of_months + " Months"} - {translate('utils.create-at')}: {moment(investment.created_at).format('YYYY-MM-DD')} </h5>
                  )
                })}
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3">
                <h5 className="mt-2 mb-2">Lãi chưa rút: </h5>
              </div>

              <div className="col-lg-3">
                <h5 className="mt-2 mb-2 green-text boldy">{dataCurrency.tosi_interest_rate}</h5>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3">
                <h5 className="mt-2 mb-2">Lãi đã rút: </h5>
              </div>

              <div className="col-lg-3">
                {dataCurrency.withdrawTotal && Object.keys(dataCurrency.withdrawTotal).length > 0 ?
                  Object.keys(dataCurrency.withdrawTotal).map(x => {
                    return (
                      <h5 className="mt-2 mb-2 green-text boldy">{dataCurrency.withdrawTotal[x]} {x == 1 ? 'BNB' : x == 2 ? 'ETH' : x == 3 ? 'XRP' : x == 4 ? 'TRX' : x == 5 ? 'BNB' : x == 6 ? 'TSI' : 'USDT'}</h5>
                    )
                  }) : <h5 className="mt-2 mb-2 green-text boldy">0 USDT </h5>}
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

export default CurrencyData
