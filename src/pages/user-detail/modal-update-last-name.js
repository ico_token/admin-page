import React, { useState } from 'react'
import { Formik, Form } from 'formik'
import { object, string } from 'yup'
import { useDispatch } from 'react-redux'
import Modal from '@/components/modal'
import Input from '@/components/input'
import Field from '@/components/field'
import Notification from '@/components/notification'
import Button from '@/components/button'
import { TYPES, actions } from '@/store/actions'

const { updateUser } = actions

const ModalUpdateLastName = ({ close, isOpen, reload, lastName, id, translate }) => {
  const dispatch = useDispatch()
  const [isLoading, setIsLoading] = useState(false)

  const onClose = () => {
    close()
  }

  const onSubmit = (values) => {
    const payload = {
      last_name: values.lastName
    }

    setIsLoading(true)
    dispatch(
      updateUser({ id, payload }, (action) => {
        setIsLoading(false)
        if (action === TYPES.UPDATE_USER_SUCCESS) {
          reload()
          onClose()
          return Notification.success(translate('success.update_success'))
        }
      })
    )
  }

  const renderForm = ({ handleSubmit, ...form }) => (
    <div>
      <Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
        <div className="row">
          <div className="col-xs-12">
            <div className="form-login">
              <label className="form-label">{translate('utils.last_name')}</label>
              <div className="controls">
                <Field
                  form={form}
                  name="lastName"
                  component={Input}
                />
              </div>
            </div>

            <div className="text-center">
              <Button
                className="btn btn-primary btn-corner"
                onClick={handleSubmit}
                loading={isLoading}
                disabled={isLoading}
              >{translate('utils.update')}
              </Button>
            </div>
          </div>
        </div>
      </Form>
    </div>
  )

  const validationSchema = object().shape({
    lastName: string().required()
  })

  return (
    <Modal
      visible={isOpen}
      onCancel={onClose}
      title={translate('users.update_last_name')}
      destroyOnClose
    >
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{ lastName }}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        component={renderForm}
        enableReinitialize
      />
    </Modal>
  )
}

export default ModalUpdateLastName
