import React, { useState } from "react";
import { Formik, Form } from "formik";
import { object, string } from "yup";
import { useDispatch } from "react-redux";
import Modal from "@/components/modal";
import Notification from "@/components/notification";
import Button from "@/components/button";
import { TYPES, actions } from "@/store/actions";
import { BYTE_TO_MB, MAX_SIZE_IMG } from "../../utils/constant";
import { getBase64File } from "../../utils/common";

const { updateUser, getSignS3, uploadS3 } = actions;

const ModalUpdateIMG = ({
  close,
  isOpen,
  reload,
  name,
  kyc,
  id,
  translate,
  url
}) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [file, setFile] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);
  const urlImage = kyc ?   kyc[name] : url

  const onClose = () => {
    close();
  };

  const onSubmit = (values) => {
    try {
      let url = urlImage;
      if (file) {
        setIsLoading(true);
        const payload = {
          file_type: file.type,
          file_name: file.name,
        };
        dispatch(
          getSignS3(payload, (action1, data1) => {
            if (action1 === TYPES.GET_SIGN_S3_SUCCESS) {
              const uploadPayload = {
                url: data1.signedRequest,
                file,
              };
              dispatch(
                uploadS3(uploadPayload, () => {
                  url = data1.url;
                  //   update image
                  dispatch(
                    updateUser(
                      {
                        id,
                        payload: {
                          [name]: url,
                        },
                      },
                      (action) => {
                        setIsLoading(false);
                        if (action === TYPES.UPDATE_USER_SUCCESS) {
                          reload();
                          onClose();
                          setFile(null);
                          setPreviewImage(null);
                          return Notification.success(
                            translate("success.update_success")
                          );
                        }
                      }
                    )
                  );
                })
              );
            } else {
              setIsLoading(false);
            }
          })
        );
      } else {
        onClose();
      }
    } catch (error) {
      setIsLoading(false);
    }
  };

  const handleChange = async (data) => {
    try {
      const file = data.target?.files[0];
      if (file?.size / BYTE_TO_MB > MAX_SIZE_IMG) {
        return;
      }
      const link = await getBase64File(file);
      setPreviewImage(link);
      setFile(file);
    } catch (error) {
    }
  };

  const renderForm = () => (
    <div>
      <Form
        id="msg_validate"
        action="#"
        novalidate="novalidate"
        className="no-mb no-mt"
      >
        <div className="row">
          <div className="col-xs-12">
            <div className="form-login">
              <label className="form-label">
                {translate("users.update_image")}
              </label>
              <div className="controls">
                <input
                  accept="image/*"
                  multiple={false}
                  placeholder={translate("kyc.please_upload_image")}
                  onChange={handleChange}
                  className="upload-image"
                  type="file"
                />
              </div>
            </div>

            <div className="text-center">
              <Button
                className="btn btn-primary btn-corner"
                onClick={onSubmit}
                loading={isLoading}
                disabled={isLoading}
              >
                {translate("utils.update")}
              </Button>
            </div>
          </div>
          <div className="col-xs-12">
            <img src={previewImage || urlImage} alt="" />
          </div>
        </div>
      </Form>
    </div>
  );

  const validationSchema = object().shape({
    id_number: string().required(),
  });

  return (
    <Modal
      visible={isOpen}
      onCancel={onClose}
      title={translate("users.update_image")}
      destroyOnClose
    >
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{ name }}
        validationSchema={validationSchema}
        component={renderForm}
        enableReinitialize
      />
    </Modal>
  );
};

export default ModalUpdateIMG;
