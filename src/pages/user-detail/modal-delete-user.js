import React, { useState } from 'react'
import { Formik, Form } from 'formik'
import { useDispatch } from 'react-redux'
import Modal from '@/components/modal'
import Notification from '@/components/notification'
import Button from '@/components/button'
import { TYPES, actions } from '@/store/actions'

const { deleteUser } = actions

const ModalDeleteUser = ({ close, isOpen, reload, id, email, translate, history }) => {
  const dispatch = useDispatch()
  const [isLoading, setIsLoading] = useState(false)

  const onClose = () => {
    close()
  }

  const onSubmit = () => {
    setIsLoading(true)
    dispatch(
        deleteUser(id, (action, data) => {
        setIsLoading(false)
        if (action === TYPES.DELETE_USER_SUCCESS) {
          reload()
          onClose()
          Notification.success(translate('success.DELETE_USER'))
          history.push('/users')
        } else if (action === TYPES.DELETE_USER_FAILURE){
          Notification.error(data.code? data.code : translate('error-messages.SYSTEM_ERROR'))
        }
      })
    )
  }

  const renderForm = ({ handleSubmit, ...form }) => (
    <div>
      <Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
        <div className="row">
          <div className="col-xs-12">
            <div className="form-login">
              <label className="form-label">{translate('users.confirm_delete_user')}</label>
            </div>

            <div className="text-center">
              <Button
                className="btn btn-danger btn-corner"
                onClick={handleSubmit}
                loading={isLoading}
                disabled={isLoading}
              >{translate('utils.yes')}
              </Button>

              <Button
                className="btn btn-primary btn-corner"
                onClick={onClose}
                loading={isLoading}
                disabled={isLoading}
              >{translate('utils.no')}
              </Button>
            </div>
          </div>
        </div>
      </Form>
    </div>
  )

  return (
    <Modal
      visible={isOpen}
      onCancel={onClose}
      title={translate('utils.delete') + " " + translate('users.users')}
      destroyOnClose
    >
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        onSubmit={onSubmit}
        component={renderForm}
        enableReinitialize
      />
    </Modal>
  )
}

export default ModalDeleteUser
