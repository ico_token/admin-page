import React, { useEffect, useState } from 'react'
import VerifyKyc from './verify-kyc'
import Transaction from '@/components/transaction'
import Profile from './profile'
import CurrencyData from './currency-data'
import TitlePage from '@/components/title-page'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
import '@/pages/users/style.scss';
import { withLocalize } from 'react-localize-redux'

const { getUserById, getUserCurrency } = actions;
const UserDetail = ({ history, match, translate }) => {
  const { id } = match.params;
  const dispatch = useDispatch();
  const [dataDetail, setDataDetail] = useState({});
  const [dataCurrency, setDataCurrency] = useState({});
  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');

  useEffect(() => {
    getData()
    getCurrencyData({})
  }, []);

  const getData = () => {
    dispatch(
      getUserById(id, (action, data) => {
        if (action === TYPES.GET_USER_BY_ID_SUCCESS) {
          setDataDetail(data);
        }
      })
    )
  };

  const getCurrencyData = (value) => {
    const payload = {}

    if (value.from && value.to && value.from !== ''  && value.to !== '') {
      payload.from = value.from
      payload.to = value.to
    }

    dispatch(
      getUserCurrency({id, payload }, (action, data) => {
        if (action === TYPES.GET_USER_CURRENCY_SUCCESS) {
          setDataCurrency(data);
        }
      })
    )
  }

  const reload = () => {
    getData()
  }

  return (
    <div>
      <TitlePage
        namePage={translate('users.user-detail')}
      />
      <div className="col-lg-12">
        <div className="row">
          <Profile
            dataDetail={dataDetail}
            reload={reload}
            translate={translate}
            history={history}
          />

          <div className="col-lg-8">
            <VerifyKyc 
              dataDetail={dataDetail}
              user_id={id}
              reload={reload}
              translate={translate}
            />
          </div>
        </div>

      </div>
      <div className="col-lg-12">
        <div className="row">
          <CurrencyData 
            translate={translate} 
            dataCurrency={dataCurrency}
            from={from}
            setFrom={setFrom}
            to={to}
            setTo={setTo}
            onSearch={getCurrencyData}/>
        </div>
      </div>
      <div className="col-lg-12">
        <div className="row">
          <Transaction
            params={{user_id: id}}
          />
        </div>
      </div>
    </div>
  )
}

export default withLocalize(UserDetail)

