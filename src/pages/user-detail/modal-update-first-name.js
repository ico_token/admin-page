import React, { useState } from 'react'
import { Formik, Form } from 'formik'
import { object, string } from 'yup'
import { useDispatch } from 'react-redux'
import Modal from '@/components/modal'
import Input from '@/components/input'
import Field from '@/components/field'
import Notification from '@/components/notification'
import Button from '@/components/button'
import { TYPES, actions } from '@/store/actions'

const { updateUser } = actions

const ModalUpdateFirstName = ({ close, isOpen, reload, firstName, id, translate }) => {
  const dispatch = useDispatch()
  const [isLoading, setIsLoading] = useState(false)

  const onClose = () => {
    close()
  }

  const onSubmit = (values) => {
    const payload = {
      first_name: values.firstName
    }

    setIsLoading(true)
    dispatch(
      updateUser({ id, payload }, (action) => {
        setIsLoading(false)
        if (action === TYPES.UPDATE_USER_SUCCESS) {
          reload()
          onClose()
          return Notification.success(translate('success.update_success'))
        }
      })
    )
  }

  const renderForm = ({ handleSubmit, ...form }) => (
    <div>
      <Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
        <div className="row">
          <div className="col-xs-12">
            <div className="form-login">
              <label className="form-label">{translate('utils.name')}</label>
              <div className="controls">
                <Field
                  form={form}
                  name="firstName"
                  component={Input}
                />
              </div>
            </div>

            <div className="text-center">
              <Button
                className="btn btn-primary btn-corner"
                onClick={handleSubmit}
                loading={isLoading}
                disabled={isLoading}
              >{translate('utils.update')}
              </Button>
            </div>
          </div>
        </div>
      </Form>
    </div>
  )

  const validationSchema = object().shape({
    firstName: string().required()
  })

  return (
    <Modal
      visible={isOpen}
      onCancel={onClose}
      title={translate('users.update_first_name')}
      destroyOnClose
    >
      <Formik
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={{ firstName }}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        component={renderForm}
        enableReinitialize
      />
    </Modal>
  )
}

export default ModalUpdateFirstName
