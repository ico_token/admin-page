import React, { useState } from "react";
import HeaderForm from "@/components/header-form";
import "./style.scss";
import { useDispatch } from "react-redux";
import { TYPES, actions } from "@/store/actions";
import Notification from "@/components/notification";
import _ from "lodash";
import Popconfirm from "@/components/popconfirm";
import Button from "@/components/button";
import ModalReason from "./modal-reason-reject";
import ModalUpdateIMG from "./modal-update-img";

const { verifyKyc } = actions;

const Information = ({ dataDetail, user_id, reload, translate }) => {
  const dispatch = useDispatch();

  const { kyc, is_kyc, id } = dataDetail;
  const [isLoading, setIsLoading] = useState("");
  const [openModalReason, setOpenModalReason] = useState(false);
  const [openModalIMG, setOpenModalIMG] = useState(false);
  const [nameImg, setNameImg] = useState("front_identity_card");

  const updateKyc = ({ status, type }) => {
    const payload = {
      id: user_id,
      is_kyc: status,
    };
    setIsLoading(type);
    dispatch(
      verifyKyc(payload, (action) => {
        setIsLoading("");
        if (action === TYPES.VERIFY_KYC_SUCCESS) {
          reload();
          return Notification.success(translate("success.VERIFY_KYC_SUCCESS"));
        }
      })
    );
  };

  const renderApproved = () => {
    if (is_kyc) {
      return (
        <img
          alt=""
          name="front"
          src={"/assets/images/approved-img.png"}
          className="img-responsive img-approved"
        />
      );
    }
  };

  const onClickChangeImg = (name) => {
    setNameImg(name);
    setOpenModalIMG(true);
  };

  return (
    <div>
      <section className="box has-border-left-3">
        <HeaderForm nameForm={translate("users.verify-kyc")} />
        <div className="content-body">
          <div className="row">
            <div className="col-lg-6">
              <label className="form-label">{translate("users.front")}: </label>
              <div className="option-identity-wrapper">
                <a
                  className="box_setting fa fa-cog"
                  onClick={() => onClickChangeImg("front_identity_card")}
                />
                <img
                  alt=""
                  name="front"
                  src={
                    kyc?.front_identity_card || "/assets/images/info-card.png"
                  }
                  className="img-responsive"
                />
                {renderApproved()}
              </div>
            </div>
            <div className="col-lg-6">
              <label className="form-label">{translate("users.back")}: </label>
              <div className="option-identity-wrapper">
                <a
                  className="box_setting fa fa-cog"
                  onClick={() => onClickChangeImg("back_identity_card")}
                />
                <img
                  alt=""
                  name="back"
                  src={
                    kyc?.back_identity_card || "/assets/images/info-card.png"
                  }
                  className="img-responsive"
                />
                {renderApproved()}
              </div>
            </div>
            <div className="col-lg-6">
              <label className="form-label">Selfie: </label>
              <div className="option-identity-wrapper">
                <a
                  className="box_setting fa fa-cog"
                  onClick={() => onClickChangeImg("selfie")}
                />
                <img
                  alt=""
                  name="selfie"
                  src={kyc?.selfie || "/assets/images/user-male.png"}
                  className="img-responsive"
                />
                {renderApproved()}
              </div>
            </div>
          </div>

          {!is_kyc && !_.isEmpty(kyc) && (
            <div className="form-btn-verify">
              <Popconfirm
                title={translate("users.approve-text")}
                onConfirm={() => updateKyc({ status: true, type: "approve" })}
                okText={translate("utils.yes")}
                cancelText={translate("utils.no")}
              >
                <Button
                  className="btn btn-success mt-10 btn-corner mr-15"
                  loading={isLoading === "approve"}
                  disabled={isLoading === "approve"}
                >
                  {translate("utils.approve")}
                </Button>
              </Popconfirm>

              <Button
                className="btn btn-danger btn-corner mt-10"
                loading={isLoading === "reject"}
                disabled={isLoading === "reject"}
                onClick={() => setOpenModalReason(true)}
              >
                {translate("utils.reject")}
              </Button>
            </div>
          )}
        </div>
      </section>
      {openModalReason && (
        <ModalReason
          close={() => setOpenModalReason(false)}
          isOpen={openModalReason}
          id={user_id}
          translate={translate}
          reload={() => reload()}
        />
      )}

      <ModalUpdateIMG
        close={() => setOpenModalIMG(false)}
        isOpen={openModalIMG}
        reload={reload}
        translate={translate}
        name={nameImg}
        kyc={kyc}
        id={id}
      />
    </div>
  );
};

export default Information;
