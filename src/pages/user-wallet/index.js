import React, { useState } from 'react';
import { withLocalize } from 'react-localize-redux'
import UserWalletDetails from '@/components/user-wallet'
import TitlePage from '@/components/title-page'
import _ from 'lodash';
// import Input from '@/components/input'
// import Button from '@/components/button'
// import DatePicker from '@/components/date-picker'
// import Select from './selectType'
// import { TRANSACTION_TYPE, FROM_TIME } from '@/utils/constant'
// import moment from 'moment'
// import Notification from '@/components/notification'
import './style.scss'
// import { Collapse } from 'antd';
import 'antd/es/collapse/style/css'
// const { Panel } = Collapse;

const UserWallet = ({ translate }) => {
  // const [valueSearch, setValueSearch] = useState({});
  // const [valueSearchPush, setValueSearchPush] = useState({});
  // const changeSearch = (e) => {
  //   const name = e.target.name;
  //   const value = e.target.value;
  //   if (name === "type") {
  //     if (value !== "all") {
  //       setValueSearch({
  //         ...valueSearch,
  //         [name]: TRANSACTION_TYPE.findIndex((element) => element === value)
  //       })
  //     } else {
  //       const newData = { ...valueSearch };
  //       delete newData.type
  //       setValueSearch(newData)
  //     }
  //   } else if(name === "from" || name === "to") {
  //     if(value) {
  //       setValueSearch({ ...valueSearch, [name]: moment(value).format('YYYY-MM-DD') })
  //     } else {
  //       const newData = { ...valueSearch };
  //       delete newData[name]
  //       setValueSearch(newData)
  //     }
  //   } else { setValueSearch({ ...valueSearch, [name]: value.trim() }) }
  // };

  // const handleSearch = () => {
  //   if(('from' in valueSearch && 'to' in valueSearch)
  //   || ('from' in valueSearch === false && 'to' in valueSearch === false)) {
  //     setValueSearchPush(valueSearch)
  //   } else {
  //     return Notification.error(translate('transactions.search-date'))
  //   }

  // };

  // const handleReset = () => {
  //   setValueSearch({})
  //   setValueSearchPush({})
  // }

  return (
    <div>
      <TitlePage namePage={translate('wallet.user-wallet')} />

      <div className="col-lg-12">
        <section className="box ">
          {/* <div className="col-lg-12 mt-15">
            <Collapse >
              <Panel header={translate('utils.filter')}>
                <div className="field col-sm-4"	>
                  <label>{translate('utils.email')}</label>
                  <Input name="email" onChange={changeSearch} value={valueSearch.email}/>
                </div>

                <div className="field col-sm-4">
                  <label>{translate('transactions.type')}</label>
                  <Select
                    name="type" onChange={changeSearch}
                    value={TRANSACTION_TYPE[valueSearch.type] || 'all'}
                    options={['all', ...TRANSACTION_TYPE]}
                    translate={translate}
                  />
                </div>

                <div className="field col-sm-4">
                  <label htmlFor="field1">{translate('utils.time')}</label>
                  <div className="form-time">
                    <DatePicker
                      name="from" placeholder={translate('utils.start')}
                      value={valueSearch.from ? moment(valueSearch.from) : ''}
                      onChange={changeSearch} />

                    <DatePicker
                      name="to" placeholder={translate('utils.end')}
                      value={valueSearch.to ? moment(valueSearch.to) : ''}
                      onChange={changeSearch}/>
                  </div>
                </div>

                <div>
                <Button
                  margin={true}
                  onClick={handleSearch}
                >{translate('utils.search')}</Button>

                <Button
                  onClick={handleReset}
                >{translate('utils.reset')}</Button>
                </div>


              </Panel>
            </Collapse>
          </div> */}
          <UserWalletDetails
            // params={valueSearchPush}
          />
        </section>
      </div>
    </div>

  )
}

export default withLocalize(UserWallet)
