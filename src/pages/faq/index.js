import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withLocalize } from 'react-localize-redux'
import { TYPES, actions } from '@/store/actions'

// Component
import Button from '@/components/button'
import Notification from '@/components/notification'
import TitlePage from '@/components/title-page'

import './style.scss';

const { getFaq, deleteFaq } = actions;

const FAQ = ({ history, translate }) => {
  const dispatch = useDispatch();
  const { submitting, faqList } = useSelector(state => state.faq)

  const FAQ_CATEGORY = {
    0: translate('faq.GENERAL'),
    1: translate('faq.PAYMENT'),
    2: translate('faq.WITHDRAW'),
    3: translate('faq.OTHER')
  }

  useEffect(() => {
    getListFaq()
  }, []);

  // Get data FAQ
  const getListFaq = () => dispatch(getFaq());

  // Delete FAQ
  const onDelete = (id) => {
    dispatch(deleteFaq({ id: +id }, (action, data, error) => {
      if (action === TYPES.DELETE_FAQ_SUCCESS) {
        getListFaq()
        return Notification.success(translate('success.DELETE_FAQ'))
      }
    }))
  }

  return (
    <div className="faq-page">
      <TitlePage namePage={translate('faq.faq')} />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header faq_header">
                <h2 className="title pull-left">{translate('faq.list-faq')}</h2>
                <Button
                  className="btn btn-primary"
                  onClick={() => history.push('/faq-create')}
                >{translate('utils.create')}</Button>
              </header>
              <div className="content-body">

                <div className="row">
                  <div className="col-xs-12">
                    <div className="table-responsive" data-pattern="priority-columns">
                      <table id="tech-companies-1" className="table table-small-font no-mb table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>{translate('faq.category')}</th>
                            <th>{translate('faq.question')}</th>
                            <th>{translate('faq.answer')}</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            faqList.map((item, i) => (
                              <tr key={i}>
                                <td>{FAQ_CATEGORY[Number(item?.category)]}</td>
                                <td>{item?.question}</td>
                                <td>{item?.answer}</td>
                                <td className="text-right box-action">
                                  <Button
                                    className="btn btn-primary"
                                    onClick={() => {
                                      history.push(`/faq-update/${item.id}`)
                                    }}
                                  >{translate('utils.update')}</Button>
                                  <Button
                                    className="btn btn-danger"
                                    loading={!!submitting === TYPES.DELETE_FAQ_REQUEST}
                                    disabled={!!submitting}
                                    onClick={() => onDelete(item.id)}
                                  >{translate('utils.delete')}</Button>
                                </td>
                              </tr>
                            ))
                          }
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </section>
          </div>
        </section>
      </div>
    </div>
  )
}

export default withLocalize(FAQ)
