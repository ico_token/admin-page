import React, { useEffect, useState,  } from 'react';
import Modal from '@/components/modal'
import Input from '@/components/input'
import Field from '@/components/field'
import Notification from '@/components/notification'
import Button from '@/components/button'
import { Formik, Form } from 'formik'
import { object, number } from 'yup'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

const { updateUser } = actions;
const ModalUpdate = ({ close, isOpen, reload, userUpdate, translate , field}) => {


  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false)
  const {id, email } = userUpdate;
  const tosi_total = userUpdate[field]

	const _onSubmit = (values) => {
		const { tosi_total } = values;
		const payload = {
			[field]: `${tosi_total}`
		}
		setIsLoading(true)
		dispatch(
			updateUser({id, payload}, (action) => {
				setIsLoading(false)
				if (action === TYPES.UPDATE_USER_SUCCESS) {
					reload()
					close()
					return Notification.success(translate('success.update-amount-success'))
				}
			})
		)
  }

	const _renderForm = ({ handleSubmit, ...form }) => {
		return (
			<div>
				<Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
					<div className="row">
						<div className="col-xs-12">
							<div className="form-login">
								<label className="form-label">{translate('utils.email')}</label>
								<div className="controls">
									<Field
										value={email}
										name="email"
										readOnly
										component={Input}
									/>
								</div>
							</div>

							<div className="form-login">
								<label className="form-label">{translate('utils.amount-now')}</label>
								<div className="controls">
									<Field
										value={tosi_total}
										name="tosi_total"
										readOnly
										component={Input}
									/>
								</div>
							</div>

							<div className="form-login">
								<label className="form-label">{translate('utils.amount')}</label>
								<div className="controls">
									<Field
										form={form}
										name="tosi_total"
										type="number"
										component={Input}
									/>
								</div>
							</div>

							<div className="text-center">
								<Button
									className="btn btn-primary btn-corner"
									onClick={handleSubmit}
									loading={isLoading}
									disabled={isLoading}
								>{translate('utils.update')}
								</Button>
							</div>
						</div>
					</div>
				</Form>
			</div>
		)
	}

	const validationSchema = object().shape({
		// tosi_total: number().required().moreThan(
		// 	tosi_total,'more-than-now-total'
		// )
	})
	return (
		<Modal
			visible={isOpen}
			onCancel={close}
			title={translate('users.update-amount')}
			destroyOnClose={true}
		>
			<Formik
				validateOnChange={false}
				validateOnBlur={false}
				initialValues={{ tosi_total }}
				validationSchema={validationSchema}
				onSubmit={_onSubmit}
				component={_renderForm}
				enableReinitialize
			/>
		</Modal>

	)
}

export default ModalUpdate