import React, { useEffect, useState } from "react";
import { withLocalize } from "react-localize-redux";
import Input from "@/components/input";
import Button from "@/components/button";
import Pagination from "@/components/pagination";
import Select from "@/components/select";
import TitlePage from "@/components/title-page";
import { TYPES, actions } from "@/store/actions";
import { useDispatch } from "react-redux";
import Popconfirm from "@/components/popconfirm";
import { RANK, RANK_IMG, FROM_TIME } from "@/utils/constant";
import moment from "moment";
import { Collapse } from "antd";
import ModalUpdateAmount from "./modal-update-amount";
import "antd/es/collapse/style/css";
import "./style.scss";
import Notification from "@/components/notification";
import { convertUsd, formatAmount } from "../../utils/common";
import Configs from "../../configs";
import FileSaver from "file-saver";
import * as XLSX from "xlsx";
import { async } from "regenerator-runtime";
import { MainApi } from "../../api/endpoint";

const { Panel } = Collapse;
const tsiToUsd = Configs.TSI_TO_USD;

const { getListUsers, handleLockUser } = actions;
const User = ({ translate }) => {
  const mockSearch = {
    page: 1,
    limit: 10,
  };
  const OPTION_KYC = [
    { name: translate("transactions.all"), value: "all" },
    { name: translate("users.verified"), value: 1 },
    { name: translate("users.not-verify"), value: 0 },
    { name: translate("utils.waiting"), value: 2 },
  ];

  const dispatch = useDispatch();
  const [listUser, setListUser] = useState([]);
  const [totalUser, setTotalUser] = useState(0);
  const [page, setPage] = useState(mockSearch.page);
  const [limit] = useState(mockSearch.limit);
  const [isLoading, setIsLoading] = useState(false);
  const [valueSearch, setValueSearch] = useState(mockSearch);
  const [userUpdate, setUserUpdate] = useState({});
  const [openModalAmount, setOpenModalAmount] = useState(false);
  const [fieldUpdate, setFieldUpdate] = useState(null);
  const [isLoadingExport, setIsLoadingExport] = useState(false);

  useEffect(() => {
    getList(valueSearch);
  }, []);

  const getList = (value) => {
    setIsLoading(true);
    dispatch(
      getListUsers(value, (action, data) => {
        setIsLoading(false);
        if (action === TYPES.GET_LIST_USERS_SUCCESS) {
          setListUser(data.rows);
          setTotalUser(data.count);
        }
      })
    );
  };
  const reload = () => {
    getList(valueSearch);
  };

  const onChangePage = (p) => {
    setPage(p);
    setValueSearch({ ...valueSearch, page: p });
    getList({ ...valueSearch, page: p });
  };

  const changeSearch = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if (name === "is_kyc") {
      if (value === "all") {
        const newData = { ...valueSearch };
        delete newData.is_kyc;
        setValueSearch(newData);
      } else {
        setValueSearch({
          ...valueSearch,
          [name]: value,
        });
      }
    } else {
      setValueSearch({
        ...valueSearch,
        [name]: value.trim(),
      });
    }
  };

  const handleSearch = () => {
    getList(valueSearch);
  };

  const handleReset = () => {
    setValueSearch(mockSearch);
    getList(mockSearch);
  };

  const openModalUpdateAmount = ({ item, fieldUpdate }) => {
    setFieldUpdate(fieldUpdate)
    setUserUpdate(item);
    setOpenModalAmount(true);
  };

  const handleLock = ({ is_locked, id }) => {
    dispatch(
      handleLockUser({ is_locked, id }, (action, data) => {
        setIsLoading(false);
        if (action === TYPES.HANDLE_LOCK_USER_SUCCESS) {
          getList(valueSearch);
          return Notification.success(
            translate("success.HANDLE_LOCK_USER_SUCCESS")
          );
        }
      })
    );
  };

  const handleExport = async () => {
    try {
      setIsLoadingExport(true);
      const res = await MainApi.get("/users", {
        limit: 4000,
        page: 1,
      });
      if (res && res.result && res.result.rows) {
        const apiData = res?.result?.rows.map((r) => ({
          Email: r.email,
          Wallet: r.tosi_address,
          [`Tosi Interest Rate`]: r.tosi_interest_rate,
          ["Tosi Total"]: r.tosi_total,
        }));
        console.log({
          apiData,
        });
        const fileType =
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
        const fileExtension = ".xlsx";
        const ws = XLSX.utils.json_to_sheet(apiData);
        const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
        const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
        const data = new Blob([excelBuffer], { type: fileType });
        FileSaver.saveAs(data, `users_${new Date().getTime()}` + fileExtension);
      }
      setIsLoadingExport(false);
    } catch (error) {
      console.log("error");
      setIsLoadingExport(false);
    }
  };

  return (
    <div>
      <TitlePage namePage={translate("users.users")} />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12 mt-15">
            <Collapse>
              <Panel header={translate("utils.filter")}>
                <div>
                  <div className="field col-sm-4">
                    <label>{translate("utils.email")}</label>
                    <Input
                      name="email"
                      onChange={changeSearch}
                      value={valueSearch.email}
                    />
                  </div>

                  <div className="field col-sm-4">
                    <label>{translate("utils.phone")}</label>
                    <Input
                      name="phone_number"
                      onChange={changeSearch}
                      value={valueSearch.phone_number}
                      type="number"
                    />
                  </div>

                  <div className="field col-sm-4">
                    <label>{translate("utils.kyc")}</label>
                    <Select
                      name="is_kyc"
                      onChange={changeSearch}
                      value={
                        [0, 1, 2].includes(valueSearch.is_kyc)
                          ? valueSearch.is_kyc
                          : translate("transactions.all")
                      }
                      options={OPTION_KYC}
                    />
                  </div>

                  <div className="field col-sm-6">
                    <label>{translate("utils.address")}</label>
                    <Input
                      name="tosi_address"
                      onChange={changeSearch}
                      value={valueSearch.tosi_address}
                    />
                  </div>

                  <div className="field col-sm-6">
                    <label>{translate("utils.age")}</label>
                    <div className="form-age">
                      <Input
                        name="start_age"
                        placeholder={translate("utils.start")}
                        onChange={changeSearch}
                        type="number"
                        value={valueSearch.start_age}
                      />
                      <Input
                        name="end_age"
                        placeholder={translate("utils.end")}
                        onChange={changeSearch}
                        type="number"
                        value={valueSearch.end_age}
                      />
                    </div>
                  </div>
                </div>

                <div>
                  <Button
                    margin={true}
                    onClick={handleSearch}
                    loading={isLoading}
                    disabled={isLoading}
                  >
                    {translate("utils.search")}
                  </Button>

                  <Button
                    onClick={handleReset}
                    loading={isLoading}
                    disabled={isLoading}
                  >
                    {translate("utils.reset")}
                  </Button>
                </div>
              </Panel>
            </Collapse>
          </div>

          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header">
                <h2 className="title pull-left">
                  {translate("users.list-users")}
                  <Button
                    onClick={handleExport}
                    loading={isLoadingExport}
                    disabled={isLoadingExport}
                  >
                    Export
                  </Button>
                </h2>
                
              </header>
              <div className="content-body">
                <div className="row">
                  <div className="col-xs-12">
                    <div
                      className="table-responsive"
                      data-pattern="priority-columns"
                    >
                      <table
                        id="tech-companies-1"
                        className="table vm trans table-small-font no-mb table-bordered table-striped"
                      >
                        <thead>
                          <tr>
                            <th>{translate("utils.email")}</th>
                            <th className="text-center">
                              {translate("utils.verified-email")}
                            </th>
                            <th>{translate("utils.address")}</th>
                            <th>{translate("utils.age")}</th>
                            <th>{translate("utils.phone")}</th>
                            <th className="text-center">
                              {translate("utils.rank")}
                            </th>
                            <th className="text-center">
                              {translate("utils.tsi-transfer-total")}
                            </th>
                            <th className="text-center">
                              {translate("utils.tosi_interest_rate")}
                            </th>
                            <th className="text-center">
                              {translate("utils.tosi_internal")}
                            </th>
                            <th className="text-center">
                              {translate("utils.verified-phone")}
                            </th>
                            <th className="text-center">
                              {translate("utils.kyc")}
                            </th>
                            <th className="text-center">
                              {translate("utils.create-at")}
                            </th>
                            <th className="text-center">
                              {translate("utils.locked")}
                            </th>
                            <th className="text-center">
                              {translate("utils.detail")}
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {listUser.map((item, i) => (
                            <tr key={i}>
                              <td>
                                <small className="text-muted">
                                  {item.email}
                                </small>
                              </td>
                              <td className="text-center">
                                {item.is_email_verified ? (
                                  <i className="fa fa-check-circle verification"></i>
                                ) : (
                                  <i className="fa fa-times-circle not-verification"></i>
                                )}
                              </td>
                              <td>
                                <small className="text-muted">
                                  {item.tosi_address}
                                </small>
                              </td>
                              <td>
                                <small className="text-muted">
                                  {item.user_info.age}
                                </small>
                              </td>
                              <td>
                                <small className="text-muted">
                                  {item.phone_number}
                                </small>
                              </td>
                              <td className="text-center">
                                {!!item.ref_info?.rank && (
                                  <img
                                    src={RANK_IMG[item.ref_info?.rank]}
                                    title={RANK[item.ref_info?.rank]}
                                    alt={RANK[item.ref_info?.rank]}
                                    className="img-table"
                                  />
                                )}
                              </td>
                       
                              <td className="text-center boldy green-text form-amount">
                                {item.tosi_total}
                                <a
                                  className="box_setting fa fa-cog ml-2"
                                  title={translate("users.update-amount")}
                                  onClick={() =>
                                    openModalUpdateAmount({ item, fieldUpdate : 'tosi_total'})
                                  }
                                ></a>
                              </td>
                              <td className="text-center w-200 boldy form-amount">
                                <small className="text-muted green-text ">
                                  {" "}
                                  {formatAmount(
                                    item.tosi_interest_rate
                                  )} TSI{" "}
                                </small>
                                <br />≈
                                <small className="text-muted green-text ">
                                  {" "}
                                  {formatAmount(
                                    convertUsd(
                                      item?.tosi_interest_rate,
                                      tsiToUsd
                                    )
                                  )}{" "}
                                  USD
                                </small>
                                <a
                                  className="box_setting fa fa-cog ml-2"
                                  title={translate("users.update-amount")}
                                  onClick={() =>
                                    openModalUpdateAmount({ item, fieldUpdate : 'tosi_interest_rate'})
                                  }
                                ></a>
                              </td>
                              <td className="text-center boldy green-text form-amount">
                                {item.tosi_internal}
                                <a
                                  className="box_setting fa fa-cog ml-2"
                                  title={translate("users.update-amount")}
                                  onClick={() =>
                                    openModalUpdateAmount({ item, fieldUpdate : 'tosi_internal'})
                                  }
                                ></a>
                              </td>
                              <td className="text-center">
                                {item.is_phone_verified ? (
                                  <i className="fa fa-check-circle verification"></i>
                                ) : (
                                  <i className="fa fa-times-circle not-verification"></i>
                                )}
                              </td>
                              <td className="text-center">
                                {item.is_kyc && (
                                  <i className="fa fa-check-circle verification"></i>
                                )}
                                {!item.is_kyc && item.kyc_id && (
                                  <i class="fa fa-spinner waiting"></i>
                                )}
                                {!item.is_kyc && !item.kyc_id && (
                                  <i className="fa fa-times-circle not-verification"></i>
                                )}
                              </td>
                              <td>
                                <small className="text-muted">
                                  {moment(item.created_at).format(FROM_TIME)}
                                </small>
                              </td>
                              <td className="text-center">
                                <div className="btn-group btn-group-justified btn-detail">
                                  {item.is_locked ? (
                                    <Popconfirm
                                      title={translate("utils.lock-confirm")}
                                      onConfirm={() =>
                                        handleLock({
                                          is_locked: !item.is_locked,
                                          id: item.id,
                                        })
                                      }
                                      okText={translate("utils.yes")}
                                      cancelText={translate("utils.no")}
                                    >
                                      <a className="btn btn-success btn-xs">
                                        {translate("utils.unlock")}
                                      </a>
                                    </Popconfirm>
                                  ) : (
                                    <Popconfirm
                                      title={translate("utils.unlock-confirm")}
                                      onConfirm={() =>
                                        handleLock({
                                          is_locked: !item.is_locked,
                                          id: item.id,
                                        })
                                      }
                                      okText={translate("utils.yes")}
                                      cancelText={translate("utils.no")}
                                    >
                                      <a className="btn btn-danger btn-xs">
                                        {translate("utils.lock")}
                                      </a>
                                    </Popconfirm>
                                  )}
                                </div>
                              </td>

                              <td className="green-text boldy text-center">
                                <div className="btn-group btn-group-justified btn-detail">
                                  <a
                                    className="btn btn-warning btn-xs"
                                    href={`/user-detail/${item.id}`}
                                    target="_blank"
                                  >
                                    {translate("utils.detail")}
                                  </a>
                                </div>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
          {totalUser > 10 && (
            <div className="col-lg-12">
              <Pagination
                current={page}
                onChange={onChangePage}
                total={totalUser}
                pageSize={limit}
              />
            </div>
          )}
        </section>
      </div>
      <ModalUpdateAmount
        close={() => setOpenModalAmount(false)}
        isOpen={openModalAmount}
        reload={reload}
        translate={translate}
        userUpdate={userUpdate}
        field={fieldUpdate}
      />
    </div>
  );
};

export default withLocalize(User);
