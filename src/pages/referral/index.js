import React, { useEffect, useState } from 'react';
import Pagination from '@/components/pagination'
import TitlePage from '@/components/title-page'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
import { withLocalize } from 'react-localize-redux'
import { RANK, RANK_IMG, FROM_TIME } from '@/utils/constant'
import './style.scss'
import moment from 'moment';

const { getReferral } = actions;
const Referral = ({translate}) => {
  const mockSearch = {
    page: 1,
    limit: 10
  }

  const dispatch = useDispatch();
  const [listReferralHistory, setListReferralHistory] = useState([]);
  const [totalReferralHistory, setTotalReferralHistory] = useState(0);
  const [page, setPage] = useState(mockSearch.page);
  const [limit] = useState(mockSearch.limit);
  const [valueSearch, setValueSearch] = useState(mockSearch);

  useEffect(() => {
    getList(valueSearch)
  }, []);

  const getList = (value) => {
    dispatch(
      getReferral(value, (action, data) => {
        if (action === TYPES.GET_REFERRAL_SUCCESS) {
          setListReferralHistory(data.rows);
          setTotalReferralHistory(data.count);
        }
      })
    )
  };

  const onChangePage = (p) => {
    setPage(p);
    setValueSearch({...valueSearch, page: p})
    getList({ ...valueSearch, page: p })
  };

  return (
    <div>
      <TitlePage
        namePage={translate('referral.referral')}
      />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header">
                <h2 className="title pull-left">{translate('referral.history-referral')}</h2>
              </header>
              <div className="content-body">

                <div className="row">
                  <div className="col-xs-12">
                    <div className="table-responsive" data-pattern="priority-columns">
                      <table id="tech-companies-1" className="table vm trans table-small-font no-mb table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>{translate('referral.name-parent')}</th>
                            <th>{translate('referral.email-parent')}</th>
                            <th className="text-center">{translate('referral.rank-parent')}</th>
                            <th className="text-center">{translate('referral.total-tsi')}</th>

                            <th>{translate('referral.name-child')}</th>
                            <th>{translate('referral.email-child')}</th>
                            <th className="text-center">{translate('referral.rank-child')}</th>

                            <th className="text-center">{translate('referral.invest-tsi')}</th>
                            <th className="text-center">{translate('referral.bonus-tsi')}</th>
                            <th className="text-center">{translate('utils.time')}</th>

                          </tr>
                        </thead>
                        <tbody>
                          {
                            listReferralHistory.map((item, i) => (
                              <tr key={i}>
                                <td><small className="text-muted">
                                  {`${item.parent_ref_id?.user_info?.first_name || ''} ${item.parent_ref_id?.user_info?.last_name || ''}`}
                                </small></td>
                                <td><small className="text-muted">{item.parent_ref_id.email}</small></td>
                                <td className="text-center">
                                  {!!item.rank_of_parent && (
                                    <img src={RANK_IMG[item.rank_of_parent]} title={RANK[item.rank_of_parent]}
                                    alt={RANK[item.rank_of_parent]} className="img-table"/>
                                  )}
                                </td>
                                <td className="text-center green-text boldy">{item.parent_ref_tosi_total}</td>


                                <td><small className="text-muted">
                                  {`${item.child_ref?.user_info?.first_name || ''} ${item.child_ref?.user_info?.last_name || ''}`}
                                </small></td>
                                <td><small className="text-muted">{item.child_ref.email}</small></td>
                                <td className="text-center">
                                  {!!item.rank_of_child && (
                                    <img src={RANK_IMG[item.rank_of_child]} title={RANK[item.rank_of_child]}
                                    alt={RANK[item.rank_of_child]} className="img-table" />
                                  )}
                                </td>

                                <td className="text-center green-text boldy">{item.tosi_invest}</td>
                                <td className="text-center green-text boldy">{item.tosi_bonus}</td>
                                <td className="text-center"><small className="text-muted">{moment(item.created_at).format(FROM_TIME)}</small></td>
                              </tr>
                            ))
                          }
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </section>
          </div>
          {
            totalReferralHistory > 10 &&
            <div className="col-lg-12">
              <Pagination
                current={page} onChange={onChangePage}
                total={totalReferralHistory} pageSize={limit}
              />
            </div>
          }
        </section>
      </div>
    </div>

  )
}

export default withLocalize(Referral)
