import React, { useEffect, useState } from 'react'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux'
import '@/pages/users/style.scss'
import HeaderForm from '@/components/header-form'
import DatePicker from '../../components/date-picker'
import Switch from '../../components/switch'
import Field from '@/components/field'
import { Formik, Form } from 'formik'
import { object, date } from 'yup'
import moment from 'moment'
import Notification from '@/components/notification'

const { getIcoSetting, updateIcoSetting } = actions;

const ICOSetting = () => {
  const [dataICO, setDataICO] = useState({})
  const dispatch = useDispatch();

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    dispatch(
      getIcoSetting(null, (action, data) => {
        if (action === TYPES.GET_ICO_SETTING_SUCCESS) {
          setDataICO(data || {});
        }
      })
    )
  }

  const _onSubmit = (values) => {
    const payload = {
      ...values,
      status: dataICO.status
    }
    dispatch(
      updateIcoSetting(payload, (action, data) => {
        if (action === TYPES.UPDATE_ICO_SETTING_SUCCESS) {
          getData()
          return Notification.success('csdsd')
        }
        if (action === TYPES.UPDATE_ICO_SETTING_FAILURE) {
          return Notification.error('Cannot Update maintenances')
        }
      })
    )
  }

  const _renderForm = ({ handleSubmit, setFieldValue, ...form }) => {
    return (
      <Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
        <div className='row'>

          <div className="col-lg-6">
            <div className="form-group">
              <label className="form-label">Start Time</label>
              <div className="controls">
                <Field
                  form={form}
                  name="start_time"
                  className="form-control"
                  format={'YYYY-MM-DD'}
                  style={{ border: 'none' }}
                  value={form.values.start_time}
                  onChange={(e) => setFieldValue(e.target.value)}
                  component={DatePicker}
                />
              </div>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="form-group">
              <label className="form-label">End Time</label>
              <div className="controls">
                <Field
                  form={form}
                  name="end_time"
                  format={'YYYY-MM-DD'}
                  className="form-control"
                  style={{ border: 'none' }}
                  value={form.values.end_time}
                  onChange={(e) => setFieldValue(e.target.value)}
                  component={DatePicker}
                />
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-xs-6">
            <div className="">
              <label className="form-label">Status</label>
              <div className="controls">
                <Switch
                  name="status"
                  checkedChildren="On" 
                  unCheckedChildren="Off"
                  onChange={(e) => setDataICO({ ...dataICO, status: e })}
                  component={Switch}
                  checked={ !!+dataICO.status || false }
                />
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-xs-6 right-col">
            <div>
              <label className="form-label"></label>
              <div className="controls">
                <button
                  className="btn btn-primary btn-corner right-15"
                  onClick={handleSubmit}>
                  Update
                </button>
              </div>
            </div>
          </div>
        </div>
      </Form>

    )
  };

  const initialValues = {
    start_time: dataICO?.start_time ? moment(dataICO.start_time) : '',
    end_time: dataICO?.end_time ? moment(dataICO.end_time) : ''
  }

  const validationSchema = object().shape({
    start_time: date().required(),
    end_time: date().required(),
  })

  return (
    <section className="box has-border-left-3">
      <HeaderForm
        nameForm={`ICO Settings`}
      />
      <div className="content-body">
        <Formik
          validateOnChange={false}
          validateOnBlur={false}
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={_onSubmit}
          component={_renderForm}
          enableReinitialize
        />
      </div>
    </section>
  )
}

export default ICOSetting

