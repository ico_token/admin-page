import React, { useEffect, useState } from 'react'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux'
import '@/pages/users/style.scss'
import HeaderForm from '@/components/header-form'
import DatePicker from '@/components/date-picker'
import Switch from '@/components/switch'
import Button from '@/components/button'
import Field from '@/components/field'
import { Formik, Form } from 'formik'
import { object, date } from 'yup'
import moment from 'moment'
import Notification from '@/components/notification'

const { getMaintenances, updateMaintenances } = actions;

const Maintenances = ({translate}) => {
  const [dataMaintenances, setDataMaintenances] = useState({})
  const [isLoading, setIsLoading] = useState(false)
  const dispatch = useDispatch();

  useEffect(() => {
    getData()
  }, []);

  const getData = () => {
    setIsLoading(true)
    dispatch(
      getMaintenances(null, (action, data) => {
        setIsLoading(false)
        if (action === TYPES.GET_MAINTENANCES_SUCCESS) {
          setDataMaintenances(data || {});
        }
      })
    )
  }

  const _onSubmit = (values) => {
    const payload = {
      ...values,
      is_maintained: dataMaintenances.is_maintained
    }
    dispatch(
      updateMaintenances(payload, (action, data) => {
        if (action === TYPES.UPDATE_MAINTENANCES_SUCCESS) {
          getData()
          return Notification.success(translate('success.update-maintenances'))
        }
      })
    )
  }

  const _renderForm = ({ handleSubmit, setFieldValue, ...form }) => {
    return (
      <Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
        <div className='row'>
          <div className="col-lg-6">
            <div className="form-group">
              <label className="form-label">{translate('utils.start-date')}</label>
              <div className="controls">
                <Field
                  form={form}
                  name="start_date"
                  className="form-control"
                  style={{ border: 'none' }}
                  value={form.values.start_date}
                  onChange={(e) => setFieldValue('start_date', e.target.value)}
                  component={DatePicker}
                />
              </div>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="form-group">
              <label className="form-label">{translate('utils.end-date')}</label>
              <div className="controls">
                <Field
                  form={form}
                  name="end_date"
                  className="form-control"
                  style={{ border: 'none' }}
                  value={form.values.end_date}
                  onChange={(e) => setFieldValue('end_date', e.target.value)}
                  component={DatePicker}
                />
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-xs-6">
            <div className="">
              <label className="form-label">{translate('system-setting.status')}</label>
              <div className="controls">
                <Switch
                  name="is_maintained"
                  checkedChildren={translate('utils.on')}
                  unCheckedChildren={translate('utils.off')}
                  onChange={(e) => setDataMaintenances({ ...dataMaintenances, is_maintained: e })}
                  component={Switch}
                  checked={ dataMaintenances.is_maintained || false }
                />
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-xs-6 right-col">
            <div>
              <label className="form-label"></label>
              <div className="controls">
                <Button
									className="btn btn-primary btn-corner right-15"
									onClick={handleSubmit}
									loading={isLoading}
									disabled={isLoading}
								>{translate('utils.update')}</Button>
              </div>
            </div>
          </div>
        </div>
      </Form>
    )
  };

  const initialValues = {
    start_date: dataMaintenances?.start_date ? moment(dataMaintenances.start_date) : '',
    end_date: dataMaintenances?.end_date ? moment(dataMaintenances.end_date) : ''
  }

  const validationSchema = object().shape({
    start_date: date().required(),
    end_date: date().required()
  })

  return (
    <section className="box has-border-left-3">
      <HeaderForm
        nameForm={translate('system-setting.maintenances')}
      />
      <div className="content-body">
        <Formik
          validateOnChange={false}
          validateOnBlur={false}
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={_onSubmit}
          component={_renderForm}
          enableReinitialize
        />
      </div>
    </section>
  )
}

export default Maintenances

