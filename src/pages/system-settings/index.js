import React from 'react'
import TitlePage from '@/components/title-page'
import '@/pages/users/style.scss'
// import ICOSetting from './ico-settings'
import Maintenances from './maintenances'
import { withLocalize } from 'react-localize-redux'

const SystemSetting = ({translate}) => {
  return (
    <div>
      <TitlePage
        namePage={translate('system-setting.system-setting')}
      />

      <div className="col-md-12">
        <Maintenances 
          translate={translate}
        />
      </div>

      {/* <div className="col-lg-12">
        <div className="row">
          <div className="col-md-6">
            <ICOSetting />
          </div>

          
        </div>
      </div> */}
    </div>
  )
}

export default withLocalize(SystemSetting)

