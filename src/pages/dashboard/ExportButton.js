import React from "react";
import Configs from "@/configs";

const endpoint = `${Configs.API_URL}`;
const ExportButton = () => {
  const handleExportExcel = async () => {
    try {
      const response = await fetch(endpoint+"/export/excel", {
        headers: {
          // Thêm authorization header nếu cần
          Authorization: `Bearer ${JSON.parse(localStorage.getItem("ACCESS_TOKEN"))}`,
        },
      });

      if (!response.ok) {
        throw new Error("Export failed");
      }

      // Lấy blob từ response
      const blob = await response.blob();

      // Tạo URL cho blob
      const url = window.URL.createObjectURL(blob);

      // Tạo thẻ a ẩn để tải file
      const link = document.createElement("a");
      link.href = url;
      link.download = "Thống kê tsi.xlsx"; // Tên file khi tải về
      document.body.appendChild(link);
      link.click();

      // Cleanup
      document.body.removeChild(link);
      window.URL.revokeObjectURL(url);
    } catch (error) {
      console.error("Export error:", error);
      // Xử lý lỗi tại đây
    }
  };

  return (
    <button
      onClick={handleExportExcel}
      style={{
        padding: "10px 20px",
        fontSize: "16px",
        backgroundColor: "#007bff",
        color: "#fff",
        border: "none",
        borderRadius: "5px",
        cursor: "pointer",
      }}
    >
      Xuất File
    </button>
  );
};

export default ExportButton;
