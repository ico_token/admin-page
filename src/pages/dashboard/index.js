import React, { useState, useEffect } from 'react'
import Slider from 'react-slick'
import './style.scss'
import { withLocalize } from 'react-localize-redux'
import { useDispatch } from 'react-redux'
import BigNumber from 'bignumber.js'
import TitlePage from '@/components/title-page'
import { TYPES, actions } from '@/store/actions'
import { COIN_SYSTEM, ICON_COIN, FROM_TIME } from '@/utils/constant'
import { getTotalAmount } from '../../api/users'
import { formatAmount } from '../../utils/common'
import ExportButton from './ExportButton'

const { getStatistical, getListCoin } = actions

const Dashboard = ({ translate }) => {
  const dispatch = useDispatch()

  const [dataDashBoard, setDataDashBoard] = useState({})
  const [listCoin, setListCoin] = useState([])
  let [totalAmount, setTotalAmount] = useState(null)

  useEffect(() => {
    const fetchData = async () => {
      dispatch(
        getStatistical({}, (action, data) => {
          if (action === TYPES.GET_STATISTICAL_SUCCESS) {
            setDataDashBoard(data || {})
          }
        })
      )
      dispatch(
        getListCoin({}, (action, data) => {
          if (action === TYPES.GET_LIST_COIN_SUCCESS) {
            setListCoin(data || [])
          }
        })
      )
      const total = await getTotalAmount()
      console.log(total)
      if (total?.result) {
        totalAmount = new BigNumber(total.result).dividedBy(1000000000000000000)
        setTotalAmount(totalAmount?.toNumber() || 0)
      }
    }
    fetchData()
  }, [])

  console.log(listCoin)

  const settings = {
    className: 'crypto-balance',
    arrows: true,
    rows: 1,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    centerPadding: '60px',
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  }

  const getCodeCoin = (id) => listCoin.find((item) => item.id === id)?.code
  const getNameCoin = (id) => listCoin.find((item) => item.id === id)?.name

  return (
    <div className="dashboard">
      <div className="col-xs-12">
        <TitlePage namePage={translate('dashboard.dashboard')} />
      </div>

      <div>
      <div class="col-lg-12 mt-15 mb-15">
      <ExportButton></ExportButton>

      </div>

        <div className="col-lg-12 mt-15 mb-15">
          <div className="col-xs-12">
            <div className="pull-left">
              <h4 className="title boldy mb-5 mt-15">{translate('dashboard.total_tsi')}</h4>
            </div>
          </div>
          <div className="clearfix" />
          <section className="wra">
            <div className="swiper-container coins-slider text-center">
              <div className="swiper-wrapper">
                <div className="coin-box flex align-items-center">
                  <div className="coin-icon mr-10">
                    <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system" />
                  </div>
                  <div className="coin-balance text-left">
                    <h5 className="coin-name boldy">{translate('dashboard.total_tsi')}</h5>
                    <p className="mb-0 green-text">{formatAmount(totalAmount)} TSI</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="apg-arrows">
              <div className="swiper-button-prev" />
              <div className="swiper-button-next" />
            </div>
          </section>
        </div>
      </div>

      <div>
      </div>
      {
        Object.keys(dataDashBoard).map((item, index) => (
          <div key={index}>
            <div className="col-xs-12">
              <div className="pull-left">
                <h4 className="title boldy mb-5 mt-15">{translate(`dashboard.${item}`) || ''}</h4>
              </div>
            </div>

            <div className="col-lg-12 mt-15 mb-15">
              <div className="clearfix" />
              <section className="wra">
                <div className="swiper-container coins-slider text-center">
                  <div className="swiper-wrapper">
                    <Slider {...settings}>
                      {
                        dataDashBoard?.[item]?.map((elm, i) => (
                          <div className="swiper-slide" key={i}>
                            <div className="coin-box flex align-items-center">
                              <div className="coin-icon mr-10">
                                {
                                elm?.coin_id
                                  ? (getCodeCoin(elm?.coin_id) === COIN_SYSTEM.CODE
                                    ? <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system" />
                                    : <i className={ICON_COIN[getCodeCoin(elm?.coin_id)]?.COLOR} />)
                                  : <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system" />
                              }
                              </div>
                              <div className="coin-balance text-left">
                                <h5 className="coin-name boldy">{getNameCoin(elm?.coin_id) || 'Tosi'}</h5>
                                <p className="mb-0 green-text">{elm.total_amount}</p>
                              </div>
                            </div>
                          </div>
                        ))
                      }
                    </Slider>
                  </div>

                </div>
                <div className="apg-arrows">
                  <div className="swiper-button-prev" />
                  <div className="swiper-button-next" />
                </div>
              </section>
            </div>

          </div>

        ))
      }
    </div>
  )
}

export default withLocalize(Dashboard)
