import React, { useEffect, useState } from 'react';
import { withLocalize } from 'react-localize-redux'
import Input from '@/components/input'
import Button from '@/components/button'
import Pagination from '@/components/pagination'
import Select from '@/components/select'
import TitlePage from '@/components/title-page'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
import { RANK, RANK_IMG } from '@/utils/constant'
import { Collapse } from 'antd';
import ModalUpdateRank from './modal-update-rank'
import 'antd/es/collapse/style/css'
import '../users/style.scss'
const { Panel } = Collapse;

const { getListUsers } = actions;
const SupplyRank = ({ translate }) => {
  const mockSearch = {
    page: 1,
    limit: 10
  }

  const OPTION_SELECT = [
    { name: translate('transactions.all'), value: 'root' },
    { name: translate('utils.yes'), value: true },
    { name: translate('utils.no'), value: false }
  ]

  const dispatch = useDispatch();
  const [listUser, setListUser] = useState([]);
  const [totalUser, setTotalUser] = useState(0);
  const [page, setPage] = useState(mockSearch.page);
  const [limit] = useState(mockSearch.limit);
  const [isLoading, setIsLoading] = useState(false)
  const [valueSearch, setValueSearch] = useState(mockSearch);
  const [openModalUpdateRank, setOpenModalUpdateRank] = useState(false)
  const [userUpdate, setUserUpdate] = useState({})

  useEffect(() => {
    getList(valueSearch)
  }, []);

  const getList = (value) => {
    setIsLoading(true)
    dispatch(
      getListUsers(value, (action, data) => {
        setIsLoading(false)
        if (action === TYPES.GET_LIST_USERS_SUCCESS) {
          setListUser(data.rows);
          setTotalUser(data.count);
        }
      })
    )
  };

  const onChangePage = (p) => {
    setPage(p);
    setValueSearch({ ...valueSearch, page: p })
    getList({ ...valueSearch, page: p })
  };

  const changeSearch = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if(name === 'is_root' ) {
      if(value === 'root') {
        const newData = { ...valueSearch };
        delete newData.is_root
        setValueSearch(newData)
      } else {
        setValueSearch({
          ...valueSearch,
          [name]: value
        })
      }
      
    } else {
      setValueSearch({
        ...valueSearch,
        [name]: value.trim()
      })
    }
  };

  const handleSearch = () => {
    getList(valueSearch)
  };

  const handleReset = () => {
    setValueSearch(mockSearch)
    getList(mockSearch)
  }

  const handleOpenModal = ({ item }) => {
    setUserUpdate(item)
    setOpenModalUpdateRank(true)
  }

  return (
    <div>
      <TitlePage namePage={translate('users.supply-rank')} />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12 mt-15">
            <Collapse >
              <Panel header={translate('utils.filter')}>
                <div>
                  <div className="field col-sm-3"	>
                    <label>{translate('utils.email')}</label>
                    <Input name="email" onChange={changeSearch} value={valueSearch.email} />
                  </div>

                  <div className="field col-sm-3">
                    <label>{translate('utils.phone')}</label>
                    <Input name="phone_number" onChange={changeSearch} type="number" value={valueSearch.phone_number} />
                  </div>

                  <div className="field col-sm-3">
                    <label>{translate('utils.address')}</label>
                    <Input name="tosi_address" onChange={changeSearch} value={valueSearch.tosi_address} />
                  </div>

                  <div className="field col-sm-3">
                    <label>Root</label>
                    <Select
                      name="is_root" onChange={changeSearch}
                      value={typeof valueSearch.is_root === "boolean" ? valueSearch.is_root : translate('transactions.all')}
                      options={OPTION_SELECT}
                    />
                  </div>
                </div>

                <div>

                  <Button
                    margin={true}
                    onClick={handleSearch}
                    loading={isLoading}
                    disabled={isLoading}
                  >{translate('utils.search')}</Button>

                  <Button
                    onClick={handleReset}
                    loading={isLoading}
                    disabled={isLoading}
                  >{translate('utils.reset')}</Button>

                </div>
              </Panel>
            </Collapse>
          </div>

          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header">
                <h2 className="title pull-left">{translate('users.list-users')}</h2>
              </header>
              <div className="content-body">

                <div className="row">
                  <div className="col-xs-12">
                    <div className="table-responsive" data-pattern="priority-columns">
                      <table id="tech-companies-1" className="table vm trans table-small-font no-mb table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>{translate('utils.email')}</th>
                            <th>{translate('utils.address')}</th>
                            <th>{translate('utils.phone')}</th>
                            <th className="text-center">{translate('utils.rank')}</th>
                            <th className="text-center">{translate('utils.rank-admin')}</th>
                            <th ></th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            listUser.map((item, i) => (
                              <tr key={i}>
                                <td><small className="text-muted">{item.email}</small></td>
                                <td><small className="text-muted">{item.tosi_address}</small></td>
                                <td><small className="text-muted">{item.phone_number}</small></td>
                                <td className="text-center">
                                  {!!item.ref_info?.rank && (
                                    <img src={RANK_IMG[item.ref_info?.rank]} title={RANK[item.ref_info?.rank]}
                                      alt={RANK[item.ref_info?.rank]} className="img-table" />
                                  )}
                                </td>
                                <td className="text-center">
                                  {!!item.ref_info?.admin_rank && (
                                    <img src={RANK_IMG[item.ref_info?.admin_rank]} title={RANK[item.ref_info?.admin_rank]}
                                      alt={RANK[item.ref_info?.admin_rank]} className="img-table" />
                                  )}
                                </td>

                                <td className="green-text boldy text-center">
                                  <div className="btn-group btn-group-justified btn-detail">
                                    <a className="btn btn-warning btn-xs"
                                      onClick={() => handleOpenModal({ item })}
                                    >{translate('utils.update')}</a>
                                  </div>
                                </td>
                              </tr>
                            ))
                          }
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </section>
          </div>
          {
            totalUser > 10 &&
            <div className="col-lg-12">
              <Pagination
                current={page} onChange={onChangePage}
                total={totalUser} pageSize={limit}
              />
            </div>
          }
        </section>
      </div>
      {
        openModalUpdateRank && <ModalUpdateRank
          close={() => setOpenModalUpdateRank(false)}
          userUpdate={userUpdate}
          translate={translate}
          isOpen={openModalUpdateRank}
          reload={() => getList(valueSearch)}
        />
      }

    </div>

  )
}

export default withLocalize(SupplyRank)
