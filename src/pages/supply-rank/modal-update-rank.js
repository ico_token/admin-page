import React, { useState } from 'react';
import { useDispatch } from 'react-redux'
import Modal from '@/components/modal'
import SelectRank from '@/components/select-rank'
import Field from '@/components/field'
import Notification from '@/components/notification'
import Button from '@/components/button'
import { TYPES, actions } from '@/store/actions'
import { Formik, Form } from 'formik'
import { withLocalize } from 'react-localize-redux'
import { object, number } from 'yup'
import _ from 'lodash'
// import './style.scss'
import { RANK } from '@/utils/constant'


const { updateRank } = actions;
const ModalUpdateRank = ({ close, isOpen, reload, userUpdate, translate }) => {
	const dispatch = useDispatch();
	const [isLoading, setIsLoading] = useState(false)
	const { id, ref_info } = userUpdate;
	const _onSubmit = (values) => {
		const { rank } = values
		setIsLoading(true)
		dispatch(
			updateRank({ id, rank }, (action) => {
				setIsLoading(false)
				if (action === TYPES.UPDATE_RANK_SUCCESS) {
					reload()
					close()
					return Notification.success(translate('success.UPDATE_RANK_SUCCESS'))
				}
			})
		)
	}

	const _renderForm = ({ handleSubmit, ...form }) => {
		return (
			<div>
				<Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
					<div className="row">
						<div className="col-xs-12">

							<div className="form-login">
								<label className="form-label">{translate('utils.rank')}</label>
								<div className="controls">
									<Field
										form={form}
										name="rank"
										options={RANK}
										component={SelectRank}
									/>
								</div>
							</div>

							<div className="text-center">
								<Button
									className="btn btn-primary btn-corner"
									onClick={handleSubmit}
									loading={isLoading}
									disabled={isLoading}
								>{translate('utils.update')}</Button>
							</div>
						</div>
					</div>
				</Form>
			</div>
		)
	}
	const initialValues = {
		rank: !_.isEmpty(userUpdate) ? ref_info?.rank : ''
	}
	const validationSchema = object().shape({
		rank: number().required().positive('required')
	})
	return (
		<Modal
			visible={isOpen}
			onCancel={close}
			title={translate('users.update-rank')}
			destroyOnClose={true}
		>

			<Formik
				validateOnChange={false}
				validateOnBlur={false}
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={_onSubmit}
				component={_renderForm}
				enableReinitialize
			/>

		</Modal>

	)
}

export default withLocalize(ModalUpdateRank)