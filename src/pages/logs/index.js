import React, { useEffect, useState } from 'react';
import Input from '@/components/input'
import Button from '@/components/button'
import Pagination from '@/components/pagination'
import TitlePage from '@/components/title-page'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { FROM_TIME } from '@/utils/constant'
import ModalValue from '@/components/modal-value'
import { Collapse } from 'antd';
import 'antd/es/collapse/style/css'
import { withLocalize } from 'react-localize-redux'
const { Panel } = Collapse;

const { getAllLogs } = actions;
const Logs = ({ translate }) => {
  const mockSearch = {
    page: 1,
    limit: 10
  }

  const dispatch = useDispatch();
  const [listLogs, setListLogs] = useState([]);
  const [totalLogs, setTotalLogs] = useState(0);
  const [page, setPage] = useState(mockSearch.page);
  const [limit] = useState(mockSearch.limit);
  const [valueSearch, setValueSearch] = useState(mockSearch);
  const [isOpen, setIsOpen] = useState(false);
  const [valueShow, setValueShow] = useState('');
  const [isLoading, setIsLoading] = useState(false)

  const _open = ({ value }) => {
    setIsOpen(true)
    setValueShow(value)
  }

  const _close = () => {
    setIsOpen(false)
  }

  useEffect(() => {
    getList(valueSearch)
  }, []);

  const getList = (value) => {
    setIsLoading(true)
    dispatch(
      getAllLogs(value, (action, data) => {
        setIsLoading(false)
        if (action === TYPES.GET_ALL_LOGS_SUCCESS) {
          setListLogs(data.rows);
          setTotalLogs(data.count);
        }
      })
    )
  };

  const onChangePage = (p) => {
    setPage(p);
    setValueSearch({...valueSearch, page: p})
    getList({ ...valueSearch, page: p })
  };

  const changeSearch = (e) => {
    const id = e.target.id;
    const value = e.target.value;
    setValueSearch({
      ...valueSearch,
      [id]: value.trim()
    })
  };

  const handleSearch = () => {
    getList(valueSearch)
  };

  const handleReset = () => {
    setValueSearch(mockSearch)
    getList(mockSearch)
  }

  return (
    <div>
      <TitlePage
        namePage={translate('logs.logs')}
      />

      <div className="col-lg-12">
        <section className="box ">

          <div className="col-lg-12 mt-15">
            <Collapse >
              <Panel header={translate('utils.filter')}>
                <div>

                  <div className="field col-lg-6"	>
                    <label htmlFor="field1">{translate('utils.filter')}</label>
                    <Input id="name" onChange={changeSearch} value={valueSearch.name}/>
                  </div>
                  <div className="field col-lg-6">
                    <label htmlFor="field1">{translate('utils.value')}</label>
                    <Input id="value" onChange={changeSearch} value={valueSearch.value}/>
                  </div>

                  <div>
                  <Button
                    margin={true}
                    onClick={handleSearch}
                    loading={isLoading}
                    disabled={isLoading}
                  >{translate('utils.search')}</Button>

<Button
                    onClick={handleReset}
                    loading={isLoading}
                    disabled={isLoading}
                  >{translate('utils.reset')}</Button>

                  </div>

                  
                </div>
              </Panel>
            </Collapse>
          </div>

          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header">
                <h2 className="title pull-left">{translate('logs.list-log')}</h2>
              </header>
              <div className="content-body">

                <div className="row">
                  <div className="col-xs-12">
                    <div className="table-responsive" data-pattern="priority-columns">
                      <table id="tech-companies-1" className="table vm trans table-small-font no-mb table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>{translate('utils.name')}</th>
                            <th>{translate('utils.value')}</th>
                            <th>{translate('utils.create-at')}</th>
                            <th>{translate('utils.update-at')}</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            listLogs.map((item, i) => (
                              <tr key={i}>
                                <td><small className="text-muted">{item.name}</small></td>
                                <td
                                  onClick={() => _open({ value: item.value })}
                                ><small className="text-muted text-long">{item.value}</small></td>
                                <td><small className="text-muted">{moment(item.created_at).format(FROM_TIME)}</small></td>
                                <td><small className="text-muted">{moment(item.updated_at).format(FROM_TIME)}</small></td>
                              </tr>
                            ))
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
          {
            totalLogs > 10 &&
            <div className="col-lg-12">
              <Pagination
                current={page} onChange={onChangePage}
                total={totalLogs} pageSize={limit}
              />
            </div>
          }
        </section>
      </div>
      <ModalValue
        close={_close}
        isOpen={isOpen}
        value={valueShow}
      />
    </div>

  )
}

export default withLocalize(Logs)
