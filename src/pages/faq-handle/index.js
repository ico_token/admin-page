import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Formik, Form } from 'formik';
import { object, string } from 'yup';

import { TYPES, actions } from '@/store/actions'
import {FAQ_CATEGORY_VALUE} from '@/constants'
import { trimValues } from '@/utils/trim'
import { withLocalize } from 'react-localize-redux'

// Components
import Field from '@/components/field-custom'
import Input from '@/components/input'
import Select from '@/components/select'
import Button from '@/components/button'
import Notification from '@/components/notification'
import TitlePage from '@/components/title-page'


import './style.scss';

const { createFaq, updateFaq, getFaqById } = actions;

const FAQCreate = ({ history, match, translate }) => {
  const dispatch = useDispatch();
  const { submitting, faqItem } = useSelector(state => state.faq)

  const [id, setId] = useState(null);

  const FAQ_CATEGORY_OPTION = [
    { name: translate('faq.GENERAL'), value: FAQ_CATEGORY_VALUE.GENERAL },
    { name: translate('faq.PAYMENT'), value: FAQ_CATEGORY_VALUE.PAYMENT },
    { name: translate('faq.WITHDRAW'), value: FAQ_CATEGORY_VALUE.WITHDRAW },
    { name: translate('faq.OTHER'), value: FAQ_CATEGORY_VALUE.OTHER }
  ]

  useEffect(() => {
    const { id } = match.params;
    if (id) {
      setId(id)
      dispatch(
        getFaqById({ id: +id })
      )
    }
  }, []);

  const _onSubmit = (values) => {
    const formData = trimValues(values)
    if (!!id) {
      // Update FAQ
      dispatch(
        updateFaq({
          ...formData,
          category: +formData.category,
          id: +id
        }, (action, data, error) => {
          if (action === TYPES.UPDATE_FAQ_SUCCESS) {
            history.push('/faq')
            return Notification.success(translate('success.UPDATE_FAQ'))
          }
        })
      )
    } else {
      // Create FAQ
      dispatch(
        createFaq({
          ...formData,
          category: +formData.category
        }, (action, data, error) => {
          if (action === TYPES.CREATE_FAQ_SUCCESS) {
            history.push('/faq')
            return Notification.success(translate('success.CREATE_FAQ'))
          }
        })
      )
    }
  }

  // Render form
  const _renderForm = ({ handleSubmit, ...form }) => {
    return (
      <Form>
        <Field
          form={form}
          name="category"
          label={translate('faq.category')}
          component={Select}
          options={FAQ_CATEGORY_OPTION}
          isRequired
        />
        <Field
          form={form}
          name="question"
          label={translate('faq.question')}
          component={Input}
          isRequired
        />
        <Field
          form={form}
          name="answer"
          label={translate('faq.answer')}
          component={Input}
          isRequired
        />
        <div className="form-box-btn">
          <Button
            type="primary"
            loading={
              !!submitting === TYPES.CREATE_FAQ_REQUEST
              || !!submitting === TYPES.UPDATE_FAQ_REQUEST
            }
            disabled={
              !!submitting
              || !form.isValid
            }
            onClick={handleSubmit}
          >
            {!!id ? translate('utils.update') : translate('utils.create')}
          </Button>
        </div>
      </Form>
    )
  }

  // Init form
  const initialValues = !!id ? {
    category: String(faqItem?.category),
    question: faqItem?.question,
    answer: faqItem?.answer
  } : {}

  // Valid form
  const validationSchema = object().shape({
    category: string().required().trim(),
    question: string().required().trim(),
    answer: string().required().trim()
  })

  return (
    <div className="faq-handle-page">
      <TitlePage namePage={!!id ? translate('faq.update-faq') : translate('faq.create-faq')} />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header faq_header">
                <h2 className="title pull-left">{!!id ? translate('faq.update-faq') : translate('faq.create-faq')}</h2>
              </header>
              <div className="content-body">
                <Formik
                  validateOnChange={false}
                  validateOnBlur={false}
                  initialValues={initialValues}
                  validationSchema={validationSchema}
                  onSubmit={_onSubmit}
                  component={_renderForm}
                  enableReinitialize
                />
              </div>
            </section>
          </div>
        </section>
      </div>
    </div>
  )
}

export default withLocalize(FAQCreate)
