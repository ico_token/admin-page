import React, { useEffect, useState } from 'react';
import { withLocalize } from 'react-localize-redux'
import Input from '@/components/input'
import Button from '@/components/button'
import Pagination from '@/components/pagination'
import TitlePage from '@/components/title-page'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { FROM_TIME } from '@/utils/constant'
import Notification from '@/components/notification'
import ModalValue from '@/components/modal-value'
import { Collapse } from 'antd';
import 'antd/es/collapse/style/css'
const { Panel } = Collapse;

const { getListContacts, updateContacts } = actions;
const Contacts = ({ translate }) => {
  const mockSearch = {
    page: 1,
    limit: 10
  }

  const dispatch = useDispatch();
  const [listContacts, setListContacts] = useState([]);
  const [totalContacts, setTotalContacts] = useState(0);
  const [page, setPage] = useState(mockSearch.page);
  const [limit] = useState(mockSearch.limit);
  const [valueSearch, setValueSearch] = useState(mockSearch);
  const [isOpen, setIsOpen] = useState(false);
  const [valueShow, setValueShow] = useState('');
  const [isLoading, setIsLoading] = useState(false)

  const _open = ({ value }) => {
    setIsOpen(true)
    setValueShow(value)
  }

  const _close = () => {
    setIsOpen(false)
  }

  useEffect(() => {
    getList(valueSearch)
  }, []);

  const getList = (value) => {
    setIsLoading(true)
    dispatch(
      getListContacts(value, (action, data) => {
        setIsLoading(false)
        if (action === TYPES.GET_LIST_CONTACTS_SUCCESS) {
          setListContacts(data.rows);
          setTotalContacts(data.count);
        }
      })
    )
  };

  const onChangePage = (p) => {
    setPage(p);
    setValueSearch({...valueSearch, page: p})
    getList({ ...valueSearch, page: p })
  };

  const changeSearch = (e) => {
    const id = e.target.id;
    const value = e.target.value;
    setValueSearch({
      ...valueSearch,
      [id]: value.trim()
    })
  };

  const handleSearch = () => {
    getList(valueSearch)
  };

  const handleReset = () => {
    setValueSearch(mockSearch)
    getList(mockSearch)
  }

  const handleUpdate = ({ id }) => {
    dispatch(
      updateContacts({ id }, (action, data) => {
        if (action === TYPES.UPDATE_CONTACTS_SUCCESS) {
          getList()
          return Notification.success(translate('success.UPDATE_CONTACTS'))
        }
      })
    )
  };

  return (
    <div>
      <TitlePage
        namePage={translate('contacts.contacts')}
      />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12 mt-15">
            <Collapse >
              <Panel header={translate('utils.filter')}>
                <div className="field col-lg-6">
                  <label>{translate('utils.name')}</label>
                  <Input id="name" onChange={changeSearch} value={valueSearch.name}/>
                </div>

                <div className="field col-lg-6"	>
                  <label>{translate('utils.email')}</label>
                  <Input id="email" onChange={changeSearch} value={valueSearch.email}/>
                </div>

                <div>
                <Button
                  margin={true}
                  onClick={handleSearch}
                  loading={isLoading}
                  disabled={isLoading}
                >{translate('utils.search')}</Button>

<Button

                  onClick={handleReset}
                  loading={isLoading}
                  disabled={isLoading}
                >{translate('utils.reset')}</Button>
                </div>

                
              </Panel>
            </Collapse>
          </div>

          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header">
                <h2 className="title pull-left">{translate('contacts.list-contacts')}</h2>
              </header>
              <div className="content-body">

                <div className="row">
                  <div className="col-xs-12">
                    <div className="table-responsive" data-pattern="priority-columns">
                      <table id="tech-companies-1" className="table vm trans table-small-font no-mb table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>{translate('utils.name')}</th>
                            <th>{translate('utils.email')}</th>
                            <th>{translate('utils.content')}</th>
                            <th>{translate('utils.replied')}</th>
                            <th>{translate('utils.create-at')}</th>
                            <th>{translate('utils.update-at')}</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            listContacts.map((item, i) => (
                              <tr key={i}>
                                <td><small className="text-muted">{item.name}</small></td>
                                <td><small className="text-muted">{item.email}</small></td>
                                <td><small className="text-muted text-long"
                                  onClick={() => _open({ value: item.content })}
                                >{item.content}</small></td>
                                <td className="text-center">
                                  {item.is_replied ?
                                    <i className="fa fa-check-circle verification"></i>
                                    : <i className="fa fa-times-circle not-verification"></i>}
                                </td>
                                <td><small className="text-muted">{moment(item.created_at).format(FROM_TIME)}</small></td>
                                <td><small className="text-muted">{moment(item.updated_at).format(FROM_TIME)}</small></td>
                                <td>
                                  <div className="btn-group btn-group-justified btn-detail">
                                    <a className="btn btn-warning btn-xs"
                                      onClick={() => handleUpdate({ id: item.id })}
                                    >{translate('utils.update')}</a>
                                  </div>
                                </td>

                              </tr>
                            ))
                          }
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </section>
            <ModalValue
              close={_close}
              isOpen={isOpen}
              value={valueShow}
            />
          </div>
          {
            totalContacts > 10 &&
            <div className="col-lg-12">
              <Pagination
                current={page} onChange={onChangePage}
                total={totalContacts} pageSize={limit}
              />
            </div>
          }
        </section>
      </div>
    </div>

  )
}

export default withLocalize(Contacts)
