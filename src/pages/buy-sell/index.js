import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { actions, TYPES } from '../../store/actions'
import ActivitiesHistory from './activities-history'
import RecentActivity from './recent-activity'
import CryptoPayment from './crypto-payment'
import Ask from './ask'
import './style.scss'

const { getListCoin } = actions

function BuySell() {
  const dispatch = useDispatch()
  const [listCoins, setListCoins] = useState([])

  useEffect(() => {
    dispatch(getListCoin({}, (action, data) => {
      if (action === TYPES.GET_LIST_COIN_SUCCESS) {
        setListCoins(data)
      }
    }))
  }, [])

  return (
    <div className="page-buy-sell">
      <div className="col-xs-12">
        <div className="page-title">
          <div className="pull-left">
            <h1 className="title">Crypto Buy & Sell</h1>
          </div>
          <div className="pull-right hidden-xs">
            <ol className="breadcrumb">
              <li>
                <a href="#"><i className="fa fa-home" />Home</a>
              </li>
              <li className="active">
                <strong>Crypto Buy & Sell</strong>
              </li>
            </ol>
          </div>

        </div>
      </div>
      <div className="clearfix" />
      <Ask />
      <div className="clearfix" />
      <CryptoPayment listCoins={listCoins} />
      <div className="clearfix" />
      <RecentActivity />
      <div className="clearfix" />
      <ActivitiesHistory />
    </div>
  )
}

export default BuySell
