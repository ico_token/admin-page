import React, { useEffect, useState } from 'react'
import classNames from 'classnames'
import { COIN_SYSTEM, ICON_COIN } from '../../utils/constant'

function CryptoPayment({ listCoins }) {
  const [selectedCoin, setSelectedCoin] = useState()

  useEffect(() => {
    if (listCoins?.length) {
      setSelectedCoin(listCoins[0])
    }
  }, [listCoins])
  return (
    <div>
      <div className="crypto-payment">
        <div className="col-lg-4 ">
          <div className="row">
            <div className="crypto-list col-lg-6 col-sm-6 col-xs-12 mt-15">
              {
                listCoins?.length > 0 && listCoins.map((e) => (
                  <div
                    className={classNames('crpto-currency-box', { active: e.id === selectedCoin?.id })}
                    key={e.id}
                    onClick={() => setSelectedCoin(e)}
                  >
                    {
                      e.code === COIN_SYSTEM.CODE
                        ? <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system" />
                        : <i className={ICON_COIN[e.code]?.COLOR} />
                    }
                    <h3 className="boldy">{e.name}</h3>
                    <span className="checky-box" />
                  </div>
                ))
              }
            </div>

            <div className="payment-list col-lg-6 col-sm-6 col-xs-12 mt-15">
              <div className="crpto-currency-box active">
                <img src="/data/crypto-dash/payment1.png" alt="" />
                <h3 className="boldy">Visa Card</h3>
                <span className="checky-box" />
              </div>
              <div className="crpto-currency-box">
                <img src="/data/crypto-dash/payment2.png" alt="" />
                <h3 className="boldy">Mastercard</h3>
                <span className="checky-box" />
              </div>
              <div className="crpto-currency-box">
                <img src="/data/crypto-dash/payment3.png" alt="" />
                <h3 className="boldy">Paypal</h3>
                <span className="checky-box" />
              </div>
              <div className="crpto-currency-box">
                <img src="/data/crypto-dash/payment4.png" alt="" />
                <h3 className="boldy">Amazon</h3>
                <span className="checky-box" />
              </div>
              <div className="crpto-currency-box">
                <img src="/data/crypto-dash/payment2.png" alt="" />
                <h3 className="boldy">Prepaid</h3>
                <span className="checky-box" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-8">
        <section className="box has-border-left-3 no-shadow">
          <header className="panel_header gradient-blue">
            <h2 className="title pull-left w-text">Payment Method</h2>
          </header>
          <div className="content-body mt-15 pb0">
            <div className="row">
              <div className="payment-info-wrap">
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="form-label">Cardholder Name</label>
                    <div className="controls">
                      <input type="text" className="form-control" placeholder="Name" id="field-1" />
                    </div>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="form-label">Card Number</label>
                    <div className="controls">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="0000-0000-0000-0000"
                        id="field-1"
                      />
                    </div>
                  </div>
                </div>
                <div className="clearfix" />
                <div className="col-lg-4">
                  <div className="form-group">
                    <label className="form-label">Expire Month</label>
                    <select className="form-control m-bot15">
                      <option>January</option>
                      <option>Option 2</option>
                      <option>Option 3</option>
                    </select>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="form-group">
                    <label className="form-label">Expire Year</label>
                    <select className="form-control m-bot15">
                      <option>2024</option>
                      <option>Option 2</option>
                      <option>Option 3</option>
                    </select>
                  </div>
                </div>
                <div className="col-lg-2">
                  <div className="form-group">
                    <label className="form-label">CVV</label>
                    <div className="controls mt-0">
                      <input type="text" className="form-control" placeholder="000" id="field-1" />
                    </div>
                  </div>
                </div>
                <div className="col-lg-2">
                  <div className="form-group">
                    <label className="form-label">.</label>
                    <div className="controls mt-5">
                      <button className="btn btn-primary">Send</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <header className="panel_header gradient-blue">
            <h2 className="title pull-left w-text">Trade Cryptocurrency</h2>
          </header>
          <div className="content-body mt-15">
            <div className="row">
              <div className="crypto-info-wrap">
                <div className="col-xs-12">
                  <div className="form-group">
                    <label className="form-label">wallet address</label>
                    <span className="desc" />

                    <div className="input-group mb-10">
                      <span className="input-group-addon">
                        <span className="arrow" />
                        <img src="/data/crypto-dash/icons/2.png" alt="icon" />
                      </span>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="NNK;Djkhsal0q0ksnq-wnkknkwxbkja"
                      />
                    </div>
                  </div>
                </div>
                <div className="col-lg-5">
                  <div className="form-group">
                    <label className="form-label">USD Currency</label>
                    <span className="desc">minimum 100 USD</span>
                    <div className="controls">
                      <input type="text" className="form-control" placeholder="100" id="field-1" />
                    </div>
                  </div>
                </div>
                <div className="col-lg-2">
                  <div className="form-group">
                    <label className="form-label">.</label>
                    <div className="controls">
                      <img
                        src="/data/crypto-dash/exchange-arrows.png"
                        className="mt-15 mb-15 center-block"
                        style={{ width: '25px' }}
                        alt=""
                      />
                    </div>
                  </div>
                </div>
                <div className="col-lg-5">
                  <div className="form-group">
                    <label className="form-label">BTC Equivalent</label>
                    <span className="desc">Exchanged BTC</span>
                    <div className="controls">
                      <input type="text" className="form-control" placeholder="0.001" id="field-1" />
                    </div>
                  </div>
                </div>
                <div className="col-lg-8 col-lg-offset-2 col-md-12">
                  <button
                    type="button"
                    className="btn btn-primary btn-lg mt-20 has-gradient-to-right-bottom"
                    style={{ width: '100%' }}
                  >Trade Bitcoin instantly - $100
                  </button>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

export default CryptoPayment
