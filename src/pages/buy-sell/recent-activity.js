import React from 'react'

function RecentActivity() {
  return (
    <div>
      <div className="col-lg-12">
        <h3 className="boldy mt-15">Recent activity Summary</h3>
      </div>
      <div className="col-lg-4 col-sm-6 col-xs-12">
        <div className="ref-num-box statistics-box mt-15 text-center">
          <h5 className="boldy mt-30 mb-0">Your are Buying</h5>
          <h2 className="bold p-text mt-15 mb-15">0.012032 BTC</h2>
          <p className="mb-0 text-muted boldy mb-30">$7,374 per BTC</p>
        </div>
      </div>
      <div className="col-lg-4 col-sm-6 col-xs-12">
        <div className="statistics-box mt-15 text-center">
          <h5 className="boldy mt-30 mb-0">Payment Method</h5>
          <img src="/data/crypto-dash/payment1.png" className="mt-15 mb-15" style={{ maxWidth: '55px' }} alt="" />
          <p className="mb-0 text-muted boldy mb-30">Desposite to BTC Wallet</p>
        </div>
      </div>
      <div className="col-lg-4 col-sm-6 col-xs-12">
        <div className="statistics-box mt-15">
          <div className="t-summary flex align-items-center">
            <h4 className="boldy">Transaction Fee:</h4>
            <p className="mb-0 boldy">$1.93</p>
          </div>
          <div className="t-summary flex align-items-center">
            <h4 className="boldy">Transaction Subtotal:</h4>
            <p className="mb-0 boldy">$98.07</p>
          </div>
          <div className="t-summary flex align-items-center mb-0">
            <h4 className="boldy">Transaction Total:</h4>
            <p className="mb-0 bold text-primary">$100.00</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RecentActivity
