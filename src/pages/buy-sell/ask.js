import React from 'react'

function Ask() {
  return (
    <div>
      <div className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div className="ask-box v2 active mt-15">
          <div className="ask-circle">
            <img src="/data/crypto-dash/crypto-buy.png" alt="" />
          </div>
          <div className="ask-info">
            <h3 className="w-text boldy mt-15">Buy Crypto</h3>
            <p className="g2-text mb-0">lorem dolor sit elit.</p>
          </div>
          <div className="ask-arrow">
            <a href=""><span className="fa fa-angle-right" /></a>
          </div>
        </div>
      </div>
      <div className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div className="ask-box v2 active mt-15">
          <div className="ask-circle">
            <img src="/data/crypto-dash/crypto-sell.png" alt="" />
          </div>
          <div className="ask-info">
            <h3 className="w-text boldy mt-15">Sell Crypto</h3>
            <p className="g2-text mb-0">lorem dolor sit elit.</p>
          </div>
          <div className="ask-arrow">
            <a href=""><span className="fa fa-angle-right" /></a>
          </div>
        </div>
      </div>
      <div className="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div className="ask-box v2 active mt-15">
          <div className="ask-circle">
            <img src="/data/crypto-dash/crypto-wallet.png" alt="" />
          </div>
          <div className="ask-info">
            <h3 className="w-text boldy mt-15">Exchange</h3>
            <p className="g2-text mb-0">lorem dolor sit elit.</p>
          </div>
          <div className="ask-arrow">
            <a href=""><span className="fa fa-angle-right" /></a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Ask
