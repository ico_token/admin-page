import React from 'react'
import Information from './information'
import SocialMediaInfo from './social-media-info'
import Profile from './profile'

function Personal() {
  return (
    <div>
      <div className="col-xs-12">
        <div className="page-title">
          <div className="pull-left">
            <h1 className="title">Personal Settings</h1>
          </div>
          <div className="pull-right hidden-xs">
            <ol className="breadcrumb">
              <li>
                <a href="#"><i className="fa fa-home" />Home</a>
              </li>
              <li className="active">
                <strong>Personal Settings</strong>
              </li>
            </ol>
          </div>
        </div>
      </div>
      <div className="clearfix" />

      <Profile />

      <div className="col-lg-8">
        <div className="row">

          <Information />

          <div className="clearfix" />

          <SocialMediaInfo />
        </div>
      </div>
    </div>
  )
}

export default Personal
