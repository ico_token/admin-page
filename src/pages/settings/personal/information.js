import React from 'react'

function Information() {
  return (
    <div>
      <div className="col-lg-12">
        <section className="box has-border-left-3">
          <header className="panel_header gradient-blue">
            <h2 className="title pull-left w-text">Personal Information</h2>
          </header>
          <div className="content-body">
            <div className="row">
              <div className="form-container mt-20 no-padding-right no-padding-left over-h">
                <form id="icon_validate" action="#" noValidate="novalidate">

                  <div className=" col-xs-12">
                    <div className="row">
                      <div className="col-lg-12">
                        <div className="form-group">
                          <label className="form-label">Name</label>
                          <span className="desc">e.g. If it is not you, Please let us know</span>
                          <div className="controls">
                            <i className="" />
                            <input
                              type="text"
                              className="form-control"
                              name="formfield1"
                              value="Smith Wright"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-12">
                        <div className="form-group">
                          <label className="form-label">Email</label>
                          <span className="desc">e.g. info@example.com</span>
                          <div className="controls">
                            <i className="" />
                            <input
                              type="text"
                              className="form-control"
                              value="info@domain.com"
                              name="formfield2"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="form-label">Phone Number</label>
                          <span className="desc">e.g. 5643 34839</span>
                          <div className="controls">
                            <i className="" />
                            <input
                              type="text"
                              className="form-control"
                              value="(+3638) 734-3949"
                              name="formfield1"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="form-label">Birthday</label>
                          <span className="desc">e.g. 13 jan 1980</span>
                          <div className="controls">
                            <i className="" />
                            <input
                              type="text"
                              className="form-control"
                              value="17 September 1989"
                              name="formfield2"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-12">
                        <div className="form-group">
                          <label className="form-label">Zip Code</label>
                          <span className="desc">e.g. xxxxx</span>
                          <div className="controls">
                            <i className="" />
                            <input
                              type="text"
                              className="form-control"
                              value="17256"
                              name="formfield1"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="form-label">City</label>
                          <div className="controls">
                            <i className="" />
                            <input
                              type="text"
                              className="form-control"
                              value="New York"
                              name="formfield2"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="form-label">Country</label>
                          <span className="desc">e.g. France</span>
                          <div className="controls">
                            <i className="" />
                            <input
                              type="text"
                              className="form-control"
                              value="United States"
                              name="formfield2"
                            />
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-12">
                        <header className="prof-avtar gradient-blue">
                          <div className="avatar-img-wrapper">
                            <img
                              src="/data/crypto-dash/profile.png"
                              style={{ maxWidth: '100px' }}
                              alt=""
                            />
                          </div>
                          <div className="form-group mb-0">
                            <p className="form-label w-text mb-5">Edit Personal
                                                            image
                            </p>
                            <label className="form-label w-text">Upload File</label>
                            <span className="desc g-text">JPG, GIF or PNG Max size of 800K</span>
                            <div className="controls">
                              <input
                                type="file"
                                className=""
                                id="formfield10"
                                name="formfield10"
                              />
                            </div>
                          </div>
                        </header>
                      </div>
                      <div className="pull-right mt-30 mr-10">
                        <button
                          type="submit"
                          className="btn btn-primary btn-corner right15"
                        ><i
                          className="fa fa-check"
                        /> Update
                        </button>
                        <button type="button" className="btn btn-default btn-corner"><i
                          className="fa fa-times"
                        />
                        </button>
                      </div>

                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

export default Information
