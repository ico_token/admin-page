import React from 'react'

function SocialMediaInfo() {
  return (
    <div>
      <div className="col-lg-12">
        <section className="box ">
          <header className="panel_header gradient-blue">
            <h2 className="title pull-left w-text">Social Media Info</h2>
          </header>
          <div className="content-body">
            <div className="row">
              <form action="#" method="post">
                <div className="col-xs-12 mt-20">

                  <div className="form-group">
                    <label className="form-label">Facebook URL</label>
                    <span className="desc" />
                    <div className="controls">
                      <input
                        type="text"
                        className="form-control"
                        value="http://www.facebook.com/link"
                        id="field-31"
                      />
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="form-label">Twitter URL</label>
                    <span className="desc" />
                    <div className="controls">
                      <input
                        type="text"
                        className="form-control"
                        value="http://www.twitter.com/link"
                        id="field-41"
                      />
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="form-label">Google Plus URL</label>
                    <span className="desc" />
                    <div className="controls">
                      <input
                        type="text"
                        className="form-control"
                        value="http://www.googleplus.com/link"
                        id="field-51"
                      />
                    </div>
                  </div>

                </div>
                <div className="col-xs-12 col-sm-9 col-md-8 padding-bottom-30">
                  <div className="text-left">
                    <button type="button" className="btn btn-primary gradient-blue">Save
                    </button>
                    <button type="button" className="btn">Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

export default SocialMediaInfo
