import React from 'react'

function Profile() {
  return (
    <div>
      <div className="col-lg-4">
        <div className="row">

          <div className="col-md-12">
            <section className="box has-border-left-3">
              <div className="content-body">

                <div className="uprofile-image mt-30">
                  <div className="prof-contain relative">
                    <img alt="" src="/data/profile/user.png" className="img-responsive" />
                    <span className="prof-check fa fa-check" />
                  </div>
                </div>
                <div className="uprofile-name">
                  <h3>
                    <a href="#">Smith Wright</a>
                    <span className="uprofile-status online" />
                  </h3>
                  <p className="uprofile-title">Crypto Trader</p>
                </div>
                <div className="uprofile-info v2">
                  <ul className="list-unstyled mb-0">
                    <li className="pt0">
                      <h5 className="mt-0 mb-0">
                        <i
                          className="fa fa-envelope"
                        /> Email
                      </h5>
                      <span>info@domain.com</span>
                    </li>
                    <li><h5 className="mt-0 mb-0"><i className="fa fa-home" /> Address</h5><span>New York, USA</span>
                    </li>
                    <li><h5 className="mt-0 mb-0"><i className="fa fa-phone" /> Phone</h5><span>(+3638) 734-3949</span>
                    </li>
                    <li><h5 className="mt-0 mb-0"><i className="fa fa-user" /> Birthday</h5><span>17 September 1989</span>
                    </li>
                    <li><h5 className="mt-0 mb-0"><i className="fa fa-credit-card" /> Zip Code
                    </h5><span>17256</span>
                    </li>
                    <li><h5 className="mt-0 mb-0"><i className="fa fa-bullseye" /> City</h5><span>New York</span>
                    </li>
                    <li><h5 className="mt-0 mb-0"><i className="fa fa-crosshairs" /> Country</h5>
                      <span>United States</span>
                    </li>

                  </ul>
                </div>
                <div className="uprofile-buttons">
                  <a className="btn btn-md btn-primary">Send Message</a>
                  <a className="btn btn-md btn-primary">Add as Friend</a>
                </div>
                <div className=" uprofile-social no-mb">
                  <a href="#" className="btn btn-primary btn-md facebook">
                    <i className="fa fa-facebook icon-xs" />
                  </a>
                  <a href="#" className="btn btn-primary btn-md twitter">
                    <i className="fa fa-twitter icon-xs" />
                  </a>
                  <a href="#" className="btn btn-primary btn-md google-plus">
                    <i className="fa fa-google-plus icon-xs" />
                  </a>
                  <a href="#" className="btn btn-primary btn-md dribbble">
                    <i className="fa fa-dribbble icon-xs" />
                  </a>
                </div>

              </div>
            </section>
          </div>
          <div className="col-lg-12">
            <section className="box has-border-left-3">
              <header className="panel_header gradient-blue">
                <h2 className="title pull-left w-text">Monthly Purchase Limits</h2>

              </header>
              <div className="content-body">
                <div className="row">
                  <div className="col-xs-12 mt-20">
                    <div className=" trade-limit">
                      <h6 className="angle-round">
                                                Credit Card Limit
                        <span className="number">
                          <b
                            className="blue-text"
                          >1256 USD
                          </b> / 2500 USD
                        </span>
                      </h6>
                      <div className="progress progress-cls">
                        <div
                          className="progress-bar has-gradient-to-right-bottom"
                          style={{ width: '74%' }}
                        />
                      </div>
                    </div>
                    <div className=" trade-limit">
                      <h6 className="angle-round">
                                                Voucher Limit
                        <span className="number">
                          <b
                            className="blue-text"
                          >250 USD
                          </b> / 2500 USD
                        </span>
                      </h6>
                      <div className="progress progress-cls">
                        <div
                          className="progress-bar has-gradient-to-right-bottom"
                          style={{ width: '34%' }}
                        />
                      </div>
                    </div>
                    <div className=" trade-limit">
                      <h6 className="angle-round">
                                                Banking Limit
                        <span className="number">
                          <b className="blue-text">
                              2304 USD
                          </b>/ 2500 USD
                        </span>
                      </h6>
                      <div className="progress progress-cls">
                        <div
                          className="progress-bar has-gradient-to-right-bottom"
                          style={{ width: '94%' }}
                        />
                      </div>
                    </div>
                    <div className=" trade-limit mb-0">
                      <h6 className="angle-round">
                          E-Wallet Limit
                        <span className="number">
                          <b className="blue-text">
                                  356 USD
                          </b> / 2500 USD
                        </span>
                      </h6>
                      <div className="progress progress-cls mb-0">
                        <div
                          className="progress-bar has-gradient-to-right-bottom"
                          style={{ width: '44%' }}
                        />
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Profile
