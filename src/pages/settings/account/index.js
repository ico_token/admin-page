import React from 'react'
import Security from './security'
import Preferences from './preferences'

function Account() {
  return (
    <div>
      <div className="col-xs-12">
        <div className="page-title">

          <div className="pull-left">
            <h1 className="title">Account Settings</h1>
          </div>

          <div className="pull-right hidden-xs">
            <ol className="breadcrumb">
              <li>
                <a href="#"><i className="fa fa-home" />Home</a>
              </li>
              <li className="active">
                <strong>Account Settings</strong>
              </li>
            </ol>
          </div>

        </div>
      </div>

      <div className="clearfix" />

      <Preferences />

      <Security />

      <div className="clearfix" />
    </div>
  )
}

export default Account
