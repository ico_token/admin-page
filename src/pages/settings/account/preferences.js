import React from 'react'

function Preferences(props) {
  return (
    <div>
      <div className="col-lg-6">
        <div className="row">
          <div className="col-lg-12">
            <section className="box has-border-left-3">
              <header className="panel_header" style={{ borderBottom: '1px solid #eee' }}>
                <h2 className="title pull-left">
                  <img src="/data/crypto-dash/set2.png" className="wd mr-5" alt="" />
                                    Buying Preferences
                </h2>
                <div className="actions panel_actions pull-right">
                  <div className="form-group no-mb">
                    <button type="submit" className="btn btn-primary btn-corner ">
                      <i className="fa fa-check" />
                                            Update Settings
                    </button>
                  </div>
                </div>
              </header>
              <div className="content-body">
                <div className="row">
                  <div className="form-container mt-20 no-padding-right no-padding-left over-h">
                    <form id="icon_validate" action="#" noValidate="novalidate">

                      <div className=" col-xs-12">
                        <div className="row">
                          <div className="col-lg-12">
                            <div className="pull-left">
                              <h4>
                                <i className="fa fa-info-circle color-primary complete f-s-14" />
                                <small>Lorem ipsum dolor sit amet, adipisicing elit. Tempore sit fugia.</small>
                              </h4>
                            </div>
                          </div>

                          <div className="clearfix" />

                          <div className="col-lg-12 mt-30">
                            <label className="form-label mb-10 bold">Payment method Preferences</label>
                            <header className="prof-avtar" style={{ border: '2px dashed #ddd' }}>
                              <div
                                className="btn-group col-lg-12 no-pl"
                                style={{ width: '100%' }}
                              >
                                <div className="btn btn-default pay-bt">
                                  <div className="avatar-img ">
                                    <div className="avatar-img-wrapper v2">
                                      <img
                                        src="/data/icons/id-card.png"
                                        style={{ maxWidth: '30px' }}
                                        alt=""
                                      />
                                    </div>
                                    <div className="form-group text-left mb-10">
                                      <label
                                        className="form-label boldy"
                                        style={{ display: 'block' }}
                                      >
                                                                                Credit Card
                                      </label>
                                      <span className="desc ml-0">Transaction Fee 1.23%</span>
                                    </div>
                                  </div>

                                </div>
                                <button
                                  type="button"
                                  className="btn btn-default pay-btn dropdown-toggle"
                                  data-toggle="dropdown"
                                  aria-expanded="false"
                                >
                                  <span className="caret" />
                                  <span className="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul className="dropdown-menu" role="menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                </ul>
                              </div>
                            </header>
                            <p className="form-label mt-30 bold">Transaction Gas</p>
                            <div className="transaction-wrap mt-15 relative">
                              <div className="round">S</div>
                              <div className="designer-info">
                                <h5 className="boldy mb-5 mt-5">Standard 4 Hours</h5>
                                <small className="text-muted">transaction gas fee 0.0001 BTC</small>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <div className="col-lg-12">
            <section className="box has-border-left-3">
              <header className="panel_header" style={{ borderBottom: '1px solid #eee' }}>
                <h2 className="title pull-left">
                  <img src="/data/crypto-dash/set6.png" className="wd mr-5" alt="" />
                                    Two-factor authentication
                </h2>
                <div className="actions panel_actions pull-right">
                  <div className="form-group no-mb">
                    <button type="submit" className="btn btn-primary btn-corner ">
                      <i className="fa fa-check" />
                                            Update Settings
                    </button>
                  </div>
                </div>
              </header>
              <div className="content-body">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="text-center no-mt no-mb">
                      <div className="col-xs-12 no-pl no-pr">
                        <div className="col-xs-1 no-pl no-pr">
                          <div style={{ position: 'relative', padding: '7px 0 0' }}>
                            <input
                              tabIndex="5"
                              type="checkbox"
                              id="flat-checkbox-1"
                              className="skin-flat-blue"
                              checked
                            />
                          </div>
                        </div>
                        <div
                          className="col-xs-11 text-left no-pl"
                          style={{ position: 'relative', padding: 0 }}
                        >
                          <h4 className="icheck-label text-left form-label">
                            <small>
                              <strong>Enable or disable Two-Factor Authentication</strong>
                            </small>
                          </h4>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </section>
          </div>
          <div className="col-lg-12">
            <section className="box has-border-left-3">
              <header className="panel_header" style={{ borderBottom: '1px solid #eee' }}>
                <h2 className="title pull-left">
                  <img src="/data/crypto-dash/set5.png" className="wd mr-5" alt="" />
                                    Newsletter Subscription
                </h2>
                <div className="actions panel_actions pull-right">
                  <div className="form-group no-mb">
                    <button type="submit" className="btn btn-primary btn-corner "><i
                      className="fa fa-check"
                    /> Update Settings
                    </button>
                  </div>
                </div>
              </header>
              <div className="content-body">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="text-center no-mt no-mb">
                      <div className="text-left">
                        <h4 className="boldy"> Select preferred email settings</h4>
                        <h4>
                          <small>By enablng this option you will recieve Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis.</small>
                        </h4>
                      </div>
                      <div className="col-xs-12 no-pl no-pr">
                        <div className="col-xs-1 no-pl no-pr">
                          <div style={{ position: 'relative', padding: '7px 0 0' }}>
                            <input
                              tabIndex="3"
                              type="checkbox"
                              id="flat-checkbox-1"
                              className="skin-flat-blue"
                              checked
                            />
                          </div>
                        </div>
                        <div
                          className="col-xs-11 text-left no-pl"
                          style={{ position: 'relative', padding: 0 }}
                        >
                          <h4 className="icheck-label text-left form-label">
                            <small>
                              <strong>Revieve our hottest offers by subscribe to our Newsletter</strong>
                            </small>
                          </h4>
                        </div>
                      </div>
                      <div className="col-xs-12 no-pl no-pr">
                        <div className="col-xs-1 no-pl no-pr">
                          <div style={{ position: 'relative', padding: '7px 0 0' }}>
                            <input
                              tabIndex="3"
                              type="checkbox"
                              id="flat-checkbox-2"
                              className="skin-flat-blue"
                            />
                          </div>
                        </div>
                        <div
                          className="col-xs-11 text-left no-pl"
                          style={{ position: 'relative', padding: 0 }}
                        >
                          <h4 className="icheck-label text-left form-label">
                            <small>
                              <strong>Receive email when I buy a crypto currency</strong>
                            </small>
                          </h4>
                        </div>
                      </div>
                      <div className="col-xs-12 no-pl no-pr">
                        <div className="col-xs-1 no-pl no-pr">
                          <div style={{ position: 'relative', padding: '7px 0 0' }}>
                            <input
                              tabIndex="3"
                              type="checkbox"
                              id="flat-checkbox-3"
                              className="skin-flat-blue"
                            />
                          </div>
                        </div>
                        <div
                          className="col-xs-11 text-left no-pl"
                          style={{ position: 'relative', padding: 0 }}
                        >
                          <h4 className="icheck-label text-left no-mb form-label">
                            <small>
                              <strong>Receive email with recommedned action to my account</strong>
                            </small>
                          </h4>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Preferences
