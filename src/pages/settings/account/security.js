import React from 'react'

function Security() {
  return (
    <div>
      <div className="col-lg-6">
        <div className="row">
          <div className="col-lg-12">
            <section className="box has-border-left-3">
              <header className="panel_header" style={{ borderBottom: '1px solid #eee' }}>
                <h2 className="title pull-left">
                  <img src="/data/crypto-dash/set1.png" className="wd mr-5" alt="" />
                                    Identity Verfication
                </h2>
                <div className="actions panel_actions pull-right">
                  <div className="form-group no-mb">
                    <button type="submit" className="btn btn-primary btn-corner "><i
                      className="fa fa-check"
                    /> Update Settings
                    </button>
                  </div>
                </div>
              </header>
              <div className="content-body">
                <div className="row">
                  <div className="form-container mt-20 no-padding-right no-padding-left over-h">
                    <form id="icon_validate" action="#" noValidate="novalidate">

                      <div className=" col-xs-12">
                        <div className="row">
                          <div className="col-lg-12">
                            <div className="pull-left">
                              <h4>
                                <i className="fa fa-info-circle color-primary complete f-s-14" />
                                <small>To increase purchase limit please provide the following documents</small>
                              </h4>
                              <h4 className="help-block ml-20">
                                <small>maximum file size 4MB - The Verification process can last to 24 hours</small>
                              </h4>
                              <ul className="ml-20 mt-30 list-unstyled">
                                <li>
                                  <h5>
                                    <i className="fa fa-dot-circle-o blue-text mr-10" />
                                    Proof of Identity (Government issued ID or Passport)
                                  </h5>
                                </li>
                                <li>
                                  <h5>
                                    <i className="fa fa-dot-circle-o blue-text mr-10" />
                                    Selfie of holding clearly your identity document in your hands
                                  </h5>
                                </li>
                              </ul>
                            </div>
                          </div>

                          <div className="clearfix" />

                          <div className="col-lg-12 mt-30">
                            <label className="form-label mb-10 bold">Upload an ID</label>
                            <header className="prof-avtar" style={{ border: '2px dashed #ddd' }}>
                              <div className="avatar-img-wrapper">
                                <img
                                  src="/data/crypto-dash/profile.png"
                                  style={{ maxWidth: '100px' }}
                                  alt=""
                                />
                              </div>
                              <div className="form-group mb-0">
                                <p className="form-label mb-5 blue-text bold">Upload proof of identity</p>
                                <label className="form-label">Upload File</label>
                                <span className="desc">JPG, GIF or PNG Max of 800K</span>
                                <div className="controls">
                                  <input
                                    type="file"
                                    className=""
                                    id="formfield10"
                                    name="formfield10"
                                  />
                                </div>
                              </div>
                            </header>
                          </div>
                          <div className="col-lg-12 mt-30">
                            <label className="form-label mb-10 bold">Upload a Selfie</label>
                            <header className="prof-avtar gradient-blue">
                              <div className="avatar-img-wrapper">
                                <img
                                  src="/data/crypto-dash/profile.png"
                                  style={{ maxWidth: '100px' }}
                                  alt=""
                                />
                              </div>
                              <div className="form-group mb-0">
                                <p className="form-label w-text mb-5">Upload a Selfie</p>
                                <label className="form-label w-text">Upload File</label>
                                <span className="desc g-text">JPG, GIF or PNG Max of 800K</span>
                                <div className="controls">
                                  <input
                                    type="file"
                                    className=""
                                    id="formfield10"
                                    name="formfield10"
                                  />
                                </div>
                              </div>
                            </header>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <div className="clearfix" />

          <div className="col-lg-12">
            <section className="box ">
              <header className="panel_header" style={{ borderBottom: '1px solid #eee' }}>
                <h2 className="title pull-left">
                  <img src="/data/crypto-dash/set4.png" className="wd mr-5" alt="" />
                  Security: Password
                </h2>
                <div className="actions panel_actions pull-right">
                  <div className="form-group no-mb">
                    <button
                      type="submit"
                      className="btn btn-primary btn-corner "
                    >
                      <i className="fa fa-check" />
                      Update Settings
                    </button>
                  </div>
                </div>
              </header>
              <div className="content-body">
                <div className="row">
                  <form action="#" method="post">
                    <div className="col-xs-12 mt-20">

                      <div className="form-group">
                        <label className="form-label">Current Password</label>
                        <span className="desc" />
                        <div className="controls">
                          <input
                            type="password"
                            className="form-control"
                            value=""
                            placeholder="Enter you current password"
                            id="field-31"
                          />
                        </div>
                      </div>

                      <div className="form-group">
                        <label className="form-label">New Password</label>
                        <span className="desc" />
                        <div className="controls">
                          <input
                            type="password"
                            className="form-control"
                            value="password"
                            id="field-41"
                          />
                        </div>
                      </div>

                    </div>
                    <div className="col-xs-12 padding-bottom-30">
                      <div className="pull-left">
                        <h4>
                          <i className="fa fa-info-circle color-primary complete f-s-14" />
                          <small>Avoid using easy to guess password</small>
                        </h4>
                        <ul className="ml-20 mt-30 list-unstyled">
                          <li>
                            <h5>
                              <i className="fa fa-dot-circle-o blue-text mr-10" />
                              Password must be at lest 7 - 15 character
                            </h5>
                          </li>
                          <li>
                            <h5>
                              <i className="fa fa-dot-circle-o blue-text mr-10" />
                              Password must contain Lowercase and uppercase letters
                            </h5>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Security
