import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { Row, Col } from 'antd'
import { TYPES, actions } from '../../store/actions'
import { COIN_SYSTEM, ICON_COIN } from '../../utils/constant'
import Button from '../../components/button'
import TitlePage from '../../components/title-page'
// import Popconfirm from '../../components/popconfirm'
import Notification from '../../components/notification'
import './style.scss'
import 'antd/es/row/style/css'
import 'antd/es/col/style/css'
import { withLocalize } from 'react-localize-redux'

const { getListCoin, deleteCoin } = actions

const Coins = ({ history, translate }) => {
  const dispatch = useDispatch()

  const [listCoins, setListCoins] = useState([])

  const handleGetListCoin = () => {
    dispatch(getListCoin({}, (action, data) => {
      if (action === TYPES.GET_LIST_COIN_SUCCESS) {
        setListCoins(data)
      }
    }))
  }

  useEffect(() => {
    handleGetListCoin()
  }, [])

  // const handleDeleteCoin = ({id}) => {
  //   dispatch(deleteCoin({id}, (action) => {
  //     if (action === TYPES.DELETE_COIN_SUCCESS) {
  //       Notification.success('Delete coin success')
  //     }
  //   }))
  // }

  return (
    <div>
      <TitlePage namePage={translate('coins.coins')} />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12">
            <section className="box">
              <header className="box-header">
                <h2 className="title pull-left">{translate('coins.list-coin')}</h2>
                <Button
                  className="btn btn-primary"
                  onClick={() => history.push('/coin-create')}
                >
                  {translate('utils.create')}
                </Button>
              </header>
              <div className="content-body">
                <hr />
                <div id="transactions" className="box-transactions">
                  <div className="transactions-table-th">
                    <Row>
                      <Col span={2}>
                        <p />
                      </Col>
                      <Col span={4}>
                        <p>{translate('utils.name')}</p>
                      </Col>
                      <Col span={6}>
                        <p>{translate('coins.description')}</p>
                      </Col>
                      <Col span={4}>
                        <p>{translate('coins.last-block')}</p>
                      </Col>
                      <Col span={3}>
                        <p>{translate('utils.ratio')}</p>
                      </Col>
                      <Col span={3}>
                        <p>{'Tỉ lệ hold'}</p>
                      </Col>
                      <Col span={3} className="text-center"></Col>
                    </Row>
                  </div>
                  <div className="transactions-table-tbody">
                    {listCoins?.length > 0 && listCoins.map((e, index) => (
                      <section className="card pull-up" key={index}>
                        <div className="card-content">
                          <div className="card-body">
                            <Row>
                              <Col span={2}>
                                {
                                  e.code === COIN_SYSTEM.CODE
                                    ? <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system mr-1" />
                                    : <i className={ICON_COIN[e.code]?.COLOR} />
                                }
                              </Col>
                              <Col span={4}>
                                {e.name}
                              </Col>
                              <Col span={6}>
                                {e.description}
                              </Col>
                              <Col span={4}>
                                {e.latest_block}
                              </Col>
                              <Col span={3}>
                                {e.ratio}
                              </Col>
                              <Col span={3}>
                                {e.ratio_hold}
                              </Col>
                              <Col span={3} className="action text-center">
                                <i className="fa fa-edit" onClick={() => history.push(`/coin-update/${e.id}`)} />
                                {/* <Popconfirm
                                  title="Are you sure?"
                                  onConfirm={() => handleDeleteCoin({id: e.id})}
                                  okText="Yes"
                                  cancelText="No"
                                >
                                  <i className="fa fa-trash" />
                                </Popconfirm> */}
                              </Col>
                            </Row>
                          </div>
                        </div>
                      </section>
                    ))}
                  </div>
                </div>
              </div>
            </section>
          </div>
        </section>
      </div>
    </div>
  )
}

export default withLocalize(Coins)
