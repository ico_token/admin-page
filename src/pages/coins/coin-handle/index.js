import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { Formik, Form } from 'formik'
import { object, string, number } from 'yup'
import { TYPES, actions } from '../../../store/actions'
import { trimValues } from '../../../utils/trim'
import Field from '../../../components/field-custom'
import Input from '../../../components/input'
import Button from '../../../components/button'
import TitlePage from '../../../components/title-page'
import Notification from '../../../components/notification'
import { withLocalize } from 'react-localize-redux'

import './style.scss'

const { getListCoin, createCoin, updateCoin } = actions

const CoinCreate = ({ history, translate }) => {
  const dispatch = useDispatch()

  const [listCoins, setListCoins] = useState([])
  const [coinItem, setCoinItem] = useState(null)
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    dispatch(getListCoin({}, (action, data) => {
      if (action === TYPES.GET_LIST_COIN_SUCCESS) {
        setListCoins(data)
      }
    }))
  }, [])

  useEffect(() => {
    if (window.location.pathname && listCoins?.length) {
      const pathname = window.location.pathname.split('/')
      if (pathname?.length) {
        const coin = listCoins.find((e) => +e.id === +pathname[pathname.length - 1])
        setCoinItem(coin)
      }
    }
  }, [window.location.pathname, listCoins])

  const onSubmit = (values, { resetForm }) => {
    const formData = trimValues(values)
    setIsLoading(true)

    if (coinItem?.id) {
      dispatch(
        updateCoin({
          ...formData,
          ratio: +formData.ratio,
          ratio_hold: +formData.ratio_hold,
          id: +coinItem?.id
        }, (action) => {
          setIsLoading(false)
          if (action === TYPES.UPDATE_COIN_SUCCESS) {
            history.push('/coins')
            setCoinItem(null)
            Notification.success(translate('success.UPDATE_COIN'))
          }
        })
      )
    } else {
      dispatch(
        createCoin({
          ...formData,
          ratio: +formData.ratio
        }, (action) => {
          setIsLoading(false)
          if (action === TYPES.CREATE_COIN_SUCCESS) {
            Notification.success(translate('success.CREATE_COIN'))
            resetForm()
          }
        })
      )
    }
  }

  // Render form
  const renderForm = ({ handleSubmit, ...form }) => (
    <Form>
      <Field
        form={form}
        name="code"
        label={translate('coins.code')}
        component={Input}
        isRequired
      />
      <Field
        form={form}
        name="name"
        label={translate('utils.name')}
        component={Input}
        isRequired
      />
      <Field
        form={form}
        name="description"
        label={translate('coins.description')}
        component={Input}
      />
      <Field
        form={form}
        name="last_block"
        label={translate('coins.last-block')}
        component={Input}
      />
      <Field
        form={form}
        name="ratio"
        label={translate('utils.ratio')}
        component={Input}
        isRequired
      />
      <Field
        form={form}
        name="ratio_hold"
        label={"Tỉ lệ hold"}
        component={Input}
        isRequired
      />
      <div className="form-box-btn">
        <Button
          type="primary"
          onClick={handleSubmit}
          loading={isLoading}
          disabled={isLoading}
        >
          {coinItem?.id ? translate('utils.update') : translate('utils.create')}
        </Button>
      </div>
    </Form>
  )

  const initialValues = (coinItem?.id) ? {
    ...coinItem
  } : {}

  // Valid form
  const validationSchema = object().shape({
    code: string().required().trim(),
    name: string().required().trim(),
    ratio: number().required().moreThan(
			0,'Must be more than 0'
		)
  })

  return (
    <div className="coin-handle-page">
      <TitlePage namePage={coinItem?.id ? translate('coins.update-coin') : translate('coins.create-coin') } />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header coin_header">
                <h2 className="title pull-left">{coinItem?.id ? translate('coins.update-coin') : translate('coins.create-coin') }</h2>
              </header>
              <div className="content-body">
                <Formik
                  validateOnChange={false}
                  validateOnBlur={false}
                  initialValues={initialValues}
                  validationSchema={validationSchema}
                  onSubmit={onSubmit}
                  component={renderForm}
                  enableReinitialize
                />
              </div>
            </section>
          </div>
        </section>
      </div>
    </div>
  )
}

export default withLocalize(CoinCreate)
