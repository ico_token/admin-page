import React, { useEffect, useState } from "react";
import Modal from "@/components/modal";
import Button from "@/components/button";
import Notification from "@/components/notification";
import { TYPES, actions } from "@/store/actions";
import { useDispatch } from "react-redux";
import { COIN_SYSTEM, ICON_COIN } from "@/utils/constant";
import "./style.scss";

const { handleWithdrawRequest,getListWithdrawRequest } = actions;

const ModalApproved = ({
  close,
  isOpen,
  reload,
  withDrawApproved,
  translate,
  amount,
}) => {
  const { coin_id, user } = withDrawApproved;
  const walletCoin = user?.wallet?.find((e) => e.coin_id === coin_id);
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isWarning, setIsWarning] = useState(false)

  useEffect(() => {
    if (withDrawApproved) {
      dispatch(
        getListWithdrawRequest({
			status : 0,
			email : user.email
		}, (action, data) => {
          if (action === TYPES.GET_LIST_WITHDRAW_REQUEST_SUCCESS) {
            console.log(data);
			if(data && data.count > 1){
				setIsWarning(true)
			}
          }
        })
      );
    }
  }, [withDrawApproved]);

  const handleSubmit = () => {
    const value = {
      is_approved: true,
      id: withDrawApproved.id,
    };
    setIsLoading(true);
    dispatch(
      handleWithdrawRequest(value, (action) => {
        setIsLoading(false);
        if (action === TYPES.HANDLE_WITHDRAW_REQUEST_SUCCESS) {
          reload();
          close();
          return Notification.success(translate("success.APPROVE_WITHDRAW"));
        }
      })
    );
  };

  const copyToClipboard = (id) => {
    const copyText = document.getElementById(id);
    const textArea = document.createElement("textarea");
    textArea.value = copyText.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
    return Notification.success(translate("success.copy"));
  };

  return (
    <Modal
      visible={isOpen}
      onCancel={close}
      title={<div>{translate("withdraw.approve-withdraw-request")} <span style={{color : 'red'}}>{isWarning && `(!)`}</span></div> }
      destroyOnClose={true}
    >
      <div>
        <div className="row">
          <div className="col-xs-12">
            <div className="uprofile-info v2 form-approved">
              <ul className="list-unstyled mb-0">
                <li className="pt0 form-info">
                  <h5 className="mt-0 mb-0 name-form">
                    {translate("coins.coins")}
                  </h5>
                  {withDrawApproved?.coin?.code === COIN_SYSTEM.CODE ? (
                    <img
                      src={COIN_SYSTEM.NORMAL}
                      alt=""
                      className="coin-system mr-1"
                    />
                  ) : (
                    <i
                      className={ICON_COIN[withDrawApproved?.coin?.code]?.COLOR}
                    />
                  )}
                  <label>{withDrawApproved.coin.code}</label>
                </li>

                <li className="pt0 form-info">
                  <h5 className="mt-0 mb-0 name-form">
                    {translate("utils.email")}
                  </h5>
                  <span className="text-mail">{user?.email}</span>
                </li>

                <li className="pt0 form-info">
                  <h5 className="mt-0 mb-0 name-form">
                    {translate("utils.amount")}
                  </h5>
                  <span className="text-mail green-text boldy" id="amount-copy">
                    {amount}
                  </span>
                  <i
                    onClick={() => copyToClipboard("amount-copy")}
                    className="fa fa-copy copy"
                  />
                </li>

                <li className="pt0 form-info">
                  <h5 className="mt-0 mb-0 name-form">
                    {translate("utils.address")}
                  </h5>
                  <span
                    className="text-mail"
                    id="address-copy"
                    title={walletCoin.address}
                  >
                    {walletCoin.address}
                  </span>
                  <i
                    onClick={() => copyToClipboard("address-copy")}
                    className="fa fa-copy copy"
                  />
                </li>
              </ul>
            </div>

            <div className="text-center">
              <Button
                className="btn btn-primary btn-corner"
                onClick={handleSubmit}
                loading={isLoading}
                disabled={isLoading}
              >
                {translate("utils.approve")}
              </Button>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default ModalApproved;
