import React, { useEffect, useState } from 'react';
import Input from '@/components/input'
import Button from '@/components/button'
import Pagination from '@/components/pagination'
import Select from '@/components/select'
import TitlePage from '@/components/title-page'
import Popconfirm from '@/components/popconfirm'
import Notification from '@/components/notification'
import { TYPES, actions } from '@/store/actions'
import ModalApproved from './modal-approved'
import { COIN_SYSTEM, ICON_COIN, FROM_TIME } from '@/utils/constant'
import BigNumber from 'bignumber.js'
import { useDispatch } from 'react-redux';
import './style.scss';
import { Collapse } from 'antd';
import 'antd/es/collapse/style/css'
import { withLocalize } from 'react-localize-redux'
import moment from 'moment';
const { Panel } = Collapse;

const { getListWithdrawRequest, handleWithdrawRequest } = actions;
const WithdrawRequests = ({translate}) => {
  const mockSearch = {
    page: 1,
    limit: 10
  }

  const OPTION_STATUS = [
    { name: translate('transactions.all'), value: 3 },
    { name: translate('utils.waiting'), value: 0 },
    { name: translate('utils.approve'), value: 1 },
    { name: translate('utils.reject'), value: 2 }
  ]

  const getTypeStatus = (status) => {
    switch (status) {
      case 0:
        return <span className="badge w-70 round-warning">{translate('utils.waiting')}</span>
      case 1:
        return <span className="badge w-70 round-success">{translate('utils.approve')}</span>
      case 2:
        return <span className="badge w-70 round-danger">{translate('utils.reject')}</span>
      default:
    }
  };

  const dispatch = useDispatch();
  const [listWithdraw, setListWithdraw] = useState([]);
  const [totalWithdraw, setTotalWithdraw] = useState(0);
  const [page, setPage] = useState(mockSearch.page);
  const [limit] = useState(mockSearch.limit);
  const [valueSearch, setValueSearch] = useState(mockSearch);
  const [withDrawApproved, setWithDrawApproved] = useState({})
  const [openModalApproved, setOpenModalApproved] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    getList(valueSearch)
  }, []);

  const getList = (value) => {
    setIsLoading(true)
    dispatch(
      getListWithdrawRequest(value, (action, data) => {
          setIsLoading(false)
          if (action === TYPES.GET_LIST_WITHDRAW_REQUEST_SUCCESS) {
            setListWithdraw(data.rows);
            setTotalWithdraw(data.count);
          }
      })
    )
  };

  const onChangePage = (p) => {
    setPage(p);
    setValueSearch({...valueSearch, page: p})
    getList({ ...valueSearch, page: p })
  };

  const changeSearch = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if(name === 'status' ) {
      if(value === 3) {
        const newData = { ...valueSearch };
        delete newData.status
        setValueSearch(newData)
      } else {
        setValueSearch({
          ...valueSearch,
          [name]: value
        })
      }
      
    } else {
      setValueSearch({
        ...valueSearch,
        [name]: value.trim()
      })
    }
  };

  const handleSearch = () => {
    getList(valueSearch)
  };
  const handleReset = () => {
    setValueSearch(mockSearch)
    getList(mockSearch)
  };

  const handleConfirmWithDraw = ({ is_approved, id }) => {
    const value = { is_approved, id }
    dispatch(
      handleWithdrawRequest(value, (action, data) => {
        if (action === TYPES.HANDLE_WITHDRAW_REQUEST_SUCCESS) {
          getList(valueSearch)
          return Notification.success(translate('success.REJECT_WITHDRAW'))
        }
      })
    )
  }

  const handleOpenModalApproved = (item) => {
    setWithDrawApproved(item)
    setOpenModalApproved(true)
  }

  const closeModal = () => {
    setOpenModalApproved(false)
    setWithDrawApproved({})
  }

  const convertCoin = (value, ratio) => {
    const coin = new BigNumber(value).dividedBy(ratio).toNumber()
    BigNumber.config({
      DECIMAL_PLACES: 8
    })
    return coin
  }

  return (
    <div>
      <TitlePage
        namePage={translate('withdraw.withdraw-request')}
      />

      <div className="col-lg-12">
        <section className="box ">
          <div className="col-lg-12 mt-15">
            <Collapse >
              <Panel header={translate('utils.filter')}>
                <div>
                  <div className="field col-sm-6"	>
                    <label>{translate('utils.email')}</label>
                    <Input name="email" onChange={changeSearch} value={valueSearch.email}/>
                  </div>

                  <div className="field col-sm-6">
                    <label>{translate('system-setting.status')}</label>
                    <Select
                      name="status" onChange={changeSearch}
                      value={[0,1,2].includes(valueSearch.status) ? valueSearch.status : translate('transactions.all')}
                      options={OPTION_STATUS}
                    />
                  </div>
                </div>

                <div>
                <Button
                  margin={true}
                  onClick={handleSearch}
                  loading={isLoading}
                  disabled={isLoading}
                >{translate('utils.search')}</Button>

                <Button
                  onClick={handleReset}
                  loading={isLoading}
                  disabled={isLoading}
                >{translate('utils.reset')}</Button>

                </div>
              </Panel>
            </Collapse>
          </div>

          <div className="col-lg-12">
            <section className="box">
              <header className="panel_header">
                <h2 className="title pull-left">{translate('withdraw.list-withdraw-request')}</h2>
              </header>
              <div className="content-body">

                <div className="row">
                  <div className="col-xs-12">
                    <div className="table-responsive" data-pattern="priority-columns">
                      <table id="tech-companies-1" className="table vm trans table-small-font no-mb table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>{translate('coins.coins')}</th>
                            <th>{translate('utils.email')}</th>
                            <th className="text-center">{translate('utils.amount')}</th>
                            <th className="text-center">{translate('system-setting.status')}</th>
                            <th className="text-center">TSI</th>
                            <th className="text-center">{translate('utils.ratio')}</th>
                            <th>{translate('utils.time')}</th>
                            <th className="text-center"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            listWithdraw.map((item, i) => (
                                <tr key={i}>
                                  <td>
                                    {
                                      item.coin.code === COIN_SYSTEM.CODE
                                        ? <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system mr-1"  style={{width : "30px"}}/>
                                        : <i className={ICON_COIN[item.coin.code]?.COLOR} />
                                    }
                                  </td>
                                  <td><small className="text-muted">{item.user.email}</small></td>
                                  <td className="text-center"> 
                                    <small className="green-text boldy text-center">
                                      {convertCoin(item?.number_of_tosi, item?.ratio)}
                                    </small>
                                  </td>
                                  <td className="text-center">{getTypeStatus(item.status)}</td>
                                  <td className="text-center"><small className="green-text boldy">{item.number_of_tosi}</small></td>
                                  <td className="text-center"><small className="text-muted text-center">{item.ratio}</small></td>
                                  <td><small className="text-muted">{moment(item.created_at).format(FROM_TIME)}</small></td>
                                  <td className="green-text boldy text-center">
                                    {
                                      item.status === 0 &&
                                      <div className="btn-group btn-group-justified btn-detail">
                                        <a className="btn btn-success btn-xs"
                                          onClick={() => handleOpenModalApproved(item)}
                                        >{translate('utils.approve')}</a>
                                        <Popconfirm
                                          title={translate('withdraw.reject-withdraw-request')}
                                          onConfirm={() => handleConfirmWithDraw({ is_approved: false, id: item.id })}
                                          okText={translate('utils.yes')}
                                          cancelText={translate('utils.no')}
                                        >
                                          <a className="btn btn-danger btn-xs">{translate('utils.reject')}</a>
                                        </Popconfirm>
                                      </div>
                                    }
                                  </td>
                                </tr>
                              ))
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
          {
            totalWithdraw > 10 &&
            <div className="col-lg-12">
              <Pagination
                current={page} onChange={onChangePage}
                total={totalWithdraw} pageSize={limit}
              />
            </div>
          }
        </section>
      </div>
      {
        openModalApproved && 
        <ModalApproved
          close={() => closeModal()}
          withDrawApproved={withDrawApproved}
          translate={translate}
          amount={convertCoin(withDrawApproved?.number_of_tosi, withDrawApproved?.ratio)}
          isOpen={openModalApproved}
          reload={() => getList(valueSearch)}
        />
      }
      
    </div>

  )
}

export default withLocalize(WithdrawRequests)
