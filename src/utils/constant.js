import IconTosi from '../../public/assets/images/logo-tosi.png'

const COIN_SYSTEM = {
  NORMAL: IconTosi,
  CODE: 'TSI'
}

const ICON_COIN = {
  BTC: {
    COLOR: 'cc BTC mr-2 font-large-2 Color-BTC',
    NORMAL: 'cc BTC-alt'
  },
  ETH: {
    COLOR: 'cc ETH mr-2 font-large-2 accent-3 Color-ETH',
    NORMAL: 'cc ETH-alt'
  },
  XRP: {
    COLOR: 'cc XRP-alt mr-2 font-large-2 Color-XRP',
    NORMAL: 'cc XRP-alt'
  },
  TRX: {
    COLOR: 'cc TRX font-large-2 Color-TRX',
    NORMAL: 'cc TRX'
  },
  BNB: {
    COLOR: 'cc BNB mr-2 font-large-2 Color-BNB',
    NORMAL: 'cc BNB'
  },
  USDT: {
    COLOR: 'cc USDT mr-2 font-large-2 Color-USDT',
    NORMAL: 'cc USDT'
  }
}

const FROM_TIME = 'YYYY/MM/DD HH:mm:ss'

const OPTION_MONTH = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
]

const RANK = ['NONE', 'BEGINNER', 'SILVER', 'GOLD', 'RUBY', 'DIAMOND', 'CROWN' ];

const RANK_IMG = {
  1: '/assets/images/beginer.png',
  2: '/assets/images/silver.png',
  3: '/assets/images/gold.png',
  4: '/assets/images/ruby.png',
  5: '/assets/images/diamond.png',
  6: '/assets/images/crown.png',
}

const TRANSACTION_TYPE = [
  'DEPOSIT',
  'WITHDRAW',
  'REFERRAL', 
  'CONVERT', 
  'WITHDRAW_TRUST_TOSI',
  'SEND_TO_ADMIN',
  'REQUEST_COIN',
  'RECALL_INVESTED_TOSI'
]

const MAX_SIZE_IMG = 10

const BYTE_TO_MB = (1024 * 1024) / 2

export {
  COIN_SYSTEM,
  ICON_COIN,
  FROM_TIME,
  OPTION_MONTH,
  RANK,
  RANK_IMG,
  TRANSACTION_TYPE,
  MAX_SIZE_IMG,
  BYTE_TO_MB
}
