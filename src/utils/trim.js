const trimValues = (values) => {
  Object.keys(values).forEach((x) => {
    if (typeof values[x] === 'string') {
      values[x] = values[x].trim()
    }
  })
  return values
}

export {
  trimValues
}