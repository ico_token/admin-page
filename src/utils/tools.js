export const getGender = (code) => {
  switch (code) {
    case 0:
      return 'Female'
    case 1:
      return 'Male'
    case 2:
      return 'Other'
    default:
      return ''
  }
}
