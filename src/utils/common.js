import BigNumber from 'bignumber.js'

const zeroPad = (value, size) => {
  let s = `000000000${value}`
  return s.substr(s.length - size)
}

const formatNumber = (x) => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')

const formatAmount = (value = 0) => {
  if (!(+value)) return 0

  const number = `${value}`
  const x = number.split('.')
  let x1 = x[0]
  const x2 = x.length > 1 ? `.${x[1]}` : ''
  const rgx = /(\d+)(\d{3})/
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2')
  }
  return (x1 + x2)
}

const convertArrayToString = (array, symbol = ' ') => array.filter((item) => !!item).join(symbol)

const getBase64File = (file) => new Promise((resolve, reject) => {
  const reader = new FileReader()
  reader.readAsDataURL(file)
  reader.onload = () => resolve(reader.result)
  reader.onerror = (error) => reject(error)
})

const convertUsd = (value = 0, rate = 1) => {
  const amount = new BigNumber(value).times(rate)
  BigNumber.config({
    ROUNDING_MODE: BigNumber.ROUND_DOWN,
    DECIMAL_PLACES: 8
  })
  return new BigNumber(amount, 10).toNumber()
}

export {
  zeroPad,
  formatNumber,
  formatAmount,
  convertArrayToString,
  getBase64File,
  convertUsd
}
