import { push } from 'connected-react-router'
import { put, select } from 'redux-saga/effects'

import Storage from '@/utils/storage'
import Misc from '@/utils/misc'
import Notification from '@/components/notification'
import { actions } from '@/store/actions'
import Request from './request'

export default function sagaHelper({ api, successMessage, errorHandler }) {
  return function* ({ type, data, callback }) {
    const successType = `${type}_SUCCESS`
    const failureType = `${type}_FAILURE`

    try {
      yield put({ type: `${type}_REQUEST`, payload: data })

      const { success, result, error } = yield api(data)

      if (success) {
        yield put({ type: successType, data: result, payload: data })

        if (successMessage) Notification.success(successMessage)

        if (callback) callback(successType, result)
      } else {
        throw error
      }
    } catch (e) {
      const error = yield Misc.getErrorJsonBody(e)
      yield put({ type: failureType, error })

      const localize = yield select((state) => state.localize)
      const languageIndex = localize.languages.findIndex((e) => e.active)
      const getLocalizeErrorMessages = (name) => (localize.translations[`error-messages.${name}`] || [])[languageIndex]

      if (['TOKEN_EXPIRED'].includes(error?.message)) {
        Storage.clear()
        yield put(push('/login'))
        yield put(actions.clearStore())
      }

      if (['NOT_AUTHENTICATED_ERROR', 'USER_NOT_FOUND'].includes(error?.code)) {
        Storage.clear()
        Request.removeAccessToken()
        yield put(push('/login'))
      }

      if (errorHandler) {
        errorHandler(error, getLocalizeErrorMessages)
      } else if (error?.code) {
        const codeErr = getLocalizeErrorMessages(error.code)
        Notification.error(codeErr?.replaceAll('_', ' '))
      }

      if (callback) callback(failureType, error)
    }
  }
}
