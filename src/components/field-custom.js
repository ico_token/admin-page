import React from 'react'
import styled from 'styled-components'
import classNames from 'classnames'
import { withLocalize } from 'react-localize-redux'
import lodash from 'lodash'
import { Field } from 'formik'

const Box = styled.div`
  position: relative;

  .field-content {
    .error-message {
      text-align: right;
      font-size: 12px;
      color: red;
      height: 18px;
      margin-top: 3px;
    }
  }

  .required {
    color: red
  }
`

export default withLocalize(({
  component: Component,
  translate,
  className,
  form,
  name,
  label,
  isRequired,
  ...props
}) => {
  props = lodash.omit(props, [
    'activeLanguage',
    'addTranslation',
    'addTranslationForLanguage',
    'defaultLanguage',
    'ignoreTranslateChildren',
    'initialize',
    'languages',
    'setActiveLanguage',
    'renderToStaticMarkup'
  ])

  return (
    <Box className={classNames('field', className)}>
      <label>{label}{isRequired && <span className="required">*</span>}</label>
      <div className="field-content">
        <Field {...props} name={name} component={Component} />
        <p className="error-message">{form && form.errors[name]}</p>
      </div>
    </Box>
  )
})
