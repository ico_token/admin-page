import React from 'react'
import { Switch } from 'antd'
import classNames from 'classnames'
import 'antd/es/switch/style/css'

export default ({ value, className, ...props }) => (
  <Switch
    {...props}
    className={classNames(className, 'switch-custom-default')}
  />
)