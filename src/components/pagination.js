import React from 'react'
import 'antd/es/pagination/style/css'
import styled from 'styled-components'
import { Pagination } from 'antd'

const StyledPagination = styled(Pagination)`
    display: flex;
    justify-content: center;
    padding-bottom: 15px!important;
`

export default ({ ...props }) => (
    <StyledPagination {...props}/>
  )