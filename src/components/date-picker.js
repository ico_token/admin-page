import React from 'react'
import { DatePicker } from 'antd'
import classNames from 'classnames'
import styled from 'styled-components'
import 'antd/es/date-picker/style/css'

const StyledDatePicker = styled(DatePicker)`

`
export default ({ field, form, className, value, onChange, name, ...props }) => {
  const _onChange = (value) => {
    if (onChange) {
      onChange({ target: { value, name: field?.name || name } })
    }
    if (field) {
      field.onChange({ target: { value, name: field.name } })
    }
  }

  return (
    <StyledDatePicker
      {...field}
      {...props}
      onBlur={null}
      value={value || null}
      onChange={_onChange}
      className={classNames(className)}
    />
  )
}

  
