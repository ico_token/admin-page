import React, { Component } from 'react'
import { Select } from 'antd'
import lodash from 'lodash'
import styled from 'styled-components'
import 'antd/es/select/style/css'
import { RANK_IMG } from '@/utils/constant'

const { Option } = Select

const StyledSelect = styled(Select)`
  width: 100%;
  .ant-select-selection--single {
    /* height: 50px; */
  height: auto;
  display: flex;
}

`
const FormCoin = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 5px;
  img {
    width: 27px;
    height: 27px;
  }
  img, i {
    margin-right: 10px
  }
`

export default class extends Component {
  
  _onChange = (value) => {
    const { field, onChange, name } = this.props

    if (onChange) onChange({ target: { value, name: field?.name || name } })
    if (!lodash.isEmpty(field)) field.onChange({ target: { value, name: field.name } })
  }

  _renderOption = (option, index) => {
    const {  options } = this.props
    if (index) {
      return (
        <Option key={index} value={index} name={option}>
          <FormCoin>
            <img src={RANK_IMG[index]} title={options[index]} alt={options[index]} className="coin-system mr-1" />
            {option}
          </FormCoin> 
        </Option>
      )
    }
  }

  render() {
    const { field, options, onChange, optionBinding, value, renderOption, ...props } = this.props
    return (
      <StyledSelect
        {...field}
        {...props}
        onChange={this._onChange}
        value={field?.value || value}
      >
        {options.map(this._renderOption)}
      </StyledSelect>
    )
  }
}
