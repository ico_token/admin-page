import { message } from 'antd'
import 'antd/es/message/style/css'

class Notification {
  static success(text) {
    message.success(text)
  }

  static warning(text) {
    message.warning(text)
  }

  static error(text) {
    message.error(text)
  }
}

export default Notification
