import React from 'react'
import { Button } from 'antd'
import styled from 'styled-components'
import 'antd/es/button/style/css'

const StyledButton = styled(Button)`
  margin: ${props => props.margin && "15px"};
`

export default ({ children, ...props }) => (
  <StyledButton {...props}>{children}</StyledButton>
)
