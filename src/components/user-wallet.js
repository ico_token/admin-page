import React, { useEffect, useState } from 'react'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
// import Pagination from './pagination'
import { withLocalize } from 'react-localize-redux'
import HeaderForm from './header-form'

const { getUserWallet } = actions;

const UserWalletDetails = ({ params, translate }) => {
  const dispatch = useDispatch();
  const [walletDetails, setWalletDetails] = useState([]);
  // const [total, setTotal] = useState(0);
  // const [page, setPage] = useState(1);
  // const [limit] = useState(10);

  useEffect(() => {
    getUserWalletDetails()
  }, []);

  const getUserWalletDetails = () => {
    dispatch(
        getUserWallet({}, (action, data) => {
          if (action === TYPES.GET_USER_WALLET_SUCCESS) {
            setWalletDetails(data);
            // setTotal(data.length)
          }
        })
      )
  }

  // FIXME: flexible change pagination when data is large
  // const onChangePage = (p) => {
  //   setPage(p);
  // };

  return (
    <div>
      <div className="col-lg-12">
        <section className="box has-border-left-3">
          <HeaderForm
            nameForm={translate('wallet.user-wallet')}
          />
          <div className="content-body">
            <div className="row">
              <div className="col-xs-12">

                <div className="table-responsive" data-pattern="priority-columns">
                  <table
                    id="tech-companies-1"
                    className="table vm trans table-small-font no-mb table-bordered table-striped"
                  >
                    <thead>
                      <tr>
                        <th>{translate('utils.email')}</th>
                        <th>BTC</th>
                        <th>ETH</th>
                        <th>XRP</th>
                        <th>TRX</th>
                        <th>BNB</th>
                        <th>USDT</th>
                        <th>TOSI HOLD</th>
                        <th>TOSI LÃI</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        walletDetails.map((item, index) => {
                          return (
                            <tr key={index}>
                              <td><small className="text-muted">{item.user_email}</small></td>
                              <td className="green-text boldy">{item.BTC}</td>
                              <td className="green-text boldy">{item.ETH}</td>
                              <td className="green-text boldy">{item.XRP}</td>
                              <td><small className="green-text boldy">{item.TRX}</small></td>
                              <td><small className="green-text boldy">{item.BNB}</small></td>
                              <td><small className="green-text boldy">{item.USDT}</small></td>
                              <td><small className="blue-text boldy">{item.tosi_total}</small></td>
                              <td><small className="blue-text boldy">{item.tosi_interest_rate}</small></td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </section>
      </div>
      {/* {
        total > 10 &&
        <div className="col-lg-12">
          <Pagination
            current={page} onChange={onChangePage}
            total={total} pageSize={limit}
          />
        </div>
      } */}
    </div>
  )
}

export default withLocalize(UserWalletDetails)
