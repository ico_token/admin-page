
import React from 'react';

const HeaderForm = ({nameForm}) => {
  return (
    <header className="panel_header gradient-blue">
      <h2 className="title pull-left w-text">{nameForm}</h2>
    </header>
  )
}

export default HeaderForm