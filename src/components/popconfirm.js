import React from 'react'
import { Popconfirm } from 'antd'
import 'antd/es/popconfirm/style/css'
import { ExclamationOutline } from '../overwrite/antd-icons'

export default ({ children, ...props }) => (
  <Popconfirm
    {...props}
  >
    {children}
  </Popconfirm>
)
