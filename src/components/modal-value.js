import React from 'react';
import Modal from '@/components/modal'
import styled from 'styled-components'


const StyledForm = styled.div`
  margin-top: 10px;
  height: auto;
`

const ModalValue = ({ close, value, isOpen }) => {
  return (
    <Modal
      visible={isOpen}
      onCancel={close}
    >
      <StyledForm>
        {value}
      </StyledForm>
    </Modal>

  )
}

export default ModalValue
