
import React from 'react';

const TitlePage = ({namePage}) => {
  return (
    <div className="col-xs-12">
      <div className="page-title">
        <div className="pull-left">
          <h1 className="title">{namePage}</h1>
        </div>

        {/* <div className="pull-right hidden-xs">
          <ol className="breadcrumb">
            <li>
              <a href="/"><i className="fa fa-home"></i>Home</a>
            </li>
            <li className="active">
              <strong>{namePage}</strong>
            </li>
          </ol>
        </div> */}

      </div>
    </div>
  )
}

export default TitlePage
