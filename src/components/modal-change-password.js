import React, {useState} from 'react';
import Modal from '@/components/modal'
import InputPassword from '@/components/input-password'
import Field from '@/components/field'
import Button from '@/components/button'
import Notification from '@/components/notification'
import { Formik, Form } from 'formik'
import { object, string, ref } from 'yup'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
import { withLocalize } from 'react-localize-redux'

const { changePasswordByOldPassword, } = actions;
const ModalChangePassWord = ({ close, isOpen, translate }) => {
  const [isLoading, setIsLoading] = useState(false)

	const dispatch = useDispatch();

	const _onSubmit = (values) => {
		const payload = {
			password: values.password,
			oldPassword: values.oldPassword
		}
		setIsLoading(true)
		dispatch(
			changePasswordByOldPassword(payload, (action) => {
				setIsLoading(false)
				if (action === TYPES.CHANGE_PASSWORD_BY_OLD_PASSWORD_SUCCESS) {
					close()
					return Notification.success(translate('success.CHANGE_PASSWORD_SUCCESS'))
				}
			})
		)
	}

	const _renderForm = ({ handleSubmit, ...form }) => {
		return (
			<div>
				<Form id="msg_validate" action="#" novalidate="novalidate" className="no-mb no-mt">
					<div>
						<div className="form-login col-xs-12">
							<label className="form-label">{translate('utils.old-password')}</label>
							<div className="controls">
								<Field
									form={form}
									name="oldPassword"
									component={InputPassword}
								/>
							</div>
						</div>

						<div className="form-login col-xs-12">
							<label className="form-label">{translate('utils.password')}</label>
							<div className="controls">
								<Field
									form={form}
									name="password"
									component={InputPassword}
								/>
							</div>
						</div>

						<div className="form-login col-xs-12">
							<label className="form-label">{translate('utils.confirm-password')}</label>
							<div className="controls">
								<Field
									form={form}
									name="confirmPassword"
									component={InputPassword}
								/>
							</div>
						</div>

						<div className="text-center">
							<Button
								className="btn btn-primary btn-corner"
								onClick={handleSubmit}
								loading={isLoading}
								disabled={isLoading}
							>{translate('utils.change-password')}</Button>
						</div>
					</div>
				</Form>
			</div>
		)
	}

	const validationSchema = object().shape({
		oldPassword: string().required(),
		password: string().required(),
		confirmPassword: string().required().oneOf([ref('password'), null], 'password_must_match')
	})
	return (
		<Modal
			visible={isOpen}
			onCancel={close}
			title={translate('utils.change-password')}
			destroyOnClose={true}
		>
			<div>
				<Formik
					validateOnChange={false}
					validateOnBlur={false}
					validationSchema={validationSchema}
					onSubmit={_onSubmit}
					component={_renderForm}
				/>
			</div>
		</Modal>
	)
}

export default withLocalize(ModalChangePassWord)