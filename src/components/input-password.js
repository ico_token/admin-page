import React from 'react'
import { Input } from 'antd'
import classNames from 'classnames'
// import { EyeInvisibleOutlined, EyeOutlined } from '@/overwrite/antd-icons';

export default ({ field, form, modern, simple, className, ...props }) => (

  <Input.Password
    // iconRender={visible => (visible ? <EyeOutlined /> : <EyeInvisibleOutlined />)}
    {...field}
    {...props}
    className={classNames(className, { modern, simple })}
  />

)
