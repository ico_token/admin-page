import React, { Component } from 'react'
import { Select } from 'antd'
import lodash from 'lodash'
import styled from 'styled-components'
import 'antd/es/select/style/css'
import { COIN_SYSTEM, ICON_COIN } from '@/utils/constant'

const { Option } = Select

const StyledSelect = styled(Select)`
  width: 100%;
  .ant-select-selection--single {
    /* height: 50px; */
  height: auto;
  display: flex;
}

`
const FormCoin = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 5px;
  img {
    width: 27px;
    height: 27px;
  }
  img, i {
    margin-right: 10px
  }
`

export default class extends Component {
  _onChange = (value) => {
    const { field, onChange, name } = this.props

    if (onChange) onChange({ target: { value, name: field?.name || name } })
    if (!lodash.isEmpty(field)) field.onChange({ target: { value, name: field.name } })
  }

  _renderOption = (option, index) => {
    const { optionBinding, renderOption } = this.props

    let value
    let name
    if (lodash.isEmpty(optionBinding)) {
      /* eslint-disable prefer-destructuring */
      value = option.id
      name = option.name
    } else {
      value = option[optionBinding.id]
      name = option[optionBinding.name]
    }

    return (
      <Option key={index} value={value} name={name}>
        <FormCoin>
          {
            option.code === COIN_SYSTEM.CODE
              ? <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system mr-1" />
              : <i className={ICON_COIN[option.code]?.COLOR} />
          }
          {renderOption ? renderOption({ value, name }) : name}
        </FormCoin> 
      </Option>
    )
  }

  render() {
    const { field, options, onChange, optionBinding, value, renderOption, ...props } = this.props

    return (
      <StyledSelect
        {...field}
        {...props}
        onChange={this._onChange}
        value={field?.value || value}
      >
        {options.map(this._renderOption)}
      </StyledSelect>
    )
  }
}
