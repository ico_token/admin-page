import React, { useEffect, useState } from 'react'
import { TYPES, actions } from '@/store/actions'
import { useDispatch } from 'react-redux';
import { COIN_SYSTEM, ICON_COIN, FROM_TIME, TRANSACTION_TYPE } from '@/utils/constant'
import Pagination from './pagination'
import moment from 'moment';
import { withLocalize } from 'react-localize-redux'
import HeaderForm from './header-form'

const { getTransactionByUser } = actions;
const Transaction = ({ params, translate, load }) => {
  const dispatch = useDispatch();
  const [dataTransaction, setTransaction] = useState([]);
  const [totalTransaction, setTotalTransaction] = useState(0);
  const [page, setPage] = useState(1);
  const [limit] = useState(10);

  const param = {
    ...params, page, limit
  }

  useEffect(() => {
    getTransaction(param)
  }, []);

  useEffect(() => {
    if(load) {
      getTransaction(param)
    }
  }, [params]);

  const getTransaction = (payload) => {
    dispatch(
      getTransactionByUser(payload, (action, data) => {
        if (action === TYPES.GET_TRANSACTION_BY_USER_SUCCESS) {
          setTransaction(data.rows);
          setTotalTransaction(data.count)
        }
      })
    )
  }; 

  const onChangePage = (p) => {
    setPage(p);
    getTransaction({...param, page: p})
  };

  return (
    <div>
      <div className="col-lg-12">
        <section className="box has-border-left-3">
          <HeaderForm
            nameForm={translate('transactions.transactions-histosy')}
          />
          <div className="content-body">
            <div className="row">
              <div className="col-xs-12">

                <div className="table-responsive" data-pattern="priority-columns">
                  <table
                    id="tech-companies-1"
                    className="table vm trans table-small-font no-mb table-bordered table-striped"
                  >
                    <thead>
                      <tr>
                        <th className="text-center">{translate('coins.coins')}</th>
                        <th>{translate('utils.email')}</th>
                        <th>{translate('utils.time')}</th>
                        <th>{translate('transactions.type')}</th>
                        <th>{translate('utils.amount')}</th>
                        <th>{translate('transactions.usdt-converted')}</th>
                        <th>{translate('transactions.fee')}</th>
                        <th>{translate('transactions.receiver')}</th>
                        <th>{translate('transactions.sender')}</th>
                        <th>{translate('transactions.hash')}</th>
                        <th>{translate('transactions.block-number')}</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        dataTransaction.map((item, index) => {
                          return (
                            <tr key={index}>
                              <td >
                                <div className="round img2">
                                  {
                                    item.coin.code === COIN_SYSTEM.CODE
                                      ? <img src={COIN_SYSTEM.NORMAL} alt="" className="coin-system" />
                                      : <i className={ICON_COIN[item.coin.code]?.COLOR} />
                                  }
                                </div>
                                <div className="designer-info">
                                  <h6>{item.coin.code}</h6>
                                </div></td>
                              <td><small className="text-muted">{item.user.email}</small></td>
                              <td><small className="text-muted">{moment(item.timestamp).format(FROM_TIME)}</small></td>
                              <td>
                                <span className={`badge w-70 ${TRANSACTION_TYPE[item.type]}`}>
                                {translate(`transactions.${TRANSACTION_TYPE[item.type]}`)}</span>
                              </td>
                              <td className="green-text boldy">{item.amount}</td>
                              <td className="green-text boldy">{item.coin_usdt_ratio != null ? item.amount * item.coin_usdt_ratio : ''}</td>
                              <td className="red-text boldy">{item.fee}</td>
                              <td><small className="text-muted">{item.receiver}</small></td>
                              <td><small className="text-muted">{item.sender}</small></td>
                              <td><small className="text-muted">{item.hash}</small></td>
                              <td><small className="text-muted">{item.block_number}</small></td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </section>
      </div>
      {
        totalTransaction > 10 &&
        <div className="col-lg-12">
          <Pagination
            current={page} onChange={onChangePage}
            total={totalTransaction} pageSize={limit}
          />
        </div>
      }
    </div>
  )
}

export default withLocalize(Transaction)
