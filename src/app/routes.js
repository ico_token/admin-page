import React, { useState, Suspense, lazy } from 'react'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Storage from '@/utils/storage'
import Loading from '@/components/loading'
import Page from '@/components/page'
import Header from '@/layout/header'
import SideBar from '@/layout/side-bar'


const Login = lazy(() => import('@/pages/account/login'))
const Register = lazy(() => import('@/pages/account/register'))
const ForgotPassword = lazy(() => import('@/pages/account/forgot-password'))
const ResetPassword = lazy(() => import('@/pages/account/reset-password'))
const NotFound = lazy(() => import('@/pages/not-found'))

const Dashboard = lazy(() => import('@/pages/dashboard'))
const AdminWallet = lazy(() => import('@/pages/admin-wallet'))
const UserWallet = lazy(() => import('@/pages/user-wallet'))
// const BuySell = lazy(() => import('@/pages/buy-sell'))
// const ICOAdmin = lazy(() => import('@/pages/ico-admin'))
// const Personal = lazy(() => import('@/pages/settings/personal'))
// const Account = lazy(() => import('@/pages/settings/account'))
const Mailbox = lazy(() => import('@/pages/mailbox'))
const Users = lazy(() => import('@/pages/users'))
const UserDetail = lazy(() => import('@/pages/user-detail'))
const SystemSettings = lazy(() => import('@/pages/system-settings'))
const Logs = lazy(() => import('@/pages/logs'))
const Contacts = lazy(() => import('@/pages/contacts'))
const Packet = lazy(() => import('@/pages/packet'))
const WithdrawRequests = lazy(() => import('@/pages/withdraw-requests'))
const Referral = lazy(() => import('@/pages/referral'))
const CoinSuppliations = lazy(() => import('@/pages/coin-supplications'))
const Group = lazy(() => import('@/pages/group'))
const Transactions = lazy(() => import('@/pages/transactions'))
const SupplyRank = lazy(() => import('@/pages/supply-rank'))

// FAQ
const FAQ = lazy(() => import('@/pages/faq'))
const FAQHandle = lazy(() => import('@/pages/faq-handle'))

const Coins = lazy(() => import('@/pages/coins'))
const CoinHandle = lazy(() => import('@/pages/coins/coin-handle'))

const PrivateRoute = ({ condition, redirect, ...props }) => {
  condition = condition()

  if (condition) return <Route {...props} />
  return <Redirect to={redirect} />
}

const Routes = () => {
  const [isSideBarOpen, setIsSideBarOpen] = useState(true)
  const _renderLazyComponent = (LazyComponent, params) => (props) => <LazyComponent {...props} {...params} />
  const pushToggle = (toggle) => {
    setIsSideBarOpen(toggle)
  };
  const _renderAuthRoutes = () => {
    return (
      <>
        <Header
          pushToggle={pushToggle}
          isSideBarOpen={isSideBarOpen}
        />
        <div className="page-container row-fluid container-fluid">
          <SideBar
            isSideBarOpen={isSideBarOpen}
          />
          <section id="main-content" className={!isSideBarOpen ? 'sidebar_shift' : ''}>
            <div className="wrapper main-wrapper row">
              <Suspense fallback={<Page sidebar><Loading /></Page>}>
                <Switch>
                  <Route exact path="/" component={_renderLazyComponent(Dashboard)} />
                  <Route exact path="/dashboard" component={_renderLazyComponent(Dashboard)} />
                  <Route exact path="/Admin-wallet" component={_renderLazyComponent(AdminWallet)} />
                  <Route exact path="/user-wallet" component={_renderLazyComponent(UserWallet)} />
                  {/* <Route exact path="/buy-sell" component={_renderLazyComponent(BuySell)} /> */}
                  {/* <Route exact path="/ico-admin" component={_renderLazyComponent(ICOAdmin)} /> */}
                  {/* <Route exact path="/personal-settings" component={_renderLazyComponent(Personal)} />
                    <Route exact path="/account-settings" component={_renderLazyComponent(Account)} /> */}
                  <Route exact path="/inbox" component={_renderLazyComponent(Mailbox)} />
                  <Route exact path="/compose" component={_renderLazyComponent(Mailbox)} />
                  <Route exact path="/view" component={_renderLazyComponent(Mailbox)} />
                  <Route exact path="/sent" component={_renderLazyComponent(Mailbox)} />
                  <Route exact path="/users" component={_renderLazyComponent(Users)} />
                  <Route exact path="/user-detail/:id" component={_renderLazyComponent(UserDetail)} />
                  <Route exact path="/faq" component={_renderLazyComponent(FAQ)} />
                  <Route exact path="/faq-create" component={_renderLazyComponent(FAQHandle)} />
                  <Route exact path="/faq-update/:id" component={_renderLazyComponent(FAQHandle)} />
                  <Route exact path="/system-settings" component={_renderLazyComponent(SystemSettings)} />
                  <Route exact path="/logs" component={_renderLazyComponent(Logs)} />
                  <Route exact path="/coins" component={_renderLazyComponent(Coins)} />
                  <Route exact path="/coin-create" component={_renderLazyComponent(CoinHandle)} />
                  <Route exact path="/coin-update/:id" component={_renderLazyComponent(CoinHandle)} />
                  <Route exact path="/contacts" component={_renderLazyComponent(Contacts)} />
                  <Route exact path="/packets" component={_renderLazyComponent(Packet)} />
                  <Route exact path="/withdraw-requests" component={_renderLazyComponent(WithdrawRequests)} />
                  <Route exact path="/referral" component={_renderLazyComponent(Referral)} />
                  <Route exact path="/coin-supplications" component={_renderLazyComponent(CoinSuppliations)} />
                  <Route exact path="/group" component={_renderLazyComponent(Group)} />
                  <Route exact path="/transactions" component={_renderLazyComponent(Transactions)} />
                  <Route exact path="/supply-rank" component={_renderLazyComponent(SupplyRank)} />
                  <Redirect to="/not-found" />
                </Switch>
              </Suspense>
            </div>
          </section>
        </div>
      </>
    )
  }

  return (
    <div>
      <Suspense fallback={<Page><Loading /></Page>}>
        <Switch>
          <Route path="/login" component={_renderLazyComponent(Login)} />
          <Route path="/register" component={_renderLazyComponent(Register)} />
          <Route path="/not-found" component={_renderLazyComponent(NotFound)} />
          <Route path="/forgot-password" component={_renderLazyComponent(ForgotPassword)} />
          <Route path="/reset-password" component={_renderLazyComponent(ResetPassword)} />
          <PrivateRoute
            condition={() => Storage.has('ACCESS_TOKEN')}
            redirect="/login"
            path="/"
            component={_renderAuthRoutes}
          />
        </Switch>
      </Suspense>
    </div>
  )
}

export default connect(
  (state) => ({
    uiStore: state.ui,
  })
)(withRouter(Routes))
