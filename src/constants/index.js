
const FAQ_CATEGORY_VALUE = {
  GENERAL: '0',
  PAYMENT: '1',
  WITHDRAW: '2',
  OTHER: '3' 
}

export {
  FAQ_CATEGORY_VALUE,
}