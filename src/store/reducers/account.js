import { TYPES } from '@/store/actions'

const INIT_STATE = {
  loaded: [],
  submitting: null,
  error: null,

  userCode: null,
  permissions: [],
  roleName: null
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case TYPES.LOGIN_REQUEST:
    case TYPES.RESET_PASSWORD_REQUEST:
    case TYPES.CHANGE_PASSWORD_REQUEST:
      return {
        ...state,
        submitting: action.type
      }

    case TYPES.LOGIN_SUCCESS:
    case TYPES.RESET_PASSWORD_SUCCESS:
    case TYPES.CHANGE_PASSWORD_REQUEST:
      return {
        ...state,
        submitting: null
      }

    case TYPES.LOGIN_FAILURE:
    case TYPES.RESET_PASSWORD_FAILURE:
    case TYPES.CHANGE_PASSWORD_FAILURE:
      return {
        ...state,
        submitting: null,
        error: action.error
      }
    default:
      return state
  }
}
