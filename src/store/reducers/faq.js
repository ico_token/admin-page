import { TYPES } from '@/store/actions'

const INIT_STATE = {
  loaded: [],
  submitting: null,
  error: null,

  faqList: [],
  faqItem: null
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case TYPES.CREATE_FAQ_REQUEST:
    case TYPES.DELETE_FAQ_REQUEST:
    case TYPES.GET_FAQ_REQUEST:
    case TYPES.UPDATE_FAQ_REQUEST:
    case TYPES.GET_FAQ_BY_ID_REQUEST:
      return {
        ...state,
        submitting: action.type
      }

    case TYPES.CREATE_FAQ_SUCCESS:
    case TYPES.DELETE_FAQ_SUCCESS:
    case TYPES.UPDATE_FAQ_SUCCESS:
      return {
        ...state,
        submitting: null
      }
    case TYPES.GET_FAQ_SUCCESS:
      return {
        ...state,
        submitting: null,
        faqList: action.data
      }
    case TYPES.GET_FAQ_BY_ID_SUCCESS:
      return {
        ...state,
        submitting: null,
        faqItem: action.data
      }
    case TYPES.CREATE_FAQ_FAILURE:
    case TYPES.DELETE_FAQ_FAILURE:
    case TYPES.GET_FAQ_FAILURE:
    case TYPES.UPDATE_FAQ_FAILURE:
    case TYPES.GET_FAQ_BY_ID_FAILURE:
      return {
        ...state,
        submitting: null,
        error: action.error
      }
    default:
      return state
  }
}
