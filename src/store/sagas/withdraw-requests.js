import { all, takeLatest, takeEvery } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import { getListWithdrawRequest, handleWithdrawRequest  } from '@/api/withdraw-requests'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.GET_LIST_WITHDRAW_REQUEST, sagaHelper({
      api: getListWithdrawRequest
    })),
    takeLatest(TYPES.HANDLE_WITHDRAW_REQUEST, sagaHelper({
      api: handleWithdrawRequest
    }))
  ])
}