import { all, takeLatest, takeEvery } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import { getReferral, getReferralTree, updateRank } from '@/api/referral'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.GET_REFERRAL, sagaHelper({
      api: getReferral
    })),
    takeLatest(TYPES.GET_REFERRAL_TREE, sagaHelper({
      api: getReferralTree
    })),
    takeLatest(TYPES.UPDATE_RANK, sagaHelper({
      api: updateRank
    }))
  ])
}
