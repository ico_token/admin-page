import { all, takeLatest } from 'redux-saga/effects'
import sagaHelper from '../../utils/saga-helper'
import { TYPES } from '../actions'
import {
  createCoin,
  getListCoin,
  updateCoin,
  deleteCoin
} from '../../api/coins'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.CREATE_COIN, sagaHelper({
      api: createCoin
    })),
    takeLatest(TYPES.GET_LIST_COIN, sagaHelper({
      api: getListCoin
    })),
    takeLatest(TYPES.UPDATE_COIN, sagaHelper({
      api: updateCoin
    })),
    takeLatest(TYPES.DELETE_COIN, sagaHelper({
      api: deleteCoin
    }))
  ])
}
