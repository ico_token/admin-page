import { all, takeLatest, takeEvery } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import {
  getListUsers,
  getUserById,
  getTransactionByUser,
  verifyKyc,
  updateUser,
  handleLockUser,
  deleteUserById,
  getUserCurrency,
  getUserWallet
} from '@/api/users'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.GET_LIST_USERS, sagaHelper({
      api: getListUsers
    })),
    takeLatest(TYPES.GET_USER_BY_ID, sagaHelper({
      api: getUserById
    })),
    takeLatest(TYPES.GET_TRANSACTION_BY_USER, sagaHelper({
      api: getTransactionByUser
    })),
    takeLatest(TYPES.VERIFY_KYC, sagaHelper({
      api: verifyKyc
    })),
    takeLatest(TYPES.UPDATE_USER, sagaHelper({
      api: updateUser
    })),
    takeLatest(TYPES.HANDLE_LOCK_USER, sagaHelper({
      api: handleLockUser
    })),
    takeLatest(TYPES.DELETE_USER, sagaHelper({
      api: deleteUserById
    })),
    takeLatest(TYPES.GET_USER_CURRENCY, sagaHelper({
      api: getUserCurrency
    })),
    takeLatest(TYPES.GET_USER_WALLET, sagaHelper({
      api: getUserWallet
    })),
  ])
}
