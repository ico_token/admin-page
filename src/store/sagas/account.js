import { all, takeLatest } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import { login, resetPassword, changePassword, changePasswordByOldPassword, getStatistical  } from '@/api/account'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.LOGIN, sagaHelper({
      api: login
    })),
    takeLatest(TYPES.RESET_PASSWORD, sagaHelper({
      api: resetPassword
    })),
    takeLatest(TYPES.CHANGE_PASSWORD, sagaHelper({
      api: changePassword
    })),
    takeLatest(TYPES.CHANGE_PASSWORD_BY_OLD_PASSWORD, sagaHelper({
      api: changePasswordByOldPassword
    })),
    takeLatest(TYPES.GET_STATISTICAL, sagaHelper({
      api: getStatistical
    }))
  ])
}
