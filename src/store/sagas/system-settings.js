import { all, takeLatest } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import { getIcoSetting, getMaintenances, updateIcoSetting, updateMaintenances, getAllLogs } from '@/api/system-settings'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.GET_ICO_SETTING, sagaHelper({
      api: getIcoSetting
    })),
    takeLatest(TYPES.GET_MAINTENANCES, sagaHelper({
      api: getMaintenances
    })),
    takeLatest(TYPES.UPDATE_ICO_SETTING, sagaHelper({
      api: updateIcoSetting
    })),
    takeLatest(TYPES.UPDATE_MAINTENANCES, sagaHelper({
      api: updateMaintenances
    })),
    takeLatest(TYPES.GET_ALL_LOGS, sagaHelper({
      api: getAllLogs
    })),
  ])
}
