import { all, takeLatest, takeEvery } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import { getListCoinSupplications, supplyTosiToUser, recallSuppliedTosiOfUser } from '@/api/coin-supplications'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.GET_LIST_COIN_SUPPLICATIONS, sagaHelper({
      api: getListCoinSupplications
    })),
    takeLatest(TYPES.SUPPLY_TOSI_TO_USER, sagaHelper({
      api: supplyTosiToUser
    })),
    takeLatest(TYPES.RECALL_SUPPLIED_TOSI_OF_USER, sagaHelper({
      api: recallSuppliedTosiOfUser
    }))
  ])
}
