import { all, takeLatest } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import {
	createPacketDetail,
	updatePacketDetail,
	deletePacketDetail,
	createPacket,
	updatePacket,
	getPacketById,
	getPacketList
} from '@/api/packet'

export default function* watcher() {
	yield all([
		takeLatest(TYPES.CREATE_PACKET_DETAIL, sagaHelper({
			api: createPacketDetail
		})),
		takeLatest(TYPES.UPDATE_PACKET_DETAIL, sagaHelper({
			api: updatePacketDetail
		})),
		takeLatest(TYPES.DELETE_PACKET_DETAIL, sagaHelper({
			api: deletePacketDetail
		})),
		takeLatest(TYPES.CREATE_PACKET, sagaHelper({
			api: createPacket
		})),
		takeLatest(TYPES.UPDATE_PACKET, sagaHelper({
			api: updatePacket
		})),
		takeLatest(TYPES.GET_PACKET_BY_ID, sagaHelper({
			api: getPacketById
		})),
		takeLatest(TYPES.GET_PACKET_LIST, sagaHelper({
			api: getPacketList
		}))
	])
}