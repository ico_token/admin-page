import { all, takeLatest } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import {
  createFaq,
  deleteFaq,
  getFaq,
  updateFaq,
  getFaqById
} from '@/api/faq'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.CREATE_FAQ, sagaHelper({
      api: createFaq
    })),
    takeLatest(TYPES.DELETE_FAQ, sagaHelper({
      api: deleteFaq
    })),
    takeLatest(TYPES.GET_FAQ, sagaHelper({
      api: getFaq
    })),
    takeLatest(TYPES.UPDATE_FAQ, sagaHelper({
      api: updateFaq
    })),
    takeLatest(TYPES.GET_FAQ_BY_ID, sagaHelper({
      api: getFaqById
    })),
  ])
}
