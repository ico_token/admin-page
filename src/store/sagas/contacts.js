import { all, takeLatest } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import { getListContacts, updateContacts  } from '@/api/contacts'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.GET_LIST_CONTACTS, sagaHelper({
      api: getListContacts
    })),
    takeLatest(TYPES.UPDATE_CONTACTS, sagaHelper({
      api: updateContacts
    }))
  ])
}
