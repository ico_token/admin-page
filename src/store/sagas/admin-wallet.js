import { all, takeLatest } from 'redux-saga/effects'
import sagaHelper from '@/utils/saga-helper'
import { TYPES } from '@/store/actions'
import { createWallet, getAdminWallet, sendOtpConfirmWallet  } from '@/api/admin-wallet'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.CREATE_WALLET, sagaHelper({
      api: createWallet
    })),
    takeLatest(TYPES.GET_ADMIN_WALLET, sagaHelper({
      api: getAdminWallet
    })),
    takeLatest(TYPES.SEND_OTP_CONFIRM_WALLET, sagaHelper({
      api: sendOtpConfirmWallet
    }))
  ])
}
