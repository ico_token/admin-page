import { all, takeLatest, put } from 'redux-saga/effects'

import sagaHelper from '../../utils/saga-helper'
import { TYPES } from '../actions'
import {
  getMaintain,
  getSignS3,
  uploadS3,
  sendOtp
} from '../../api/utils'

function* isLoading() {
  yield put({ type: TYPES.IS_LOADING_SUCCESS })
}

function* closeNotification() {
  yield put({ type: TYPES.SET_CLOSE_NOTIFICATION_REQUEST })
}

function* openNotification() {
  yield put({ type: TYPES.SET_OPEN_NOTIFICATION_REQUEST })
}

export default function* watcher() {
  yield all([
    takeLatest(TYPES.IS_LOADING, isLoading),
    takeLatest(TYPES.GET_MAINTAIN, sagaHelper({
      api: getMaintain
    })),
    takeLatest(TYPES.GET_SIGN_S3, sagaHelper({
      api: getSignS3
    })),
    takeLatest(TYPES.UPLOAD_S3, sagaHelper({
      api: uploadS3
    })),
    takeLatest(TYPES.SEND_OTP, sagaHelper({
      api: sendOtp
    })),
    takeLatest(TYPES.SET_CLOSE_NOTIFICATION, closeNotification),
    takeLatest(TYPES.SET_OPEN_NOTIFICATION, openNotification),
  ])
}
