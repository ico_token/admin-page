import { all } from 'redux-saga/effects'

import account from './account'
import users from './users'
import faq from './faq'
import systemSettings from './system-settings'
import coins from './coins'
import contacts from './contacts'
import packet from './packet'
import adminWallet from './admin-wallet'
import withdrawRequest from './withdraw-requests'
import referral from './referral'
import coinSupplications from './coin-supplications'
import utils from './utils'

export default function* sagas() {
  yield all([
    account(),
    users(),
    faq(),
    systemSettings(),
    coins(),
    contacts(),
    packet(),
    adminWallet(),
    withdrawRequest(),
    referral(),
    coinSupplications(),
    utils(),
  ])
}
